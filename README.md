muratafuneral
===============
村田葬儀社 オーダーシステム


## ブランチモデル

`Git Flow`で管理しています。

> * [A successful Git branching model (原文)](http://nvie.com/posts/a-successful-git-branching-model/)
> * [A successful Git branching model (日本語訳)](http://keijinsonyaban.blogspot.jp/2010/10/successful-git-branching-model.html)

SourceTree最新版の`Git Flow`ではブランチ終了のマージ時に`fast-forward`が有効になってしまっている為、
ブランチ終了時は手動で`develop`チェックアウトのち`--no-ff`マージ、のちブランチ削除を実行した方が良いかも。

また、`.gitignore`の表記方法の関係で Git 2.7 以降を利用してください。

> * [Git 2.7 で .gitignore が便利になっている](http://www.tam-tam.co.jp/tipsnote/program/post8318.html)


## 準備

クローン後、プロジェクトルートに`Composer`をインストールし依存関係をインストールします。


```
$ cd muratafuneral/
$ curl -s https://getcomposer.org/installer | php
$ cd muratafuneral/order.sys/
$ php ../composer.phar install
```


## 環境による設定の切り替え

本プロジェクトはFuelPHPを参考にした環境別設定ファイル切り替えを実装しています。
サーバに環境変数`CAKE_ENV`を用意し、`development|staging|production`を設定してください。
環境別設定の設定ファイルは`config/environments`に存在します。

* `development`: 開発環境(デフォルト)
* `staging`: ステージング環境
* `production`: 本番環境

Apacheの場合は、`.htaccess`に`SetEnv`を記述することで設定できます。
環境変数`CAKE_ENV`が`development`もしくは未設定の場合は開発環境設定が適用されます。

Apacheサーバに`mod_env`がインストールされていない場合、`SetEnv`を使用できません。
その場合のみ`config/environment.php`にて環境選択を設定することができます。
どちらも設定した場合は、環境変数`CAKE_ENV`が優先されます。


## 開発サーバ設定

開発用にビルトインサーバを使用できます。
ビルトインサーバを実行中、ターミナルは常駐状態になります。

```
$ cd muratafuneral/order.sys/
$ bin/cake server
```


## マイグレーション

下記コマンドにより、マイグレーションを実行してテーブルを初期化してください。

```
$ cd muratafuneral/order.sys/
$ bin/cake migrations migrate
```

また、下記コマンドにより、テーブルの初期データを挿入することができます。

```
$ cd muratafuneral/order.sys/
$ bin/cake migrations seed
```


## 最新版適用時の注意

最新版を適用した後は、下記コマンドを実行してください。
データベースもマイグレーションの差分適用に移行するまでは、全テーブルの削除とマイグレーションの再実行をお願いします。

```
$ cd muratafuneral/order.sys/
$ php ../composer.phar install
$ bin/cake migrations migrate
$ bin/cake migrations seed
```
