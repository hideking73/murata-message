<?php
return [
    /**
     * [トークンの有効期限]
     */
    'Token' => [
        'expires' => '+3 days'
    ],

    /**
     * [会員の状態]
     * - 'Customers.is_formal'により判断
     */
    'CustomerState' => [
        false => '仮会員',
        true => '正会員'
    ],

    /**
     * [商品タイプID]
     * - 'ProductTypes.id'
     */
    'ProductType' => [
        'offering' => 1,    // 供花・弔意
        'celebrate' => 2,   // ご祝花・お祝い
        'other' => 9        // その他
    ],

    /**
     * [メッセージタイプID]
     * - 'MessageTypes.id'
     */
    'MessageType' => [
        'template' => 1,    // テンプレートメッセージ
        'original' => 2     // オリジナルメッセージ
    ],

    /**
     * [受注状態ID]
     * - 'OrderStates.id'
     */
    'OrderState' => [
        'waiting' => 1,     // 入金待ち
        'ready' => 2,       // 発送準備中
        'complete' => 3     // 発送準備中
    ],

    /**
     * [決済方法ID]
     * - 'PaymentTypes.id'
     */
    'PaymentType' => [
        'bank' => 1,        // 銀行振込
        'on_the_day' => 2   // 当日お支払い
    ],

    /**
     * [お届け先タイプID]
     * - 'DeliveryTypes.id'
     */
    'DeliveryType' => [
        'murata' => 1,      // 村田葬儀社の葬祭式場
        'other' => 2        // 他社の式場
    ],

    /**
     * [消費税の端数計算(処理名)]
     */
    'Tax' => [
        'calc' => 'floor'   // floor|ceil|round
    ],

    /**
     * [割引の端数計算(処理名)]
     */
    'Discount' => [
        'calc' => 'floor'   // floor|ceil|round
    ],

    /**
     * [送料(税別)]
     */
    'Carriage' => [
        'carriage' => 500   // 全国共通
    ]
];
