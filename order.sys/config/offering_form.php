<?php
return [
    'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>&nbsp;',
    'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>&nbsp;',
    'inputContainer' => '{{content}}',
    'inputContainerError' => '{{content}}{{error}}'
];
