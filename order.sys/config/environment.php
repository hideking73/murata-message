<?php
return [
    /**
     * [環境選択設定]
     * - サーバ側で環境変数が使用できない場合にのみ設定してください。
     *
     * development|staging|production
     */
    'environment' => 'development',
];
