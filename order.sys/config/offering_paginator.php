<?php
return [
    'nextActive' => '<li><a href="{{url}}" aria-label="Next">{{text}}</a></li>',
    'nextDisabled' => '<li class="disabled"><a href="#" aria-label="Next">{{text}}</a></li>',
    'prevActive' => '<li><a href="{{url}}" aria-label="Previous">{{text}}</a></li>',
    'prevDisabled' => '<li class="disabled"><a href="#" aria-label="Previous">{{text}}</a></li>',
    'first' => '<li class="first"><a href="{{url}}">{{text}}</a></li>',
    'last' => '<li class="last"><a href="{{url}}">{{text}}</a></li>',
    'number' => '<li><a href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="active"><a href="#">{{text}}</a></li>'
];
