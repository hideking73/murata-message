<?php
use Migrations\AbstractMigration;

class Setup extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // [system] sessions
        $table = $this->table('sessions', ['id' => false, 'primary_key' => ['id'], 'comment' => 'セッション']);
        $table
            ->addColumn('id', 'string', ['limit' => 40, 'null' => false])
            ->addColumn('data', 'text', ['comment' => 'データ'])
            ->addColumn('expires', 'integer', ['limit' => 11, 'null' => false, 'comment' => '有効期限'])
            ->create();

        // [master] prefectures
        $table = $this->table('prefectures', ['comment' => '都道府県']);
        $table
            ->addColumn('prefecture', 'string', ['limit' => 255, 'null' => false, 'comment' => '都道府県'])
            ->create();

        // [master] product_types
        $table = $this->table('product_types', ['comment' => '商品タイプ']);
        $table
            ->addColumn('product_type', 'string', ['limit' => 255, 'null' => false, 'comment' => '商品タイプ'])
            ->create();

        // [master] message_types
        $table = $this->table('message_types', ['comment' => 'メッセージタイプ']);
        $table
            ->addColumn('message_type', 'string', ['limit' => 255, 'null' => false, 'comment' => 'メッセージタイプ'])
            ->addColumn('message_price', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => 'メッセージ料(税別)'])
            ->create();

        // [master] tax_rates
        $table = $this->table('tax_rates', ['comment' => '消費税率']);
        $table
            ->addColumn('tax_rate', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => '消費税率(%)'])
            ->addColumn('apply', 'datetime', ['null' => false, 'comment' => '適用日時'])
            ->create();

        // [master] delivery_types
        $table = $this->table('delivery_types', ['comment' => 'お届け先タイプ']);
        $table
            ->addColumn('delivery_type', 'string', ['limit' => 10, 'null' => false, 'comment' => 'お届け先タイプ'])
            ->create();

        // [master] facilities
        $table = $this->table('facilities', ['comment' => '施設(葬祭式場)']);
        $table
            ->addColumn('facility', 'string', ['limit' => 255, 'null' => false, 'comment' => '施設(葬祭式場)'])
            ->create();

        // [master] delivery_areas
        $table = $this->table('delivery_areas', ['comment' => '配達エリア']);
        $table
            ->addColumn('postal_code', 'string', ['limit' => 10, 'null' => false, 'comment' => '郵便番号'])
            ->create();

        // [master] payment_types
        $table = $this->table('payment_types', ['comment' => '決済方法']);
        $table
            ->addColumn('payment_type', 'string', ['limit' => 255, 'null' => false, 'comment' => '決済方法'])
            ->addColumn('offering', 'string', ['limit' => 255, 'null' => false, 'comment' => '決済方法(供花・弔意)'])
            ->addColumn('celebrate', 'string', ['limit' => 255, 'null' => false, 'comment' => '決済方法(ご祝花・お祝い)'])
            ->create();

        // [master] order_states
        $table = $this->table('order_states', ['comment' => '受注対応状況']);
        $table
            ->addColumn('order_state', 'string', ['limit' => 255, 'null' => false, 'comment' => '受注対応状況'])
            ->create();

        // [data] products
        $table = $this->table('products', ['comment' => '商品情報']);
        $table
            ->addColumn('product_type_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '商品タイプID'])
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false, 'comment' => '商品名'])
            ->addColumn('comment', 'text', ['null' => false, 'comment' => 'コメント'])
            ->addColumn('spec', 'text', ['null' => false, 'comment' => '仕様(容量・本数など)'])
            ->addColumn('unit', 'string', ['limit' => 255, 'null' => false, 'comment' => '単位'])
            ->addColumn('price', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => '単価(税別)'])
            ->addColumn('image', 'text', ['null' => true, 'default' => null, 'comment' => '画像パス'])
            ->addColumn('is_discount', 'boolean', ['signed' => false, 'null' => false, 'default' => false, 'comment' => '割引対象商品フラグ [0:非対象,1:対象]'])
            ->addColumn('is_active', 'boolean', ['signed' => false, 'null' => false, 'default' => true, 'comment' => 'レコード状態フラグ [0:無効(削除),1:有効]'])
            ->addColumn('created', 'datetime', ['null' => false, 'comment' => '作成日時'])
            ->addColumn('modified', 'datetime', ['null' => false, 'comment' => '更新日時'])
            ->addForeignKey('product_type_id', 'product_types', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->create();

        // [data] message_templates
        $table = $this->table('message_templates', ['comment' => 'メッセージテンプレート']);
        $table
            ->addColumn('product_type_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '商品タイプID'])
            ->addColumn('message_template', 'text', ['null' => false, 'comment' => 'メッセージ'])
            ->addColumn('is_active', 'boolean', ['signed' => false, 'null' => false, 'default' => true, 'comment' => 'レコード状態フラグ [0:無効(削除),1:有効]'])
            ->addColumn('created', 'datetime', ['null' => false, 'comment' => '作成日時'])
            ->addColumn('modified', 'datetime', ['null' => false, 'comment' => '更新日時'])
            ->addForeignKey('product_type_id', 'product_types', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->create();

        // [data] customers
        $table = $this->table('customers', ['comment' => '会員情報']);
        $table
            ->addColumn('email', 'string', ['limit' => 255, 'null' => false, 'comment' => 'メールアドレス'])
            ->addColumn('password', 'text', ['null' => false, 'comment' => 'パスワード'])
            ->addColumn('billing', 'string', ['limit' => 255, 'null' => false, 'comment' => '請求先(会社名または個人名)'])
            ->addColumn('is_billing_external', 'boolean', ['signed' => false, 'null' => false, 'default' => false, 'comment' => '請求先名外字フラグ [0:外字なし,1:外字あり]'])
            ->addColumn('position', 'string', ['limit' => 255, 'null' => true, 'default' => null, 'comment' => '部署名'])
            ->addColumn('representative', 'string', ['limit' => 255, 'null' => true, 'default' => null, 'comment' => '担当者名'])
            ->addColumn('postal_code', 'string', ['limit' => 10, 'null' => false, 'comment' => '郵便番号'])
            ->addColumn('prefecture_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '都道府県ID'])
            ->addColumn('address', 'string', ['limit' => 255, 'null' => false, 'comment' => '住所(市区町村名)'])
            ->addColumn('address_etc', 'string', ['limit' => 255, 'null' => false, 'comment' => 'その他の住所(番地・ビル名)'])
            ->addColumn('tel', 'string', ['limit' => 20, 'null' => false, 'comment' => '電話番号'])
            ->addColumn('fax', 'string', ['limit' => 20, 'null' => true, 'default' => null, 'comment' => 'ファックス番号'])
            ->addColumn('discount_rate', 'integer', ['signed' => false, 'limit' => 2, 'null' => false, 'default' => 0, 'comment' => '割引率(%)'])
            ->addColumn('closing_day', 'integer', ['signed' => false, 'limit' => 2, 'null' => true, 'default' => null, 'comment' => '締日'])
            ->addColumn('is_formal', 'boolean', ['signed' => false, 'null' => false, 'default' => false, 'comment' => '正会員フラグ [0:仮会員,1:正会員]'])
            ->addColumn('is_active', 'boolean', ['signed' => false, 'null' => false, 'default' => true, 'comment' => 'レコード状態フラグ [0:無効(削除),1:有効]'])
            ->addColumn('created', 'datetime', ['null' => false, 'comment' => '作成日時'])
            ->addColumn('modified', 'datetime', ['null' => false, 'comment' => '更新日時'])
            ->addIndex(['email'], ['unique' => true])
            ->addForeignKey('prefecture_id', 'prefectures', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->create();

        // [data] customer_signup_tokens
        $table = $this->table('customer_signup_tokens', ['comment' => '会員登録トークン']);
        $table
            ->addColumn('customer_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '会員情報ID'])
            ->addColumn('token', 'string', ['limit' => 36, 'null' => false, 'comment' => 'トークン'])
            ->addColumn('expires', 'datetime', ['null' => false, 'comment' => '有効期限'])
            ->addIndex(['customer_id'], ['unique' => true])
            ->create();

        // [data] customer_edit_tokens
        $table = $this->table('customer_edit_tokens', ['comment' => '会員編集トークン']);
        $table
            ->addColumn('customer_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '会員情報ID'])
            ->addColumn('data', 'text', ['null' => false, 'comment' => '編集内容'])
            ->addColumn('token', 'string', ['limit' => 36, 'null' => false, 'comment' => 'トークン'])
            ->addColumn('expires', 'datetime', ['null' => false, 'comment' => '有効期限'])
            ->addIndex(['customer_id'], ['unique' => true])
            ->create();

        // [data] reset_password_tokens
        $table = $this->table('reset_password_tokens', ['comment' => 'リセットパスワードトークン']);
        $table
            ->addColumn('customer_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '会員情報ID'])
            ->addColumn('token', 'string', ['limit' => 36, 'null' => false, 'comment' => 'トークン'])
            ->addColumn('expires', 'datetime', ['null' => false, 'comment' => '有効期限'])
            ->addIndex(['customer_id'], ['unique' => true])
            ->create();

        // [data] orders
        $table = $this->table('orders', ['comment' => '受注情報']);
        $table
            ->addColumn('order_state_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '受注対応状況ID'])
            ->addColumn('payment_type_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '決済方法ID'])
            ->addColumn('delivery', 'string', ['limit' => 255, 'null' => false, 'comment' => 'お届け先'])
            ->addColumn('delivery_type_id', 'integer', ['limit' => 11, 'null' => false, 'default' => 1, 'comment' => 'お届け先タイプID'])
            ->addColumn('facility_id', 'integer', ['limit' => 11, 'null' => true, 'default' => null, 'comment' => '施設ID'])
            ->addColumn('delivery_postal_code', 'string', ['limit' => 10, 'null' => true, 'default' => null, 'comment' => 'お届け先の郵便番号'])
            ->addColumn('delivery_prefecture_id', 'integer', ['limit' => 11, 'null' => true, 'default' => null, 'comment' => 'お届け先の都道府県ID'])
            ->addColumn('delivery_address', 'string', ['limit' => 255, 'null' => true, 'default' => null, 'comment' => 'お届け先の住所(市区町村名)'])
            ->addColumn('delivery_address_etc', 'string', ['limit' => 255, 'null' => true, 'default' => null, 'comment' => 'お届け先のその他の住所(番地・ビル名)'])
            ->addColumn('company', 'string', ['limit' => 255, 'null' => true, 'default' => null, 'comment' => '名札:会社名'])
            ->addColumn('sender', 'string', ['limit' => 255, 'null' => true, 'default' => null, 'comment' => '名札:肩書・差出人名(お名前)'])
            ->addColumn('is_sender_external', 'boolean', ['signed' => false, 'null' => false, 'default' => false, 'comment' => '差出人名外字フラグ [0:外字なし,1:外字あり]'])
            ->addColumn('memo', 'text', ['null' => true, 'default' => null, 'comment' => '備考'])
            ->addColumn('other_detail', 'text', ['null' => true, 'default' => null, 'comment' => 'その他商品詳細欄'])
            ->addColumn('message_type_id', 'integer', ['limit' => 11, 'null' => true, 'default' => null, 'comment' => 'メッセージタイプID'])
            ->addColumn('message_template_id', 'integer', ['limit' => 11, 'null' => true, 'default' => null, 'comment' => 'メッセージテンプレートID'])
            ->addColumn('original_message', 'text', ['null' => true, 'default' => null, 'comment' => 'オリジナルメッセージ'])
            ->addColumn('message_price', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => '購入時のメッセージ料(税別)'])
            ->addColumn('tax_calc', 'string', ['limit' => 255, 'null' => false, 'default' => 'floor', 'comment' => '購入時の消費税の端数計算(処理名) [floor|ceil|round]'])
            ->addColumn('tax_rate', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => '購入時の消費税率(%)'])
            ->addColumn('discount_calc', 'string', ['limit' => 255, 'null' => false, 'default' => 'floor', 'comment' => '購入時の割引の端数計算(処理名) [floor|ceil|round]'])
            ->addColumn('discount_rate', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => '購入時の割引率(%)'])
            ->addColumn('carriage', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => '購入時の送料(税別)'])
            ->addColumn('billing', 'string', ['limit' => 255, 'null' => false, 'comment' => '請求先(会社名または個人名)'])
            ->addColumn('is_billing_external', 'boolean', ['signed' => false, 'null' => false, 'default' => false, 'comment' => '請求先名外字フラグ [0:外字なし,1:外字あり]'])
            ->addColumn('position', 'string', ['limit' => 255, 'null' => true, 'default' => null, 'comment' => '部署名'])
            ->addColumn('representative', 'string', ['limit' => 255, 'null' => true, 'default' => null, 'comment' => '担当者名'])
            ->addColumn('postal_code', 'string', ['limit' => 10, 'null' => false, 'comment' => '郵便番号'])
            ->addColumn('prefecture_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '都道府県ID'])
            ->addColumn('address', 'string', ['limit' => 255, 'null' => false, 'comment' => '住所(市区町村名)'])
            ->addColumn('address_etc', 'string', ['limit' => 255, 'null' => false, 'comment' => 'その他の住所(番地・ビル名)'])
            ->addColumn('tel', 'string', ['limit' => 255, 'null' => false, 'comment' => '電話番号'])
            ->addColumn('fax', 'string', ['limit' => 20, 'null' => true, 'default' => null, 'comment' => 'ファックス番号'])
            ->addColumn('email', 'string', ['limit' => 255, 'null' => false, 'comment' => 'メールアドレス'])
            ->addColumn('customer_id', 'integer', ['limit' => 11, 'null' => true, 'default' => null, 'comment' => '会員情報ID'])
            ->addColumn('is_active', 'boolean', ['signed' => false, 'null' => false, 'default' => true, 'comment' => 'レコード状態フラグ [0:無効(削除),1:有効]'])
            ->addColumn('created', 'datetime', ['null' => false, 'comment' => '作成日時'])
            ->addColumn('modified', 'datetime', ['null' => false, 'comment' => '更新日時'])
            ->addForeignKey('order_state_id', 'order_states', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->addForeignKey('payment_type_id', 'payment_types', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->addForeignKey('delivery_type_id', 'delivery_types', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->addForeignKey('facility_id', 'facilities', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->addForeignKey('delivery_prefecture_id', 'prefectures', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->addForeignKey('message_type_id', 'message_types', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->addForeignKey('message_template_id', 'message_templates', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->addForeignKey('prefecture_id', 'prefectures', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->addForeignKey('customer_id', 'customers', 'id', ['delete'=> 'SET_NULL', 'update'=> 'CASCADE'])
            ->create();

        // [data] orders_products
        $table = $this->table('orders_products', ['comment' => '中間テーブル[受注情報:商品情報]']);
        $table
            ->addColumn('order_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '受注情報ID'])
            ->addColumn('product_id', 'integer', ['limit' => 11, 'null' => false, 'comment' => '商品情報ID'])
            ->addColumn('quantity', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => '数量'])
            ->addColumn('price', 'integer', ['signed' => false, 'limit' => 11, 'null' => false, 'default' => 0, 'comment' => '購入時の単価(税別)'])
            ->addColumn('is_discount', 'boolean', ['signed' => false, 'null' => false, 'default' => false, 'comment' => '購入時の割引対象商品フラグ [0:非対象,1:対象]'])
            ->addColumn('data', 'text', ['null' => false, 'comment' => '購入時の商品情報(シリアライズ)'])
            ->addForeignKey('order_id', 'orders', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addForeignKey('product_id', 'products', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'CASCADE'])
            ->create();

        // [data] administrators
        $table = $this->table('administrators', ['comment' => '管理者']);
        $table
            ->addColumn('username', 'string', ['limit' => 255, 'null' => false, 'comment' => 'ログインID'])
            ->addColumn('password', 'text', ['null' => false, 'comment' => 'パスワード'])
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false, 'comment' => '管理者名'])
            ->addColumn('is_active', 'boolean', ['signed' => false, 'null' => false, 'default' => true, 'comment' => 'レコード状態フラグ [0:無効(削除),1:有効]'])
            ->addColumn('created', 'datetime', ['null' => false, 'comment' => '作成日時'])
            ->addColumn('modified', 'datetime', ['null' => false, 'comment' => '更新日時'])
            ->addIndex(['username'], ['unique' => true])
            ->create();
    }
}
