<?php
return [
    /**
     * Debug Level:
     *
     * Production Mode:
     * false: No error messages, errors, or warnings shown.
     *
     * Development Mode:
     * true: Errors and warnings shown.
     */
    'debug' => filter_var(env('DEBUG', false), FILTER_VALIDATE_BOOLEAN),

    /**
     * Email configuration.
     *
     * By defining transports separately from delivery profiles you can easily
     * re-use transport configuration across multiple profiles.
     *
     * You can specify multiple configurations for production, development and
     * testing.
     *
     * Each transport needs a `className`. Valid options are as follows:
     *
     *  Mail   - Send using PHP mail function
     *  Smtp   - Send using SMTP
     *  Debug  - Do not send the email, just return the result
     *
     * You can add custom transports (or override existing transports) by adding the
     * appropriate file to src/Mailer/Transport.  Transports should be named
     * 'YourTransport.php', where 'Your' is the name of the transport.
     */
    'EmailTransport' => [
        'default' => [
            'className' => 'Mail',
            // The following keys are used in SMTP transports
            'host' => 'localhost',
            'port' => 25,
            'timeout' => 30,
            'username' => 'user',
            'password' => 'secret',
            'client' => null,
            'tls' => null,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        ],
    ],

    /**
     * Email delivery profiles
     *
     * Delivery profiles allow you to predefine various properties about email
     * messages from your application and give the settings a name. This saves
     * duplication across your application and makes maintenance and development
     * easier. Each profile accepts a number of keys. See `Cake\Mailer\Email`
     * for more information.
     */
    'Email' => [
        // 会員登録時
        'signup' => [
            'transport' => 'default',
            'layout' => 'default',
            'template' => 'signup',
            'from' => ['noreply@murata-group.co.jp' => '村田葬儀社'],
            'subject' => '【村田葬儀社】メールアドレス確認',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
        // 会員編集時
        'edit' => [
            'transport' => 'default',
            'layout' => 'default',
            'template' => 'edit',
            'from' => ['noreply@murata-group.co.jp' => '村田葬儀社'],
            'subject' => '【村田葬儀社】会員情報変更確認',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
        // パスワード再発行時
        'forgot' => [
            'transport' => 'default',
            'layout' => 'default',
            'template' => 'forgot',
            'from' => ['noreply@murata-group.co.jp' => '村田葬儀社'],
            'subject' => '【村田葬儀社】パスワード再発行',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
        // 注文完了時(請求先)
        'order_billing' => [
            'transport' => 'default',
            'layout' => 'default',
            'template' => 'order_billing',
            'from' => ['noreply@murata-group.co.jp' => '村田葬儀社'],
            'subject' => '【村田葬儀社】ご注文ありがとうございます',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
        // 注文完了時(管理者)
        'order_admin' => [
            'transport' => 'default',
            'layout' => 'default',
            'template' => 'order_admin',
            'from' => ['noreply@murata-group.co.jp' => '村田葬儀社'],
            'to' => ['you@localhost' => '村田葬儀社'],
            'subject' => '【村田葬儀社】新規受注しました',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
    ],

    /**
     * Connection information used by the ORM to connect
     * to your application's datastores.
     * Drivers include Mysql Postgres Sqlite Sqlserver
     * See vendor\cakephp\cakephp\src\Database\Driver for complete list
     */
    'Datasources' => [
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => 'my_app',
            'password' => 'secret',
            'database' => 'my_app',
            'encoding' => 'utf8',
            'timezone' => 'Asia/Tokyo',
            'flags' => [
                PDO::ATTR_AUTOCOMMIT => false
            ],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
            'url' => env('DATABASE_URL', null),
        ],
        'test' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => 'my_app',
            'password' => 'secret',
            'database' => 'test_myapp',
            'encoding' => 'utf8',
            'timezone' => 'Asia/Tokyo',
            'flags' => [
                PDO::ATTR_AUTOCOMMIT => false
            ],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
            'url' => env('DATABASE_TEST_URL', null),
        ],
    ],
];
