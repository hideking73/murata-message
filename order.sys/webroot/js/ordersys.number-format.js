var numberFormat = function (num) {
    return num.toString().replace(/^\d+[^\.]/, function (t) {
        return t.replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t) {
            return t + ',';
        });
    });
};
var currency = function (num) {
    return '￥' + numberFormat(num);
};
