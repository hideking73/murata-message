(function ($) {
    // 文字数カウント
    var LIMIT = 300;
    var WARNING = 0;
    var messageCount = function (e) {
        var remaining = LIMIT - $(e).val().length;
        $('#message_limit').css({
            'color': remaining < WARNING ? '#de5d50' : ''
        });
        $('#message_count').text(remaining);
    };
    // 描画後
    $(function () {
        messageCount($('[name="original_message"]'));
        $('[name="original_message"]').on('keyup', function () {
            messageCount(this);
        });
    });
})(jQuery);
