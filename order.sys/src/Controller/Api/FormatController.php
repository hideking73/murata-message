<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\ORM\TableRegistry;
use Exception;
use App\Utility\Postal;
use App\Utility\Phone;

/**
 * Format Controller
 */
class FormatController extends AppController
{

    /**
     * Get Postal method
     *
     * @return \Cake\Network\Response|null
     */
    public function getPostal()
    {
        try {
            // Ajaxチェック
            if (!$this->request->is('ajax')) {
                throw new Exception('不正なアクセスです。');
            }
            // 番号チェック
            if (empty($this->request->query('number'))) {
                throw new Exception('パラメータが不正です。');
            }
            // 電話番号ユーティリティ取得
            $postal = new Postal();
            // 番号をフォーマット
            $formated = $postal->getJapanFormat($this->request->query('number'));
            // 成功レスポンス設定
            $result = [
                'status' => true,
                'data' => ['number' => $formated],
                'message' => 'フォーマット済み番号を取得しました。',
            ];
        } catch (Exception $e) {
            // 失敗レスポンス設定
            $result = [
                'status' => false,
                'data' => [],
                'message' => $e->getMessage(),
            ];
        }
        // パラメータ設定
        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }

    /**
     * Get Phone method
     *
     * @return \Cake\Network\Response|null
     */
    public function getPhone()
    {
        try {
            // Ajaxチェック
            if (!$this->request->is('ajax')) {
                throw new Exception('不正なアクセスです。');
            }
            // 番号チェック
            if (empty($this->request->query('number'))) {
                throw new Exception('パラメータが不正です。');
            }
            // 電話番号ユーティリティ取得
            $phone = new Phone();
            // 番号をフォーマット
            $formated = $phone->getJapanFormat($this->request->query('number'));
            // 成功レスポンス設定
            $result = [
                'status' => true,
                'data' => ['number' => $formated],
                'message' => 'フォーマット済み番号を取得しました。',
            ];
        } catch (Exception $e) {
            // 失敗レスポンス設定
            $result = [
                'status' => false,
                'data' => [],
                'message' => $e->getMessage(),
            ];
        }
        // パラメータ設定
        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }
}
