<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Cart Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class CartController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Orders = TableRegistry::get('Orders');
    }

    /**
     * Update Order Session method
     *
     * @return \Cake\Network\Response|null
     */
    public function updateOrderSession()
    {
        try {
            // 方式チェック
            if (!$this->request->is(['ajax', 'patch', 'post', 'put'])) {
                throw new Exception('不正なアクセスです。');
            }
            // 注文エンティティ確認
            if (!$this->request->session()->check('Offering.Orders')) {
                throw new Exception('カート情報がありません。');
            }

            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));

            // カート用配列の調整
            if (isset($this->request->data['products'])) {
                foreach ($this->request->data['products'] as $key => $product) {
                    $this->request->data['products'][$key] = [
                        'id' => $product['_joinData']['product_id'],
                        '_joinData' => $product['_joinData']
                    ];
                }
            }

            // 注文エンティティ更新
            $order = $this->Orders
                ->patchEntity($order, $this->request->data, [
                    'validate' => 'orderCart',
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);

            // 注文エンティティをシリアライズ化してセッション保存
            $this->request->session()->write('Offering.Orders', serialize($order));

            // レスポンス用データ作成
            $data = [
                'tax_in_message_price' => $order->get('tax_in_message_price'),
                'tax_in_subtotal' => $order->get('tax_in_subtotal'),
                'tax_in_carriage' => $order->get('tax_in_carriage'),
                'tax_in_discount' => $order->get('tax_in_discount'),
                'tax_in_total' => $order->get('tax_in_total'),
                'products' => []
            ];
            foreach ($order->get('products') as $key => $product) {
                $data['products'][$key] = [
                    'tax_in_subtotal' => $product->get('_joinData')->get('tax_in_subtotal')
                ];
            }

            // 成功レスポンス設定
            $result = [
                'status' => true,
                'data' => $data,
                'message' => 'カート情報を更新しました。',
            ];
        } catch (Exception $e) {
            // 失敗レスポンス設定
            $result = [
                'status' => false,
                'data' => [],
                'message' => $e->getMessage(),
            ];
        }
        // パラメータ設定
        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }
}
