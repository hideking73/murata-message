<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 */
class CustomersController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Customers = TableRegistry::get('Customers');
    }

    /**
     * Get Customer method
     *
     * @return \Cake\Network\Response|null
     */
    public function getCustomer()
    {
        try {
            // Ajaxチェック
            if (!$this->request->is('ajax')) {
                throw new Exception('不正なアクセスです。');
            }
            // 認証チェック
            if (!$this->request->session()->check('Auth.Administrator')) {
                throw new Exception('アクセス権限がありません。');
            }
            // 会員IDチェック
            if (empty($this->request->query('customer_id'))) {
                throw new Exception('パラメータが不正です。');
            }
            // 対象エンティティ取得
            $customer = $this->Customers
                ->find('active')
                ->find('formal')
                ->where(['Customers.id' => $this->request->query('customer_id')])
                ->first();
            // 対象エンティティ取得判定
            if (!$customer) {
                throw new Exception('会員情報を取得できませんでした。');
            }
            // 成功レスポンス設定
            $result = [
                'status' => true,
                'data' => $customer->toArray(),
                'message' => '会員情報を取得しました。',
            ];
        } catch (Exception $e) {
            // 失敗レスポンス設定
            $result = [
                'status' => false,
                'data' => [],
                'message' => $e->getMessage(),
            ];
        }
        // パラメータ設定
        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }
}
