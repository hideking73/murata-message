<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Exception;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Products = TableRegistry::get('Products');
    }

    /**
     * Get Product method
     *
     * @return \Cake\Network\Response|null
     */
    public function getProduct()
    {
        try {
            // Ajaxチェック
            if (!$this->request->is('ajax')) {
                throw new Exception('不正なアクセスです。');
            }
            // 認証チェック
            if (!$this->request->session()->check('Auth.Administrator')) {
                throw new Exception('アクセス権限がありません。');
            }
            // 商品IDチェック
            if (empty($this->request->query('product_id'))) {
                throw new Exception('パラメータが不正です。');
            }
            // 対象エンティティ取得
            $product = $this->Products
                ->find('active')
                ->where(['Products.id' => $this->request->query('product_id')])
                ->first();
            // 対象エンティティ取得判定
            if (!$product) {
                throw new Exception('商品情報を取得できませんでした。');
            }
            // "その他"商品の場合、任意の価格を設定
            if ($product->get('product_type_id') == Configure::read('ProductType.other')) {
                $product->set(['price' => $this->request->query('price')]);
            }
            // 成功レスポンス設定
            $result = [
                'status' => true,
                'data' => $product->toArray(),
                'message' => '商品情報を取得しました。',
            ];
        } catch (Exception $e) {
            // 失敗レスポンス設定
            $result = [
                'status' => false,
                'data' => [],
                'message' => $e->getMessage(),
            ];
        }
        // パラメータ設定
        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }
}
