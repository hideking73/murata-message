<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Cake\Mailer\MailerAwareTrait;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\NotFoundException;
use Exception;
use DateTime;

use App\Form\ForgotForm;

/**
 * Member Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\PrefecturesTable $Prefectures
 */
class MemberController extends AppController
{
    use MailerAwareTrait;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['signup', 'activation', 'login', 'forgot', 'resetPassword']);

        $this->Customers = TableRegistry::get('Customers');
        $this->Orders = TableRegistry::get('Orders');
        $this->Prefectures = TableRegistry::get('Prefectures');
    }

    /**
     * Login method
     *
     * @return \Cake\Network\Response|null
     */
    public function login()
    {
        // ログイン済みの場合
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        // ログイン処理
        if ($this->request->is('post')) {
            // ログイン情報判定
            if (!$login = $this->Auth->identify()) {
                $this->Flash->error(__('ログインできませんでした。' . PHP_EOL . '入力内容に誤りがないかご確認ください。'));
            }

            // ログイン情報セッションセット
            $this->Auth->setUser($login);

            // パスワードリセットトークン確認
            if (!is_null($login['customer_signup_token'])) {
                // パスワードリセット画面へリダイレクト
                return $this->redirect([
                    'prefix' => false,
                    'controller' => 'Member',
                    'action' => 'reset_password',
                    $login['customer_signup_token']['token']
                ]);
            }

            // 通常のログイン後リダイレクト
            return $this->redirect($this->Auth->redirectUrl());
        }
    }

    /**
     * Logout method
     *
     * @return \Cake\Network\Response|null
     */
    public function logout()
    {
        // 注文セッション削除
        $this->request->session()->delete('Offering');
        // ログアウト処理
        $this->Flash->success(__('ログアウトしました。'));
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Sign Up method
     *
     * @return \Cake\Network\Response|null
     */
    public function signup()
    {
        // 会員エンティティ取得
        if ($this->request->session()->check('Signup')) {
            // 注文時に作成した会員エンティティのセッションから取得
            $customer = unserialize($this->request->session()->read('Signup'));
        } else {
            // 会員エンティティ新規取得
            $customer = $this->Customers
                ->newEntity();
        }
        $this->set(compact('customer'));

        // 選択肢データのエンティティ取得
        $prefectures = $this->Prefectures
            ->find('list');
        $this->set(compact('prefectures'));

        // 会員登録処理
        if ($this->request->is('post')) {
            // 会員エンティティのバリデーション実行
            $customer = $this->Customers
                ->patchEntity($customer, $this->request->data, [
                    'validate' => 'signup',
                ]);
            $this->set(compact('customer'));

            // 会員エンティティのバリデーションエラー確認
            if (!empty($customer->errors())) {
                $this->Flash->error(__('入力項目に誤りがあります。'));
                return $this->render();
            }

            // "確認画面"表示
            if (isset($this->request->data['confirm'])) {
                return $this->render('signup_confirm');
            }

            // "完了画面"表示
            if (isset($this->request->data['complete'])) {
                // トランザクション開始
                $connection = ConnectionManager::get('default');
                $connection->begin();

                // 保存処理
                try {
                    // トークン
                    $this->request->data['customer_signup_token'] = [
                        'token' => Text::uuid(),
                        'expires' => new Datetime(Configure::read('Token.expires'))
                    ];

                    // 会員エンティティのバリデーション実行
                    $customer = $this->Customers
                        ->patchEntity($customer, $this->request->data, [
                            'validate' => false
                        ]);
                    $this->set(compact('customer'));

                    // 会員エンティティの保存処理
                    if (!$this->Customers->save($customer)) {
                        throw new BadRequestException('会員登録に失敗しました。はじめから登録をお願いします。');
                    }

                    // メール送信
                    $this->getMailer('Customer')
                        ->send('signup', [$customer]);

                    // 注文時に作成した会員エンティティのセッションを削除
                    $this->request->session()->delete('Signup');

                    // トランザクション:コミット
                    $connection->commit();
                } catch (Exception $e) {
                    // トランザクション:ロールバック
                    $connection->rollback();

                    // "会員登録画面"トップへ
                    $this->Flash->error(__($e->getMessage()));
                    return $this->redirect(['action' => 'signup']);
                }
                return $this->render('signup_complete');
            }
        }
    }

    /**
     * Activation method
     *
     * @return \Cake\Network\Response|null
     */
    public function activation()
    {
        // トークン取得
        $token = $this->request->query('token');

        // 会員エンティティを取得
        $customer = $this->Customers
            ->find('activation', compact('token'))
            ->first();

        // 会員エンティティを確認
        if (!$customer) {
            throw new NotFoundException();
        }

        // トークンのエンティティ取得
        $customer_signup_token = $this->Customers->CustomerSignupTokens
            ->get($customer->get('token_id'));

        // トランザクション開始
        $connection = ConnectionManager::get('default');
        $connection->begin();

        // 保存処理
        try {
            // 有効期限確認
            if ($customer_signup_token->get('expires') < new DateTime('now')) {
                // メールアドレスの先頭に削除日を追加(UNIQUE対策)
                $email = '[Delete:' . $customer->get('modified')->format('YmdHis') . ']' . $customer->get('email');
                $customer->set(['email' => $email]);

                // 有効期限を超過した会員は無効化(トークンは削除)
                $this->Customers->passive($customer);
                $this->Customers->CustomerSignupTokens->delete($customer_signup_token);
            }

            // 会員を正会員化
            if (!$this->Customers->formal($customer)) {
                throw new BadRequestException('会員の有効化に失敗しました。');
            }

            // トークン削除
            if (!$this->Customers->CustomerSignupTokens->delete($customer_signup_token)) {
                throw new BadRequestException('トークンの削除に失敗しました。');
            }

            // トランザクション:コミット
            $connection->commit();
        } catch (Exception $e) {
            // トランザクション:ロールバック
            $connection->rollback();

            // アクティベートエラー
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Home',
                'action' => 'index'
            ]);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {}

    /**
     * History method
     *
     * @return \Cake\Network\Response|null
     */
    public function history()
    {
        // ページネーション設定取得
        $settings = [
            'maxLimit' => 5,
            'order' => [
                'Orders.created' => 'desc'
            ]
        ];
        // 一覧取得用クエリ生成
        $query = $this->Orders
            ->find('active')
            ->find('detail')
            ->where(['Orders.customer_id' => $this->Auth->user('id')]);
        // 対象エンティティ取得
        $orders = $this->paginate($query, $settings);
        $this->set(compact('orders'));
    }

    /**
     * Edit method
     *
     * @return \Cake\Network\Response|null
     */
    public function edit()
    {
        // 仮変更中かどうか確認
        $customer_edit_token = $this->Customers->CustomerEditTokens
            ->find()
            ->where(['CustomerEditTokens.customer_id' => $this->Auth->user('id')])
            ->first();
        if ($customer_edit_token) {
            // 変更不可
            $this->Flash->error(__('現在仮変更中です。メールをご確認ください。'));
            return $this->redirect(['action' => 'index']);
        }

        // 会員エンティティ取得
        $customer = $this->Customers
            ->find('ignorePassword')
            ->where(['Customers.id' => $this->Auth->user('id')])
            ->first();
        $this->set(compact('customer'));

        // 選択肢データのエンティティ取得
        $prefectures = $this->Prefectures
            ->find('list');
        $this->set(compact('prefectures'));

        // 会員更新処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // "確認画面"
            if (isset($this->request->data['confirm'])) {
                // 会員エンティティのバリデーション実行
                $customer = $this->Customers
                    ->patchEntity($customer, $this->request->data, [
                        'validate' => 'edit',
                    ]);
                $this->set(compact('customer'));

                // 会員エンティティのバリデーションエラー確認
                if (!empty($customer->errors())) {
                    $this->Flash->error(__('入力項目に誤りがあります。'));
                    return $this->render();
                }
                // "確認画面"表示
                return $this->render('edit_confirm');
            }

            // "完了画面"
            if (isset($this->request->data['complete'])) {
                // トランザクション開始
                $connection = ConnectionManager::get('default');
                $connection->begin();

                // 保存処理
                try {
                    // 会員エンティティのバリデーション実行
                    $customer = $this->Customers
                        ->patchEntity($customer, $this->request->data, [
                            'validate' => false,
                        ]);
                    $this->set(compact('customer'));

                    // 会員エンティティのバリデーション実行
                    $customer_edit_token = $this->Customers->CustomerEditTokens
                        ->newEntity([
                            'customer_id' => $customer->get('id'),
                            'token' => Text::uuid(),
                            'expires' => new Datetime(Configure::read('Token.expires')),
                            'data' => serialize($customer)
                        ]);

                    // 会員エンティティの保存処理
                    if (!$this->Customers->CustomerEditTokens->save($customer_edit_token)) {
                        throw new BadRequestException('仮変更に失敗しました。はじめから変更をお願いします。');
                    }

                    // メール送信
                    $this->getMailer('Customer')
                        ->send('edit', [$customer, $customer_edit_token]);

                    // トランザクション:コミット
                    $connection->commit();
                } catch (Exception $e) {
                    // トランザクション:ロールバック
                    $connection->rollback();

                    // "会員変更画面"トップへ
                    $this->Flash->error(__($e->getMessage()));
                    return $this->redirect(['action' => 'edit']);
                }
                // "完了画面"表示
                return $this->render('edit_complete');
            }
        }
    }

    /**
     * Apply method
     *
     * @return \Cake\Network\Response|null
     */
    public function apply()
    {
        // トークン取得
        $token = $this->request->query('token');

        // 会員エンティティを取得
        $customer = $this->Customers
            ->find('apply', compact('token'))
            ->first();

        // 会員エンティティを確認
        if (!$customer) {
            throw new NotFoundException();
        }

        // トークンのエンティティ取得
        $customer_edit_token = $this->Customers->CustomerEditTokens
            ->get($customer->get('token_id'));

        // トランザクション開始
        $connection = ConnectionManager::get('default');
        $connection->begin();

        // 保存処理
        try {
            // 有効期限確認
            if ($customer_edit_token->get('expires') < new DateTime('now')) {
                // 有効期限を超過したトークンを削除
                $this->Customers->CustomerEditTokens->delete($customer_edit_token);
            }

            // 変更を適用
            $customer = unserialize($customer_edit_token->get('data'));
            if (!$this->Customers->save($customer)) {
                throw new BadRequestException('会員情報の変更に失敗しました。');
            }

            // トークン削除
            if (!$this->Customers->CustomerEditTokens->delete($customer_edit_token)) {
                throw new BadRequestException('トークンの削除に失敗しました。');
            }

            // トランザクション:コミット
            $connection->commit();
        } catch (Exception $e) {
            // トランザクション:ロールバック
            $connection->rollback();

            // 変更適用エラー
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect([
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'index'
            ]);
        }
    }

    /**
     * Leave method
     *
     * @return \Cake\Network\Response|null
     */
    public function leave()
    {
        // 会員エンティティ取得
        $customer = $this->Customers
            ->find('ignorePassword')
            ->where(['Customers.id' => $this->Auth->user('id')])
            ->first();

        // トランザクション開始
        $connection = ConnectionManager::get('default');
        $connection->begin();

        // 退会処理
        try {
            // メールアドレスの先頭に削除日を追加(UNIQUE対策)
            $email = '[Delete:' . $customer->get('modified')->format('YmdHis') . ']' . $customer->get('email');
            $customer->set(['email' => $email]);

            // 会員エンティティの無効化
            if (!$this->Customers->passive($customer)) {
                throw new BadRequestException('会員情報の削除に失敗しました。確認してやりなおしてください。');
            }

            // トランザクション:コミット
            $connection->commit();

            // 退会完了後ログアウトする
            $this->Flash->success(__('正常に会員情報を削除し、退会完了しました。'));
            return $this->redirect($this->Auth->logout());
        } catch (Exception $e) {
            // トランザクション:ロールバック
            $connection->rollback();

            // 退会失敗
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Forgot method
     *
     * @return \Cake\Network\Response|null
     */
    public function forgot()
    {
        // フォームのインスタンスを取得
        $forgot = new ForgotForm();
        $this->set(compact('forgot'));

        // リセットパスワードトークン発行処理
        if (!$this->request->is('post')) {
            return $this->render();
        }

        // 取得したパラメータに対してバリデーション実行
        if (!$forgot->execute($this->request->data)) {
            $this->Flash->error(__('入力項目に誤りがあります。'));
            return $this->render();
        }

        // 会員エンティティ取得
        $customer = $this->Customers
            ->find('ignorePassword')
            ->where(['Customers.email' => $this->request->data('email')])
            ->first();

        // トークン発行
        if ($customer) {
            // トークンのエンティティ取得
            $reset_password_token = $this->Customers->ResetPasswordTokens
                ->newEntity([
                    'customer_id' => $customer->get('id'),
                    'token' => Text::uuid(),
                    'expires' => new Datetime(Configure::read('Token.expires'))
                ]);

            // トランザクション開始
            $connection = ConnectionManager::get('default');
            $connection->begin();

            // 保存処理
            try {
                // トークン保存
                if (!$this->Customers->ResetPasswordTokens->save($reset_password_token)) {
                    throw new BadRequestException('トークンの作成に失敗しました。');
                }

                // メール送信
                $this->getMailer('Customer')
                    ->send('forgot', [$customer, $reset_password_token]);

                // トランザクション:コミット
                $connection->commit();
            } catch (Exception $e) {
                // トランザクション:ロールバック
                $connection->rollback();
            }
        }

        // [重要]不正アクセス対策のため、該当データがなくてもエラー表示せず完了画面を表示する
        $this->render('forgot_complete');
    }

    /**
     * Reset Password method
     *
     * @return \Cake\Network\Response|null
     */
    public function resetPassword()
    {
        // トークン取得
        $token = $this->request->query('token');

        // 会員エンティティを取得
        $customer = $this->Customers
            ->find('resetPassword', compact('token'))
            ->first();
        $this->set(compact('customer'));

        // 会員エンティティを確認
        if (!$customer) {
            // 無い場合はログイン画面へ
            return $this->redirect([
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'login'
            ]);
        }

        // トークンのエンティティ取得
        $reset_password_token = $this->Customers->ResetPasswordTokens
            ->get($customer->get('token_id'));

        // 有効期限確認
        if ($reset_password_token->get('expires') < new DateTime('now')) {
            // 有効期限を超過したトークンを削除
            $this->Customers->ResetPasswordTokens->delete($reset_password_token);
            // 再ログインを促す
            $this->Flash->error(__('再度ログインしてください。'));
            return $this->redirect($this->Auth->logout());
        }

        // パスワード入力画面
        if (!$this->request->is(['patch', 'post', 'put'])) {
            return $this->render();
        }

        // トランザクション開始
        $connection = ConnectionManager::get('default');
        $connection->begin();

        // 保存処理
        try {
            // 会員エンティティのバリデーション実行
            $customer = $this->Customers
                ->patchEntity($customer, $this->request->data, [
                    'validate' => 'resetPassword'
                ]);
            $this->set(compact('customer'));

            // 会員エンティティのバリデーションエラー確認
            if (!$this->Customers->save($customer)) {
                throw new BadRequestException('入力項目に誤りがあります。');
            }

            // トークン削除
            if (!$this->Customers->ResetPasswordTokens->delete($reset_password_token)) {
                throw new BadRequestException('トークンの削除に失敗しました。');
            }

            // トランザクション:コミット
            $connection->commit();

            // ログイン済みの場合はパスワード変更後に強制ログアウトする
            $this->Auth->logout();

            // 完了画面表示
            return $this->render('reset_password_complete');
        } catch (Exception $e) {
            // トランザクション:ロールバック
            $connection->rollback();

            // パスワード再発行エラー
            $this->Flash->error(__($e->getMessage()));
            return $this->render();
        }
    }
}
