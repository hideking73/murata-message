<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
use Cake\Utility\Hash;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\I18n\Number;
use DateTime;
use FPDI;
use TCPDF_FONTS;

use App\Form\OrderSearchForm;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\OrderStatesTable $OrderStates
 * @property \App\Model\Table\PaymentTypesTable $PaymentTypes
 * @property \App\Model\Table\MessageTypesTable $MessageTypes
 * @property \App\Model\Table\MessageTemplatesTable $MessageTemplates
 * @property \App\Model\Table\PrefecturesTable $Prefectures
 * @property \App\Model\Table\DeliveryTypesTable $DeliveryTypes
 * @property \App\Model\Table\FacilitiesTable $Facilities
 * @property \App\Model\Table\ProductsTable $Products
 * @property \App\Model\Table\CustomersTable $Customers
 */
class OrdersController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Orders = TableRegistry::get('Orders');
        $this->OrderStates = TableRegistry::get('OrderStates');
        $this->PaymentTypes = TableRegistry::get('PaymentTypes');
        $this->MessageTypes = TableRegistry::get('MessageTypes');
        $this->MessageTemplates = TableRegistry::get('MessageTemplates');
        $this->Prefectures = TableRegistry::get('Prefectures');
        $this->DeliveryTypes = TableRegistry::get('DeliveryTypes');
        $this->Facilities = TableRegistry::get('Facilities');
        $this->Products = TableRegistry::get('Products');
        $this->Customers = TableRegistry::get('Customers');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // ページネーション設定取得
        $settings = [
            'maxLimit' => 20,
            'order' => [
                'Orders.created' => 'desc'
            ]
        ];

        // 一覧取得用クエリ生成
        $query = $this->Orders
            ->find('active')
            ->find('detail');

        // 検索フォームのインスタンスを取得
        $order_search = new OrderSearchForm();
        // GETクエリパラメータ取得
        if ($this->request->query) {
            // 表示用データに追加
            $this->request->data = $this->request->query;
            // パラメータのバリデーション
            $errors = [];
            if (!$order_search->execute($this->request->query)) {
                $errors = $order_search->errors();
            }
        }
        $this->set(compact('order_search'));

        // GETクエリパラメータ確認
        if ($this->request->query('delivery')) {
            $query->where([
                'Orders.delivery LIKE' => '%' . $this->request->query('delivery') . '%'
            ]);
        }
        if ($this->request->query('billing')) {
            $query->where([
                'Orders.billing LIKE' => '%' . $this->request->query('billing') . '%'
            ]);
        }
        if ($this->request->query('facility') && $this->request->query('facility') != 'none') {
            $query->where([
                'Orders.delivery_type_id' => Configure::read('DeliveryType.murata'),
                'Orders.facility_id' => $this->request->query('facility')
            ]);
        }
        if ($this->request->query('facility') && $this->request->query('facility') == 'none') {
            $query->where([
                'Orders.delivery_type_id' => Configure::read('DeliveryType.other')
            ]);
        }
        if ($this->request->query('created_from.year') && $this->request->query('created_from.month') && $this->request->query('created_from.day') && !isset($errors['created_from'])) {
            $created_from = new Datetime();
            $created_from->setDate($this->request->query('created_from.year'), $this->request->query('created_from.month'), $this->request->query('created_from.day'));
            $query->where([
                'Orders.created >= ' => $created_from->format('Y-m-d 00:00:00')
            ]);
        }
        if ($this->request->query('created_to.year') && $this->request->query('created_to.month') && $this->request->query('created_to.day') && !isset($errors['created_to'])) {
            $created_to = new Datetime();
            $created_to->setDate($this->request->query('created_to.year'), $this->request->query('created_to.month'), $this->request->query('created_to.day'));
            $query->where([
                'Orders.created <= ' => $created_to->format('Y-m-d 23:59:59')
            ]);
        }

        // 対象エンティティ取得
        $orders = $this->paginate($query, $settings);

        // 表示用パラメータセット
        $this->set(compact('orders'));
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null
     */
    public function view($id = null)
    {
        // 選択肢データ取得
        $order_states = $this->OrderStates->find('list');
        $this->set(compact('order_states'));
        // 対象エンティティ取得
        $order = $this->Orders
            ->find('active')
            ->find('detail')
            ->where(['Orders.id' => $id])
            ->first();
        $this->set(compact('order'));
        // 対象エンティティ取得判定
        if (!$order) {
            throw new NotFoundException(__('データがありません。'));
        }
        // 保存処理(受注状況変更)
        if ($this->request->is(['patch', 'post', 'put'])) {
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ更新
            $order->set(['order_state_id' => $this->request->data['order_state_id']]);
            // 対象エンティティ保存
            if (!$order = $this->Orders->save($order)) {
                throw new BadRequestException(__('更新に失敗しました。確認してやりなおしてください。'));
            }
            $this->set(compact('order'));
            // 更新完了
            $this->Connection->commit();
            $this->Flash->success(__('受注対応状況を更新しました。'));
        }
    }

    /**
     * Add method
     *
     * @param string|null $customer_id Customer id.
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($customer_id = null)
    {
        // 選択肢データ取得
        $order_states = $this->OrderStates->find('list');
        $payment_types = $this->PaymentTypes->find('list');
        $message_types = $this->MessageTypes->find('list');
        $message_templates = $this->MessageTemplates->find('active')->find('list');
        $prefectures = $this->Prefectures->find('list');
        $delivery_types = $this->DeliveryTypes->find('list');
        $facilities = $this->Facilities->find('list');
        $products = $this->Products->find('active')->find('list');
        $customers = $this->Customers->find('active')->find('formal')->find('list');
        $this->set(compact('order_states', 'payment_types', 'message_types', 'message_templates', 'prefectures', 'delivery_types', 'facilities', 'products', 'customers'));
        // 対象エンティティ取得
        $order = $this->Orders
            ->newEntity(null, [
                'associated' => [
                    'Products' => [
                        'validate' => false
                    ],
                    'Products._joinData'
                ]
            ]);
        $this->set(compact('order'));
        // JS用商品配列作成
        $json_products = json_encode(Hash::combine($order->toArray(), 'products.{n}._joinData.product_id', 'products.{n}._joinData.quantity'));
        $json_prices = json_encode(Hash::combine($order->toArray(), 'products.{n}._joinData.product_id', 'products.{n}._joinData.price'));
        $this->set(compact('json_products', 'json_prices'));
        // 保存処理
        if ($this->request->is('post')) {
            // belongsToMany対応
            foreach ($this->request->data['products'] as $key => $product) {
                $this->request->data['products'][$key]['id'] = $product['_joinData']['product_id'];
            }
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ更新(バリデーション実行)
            $order = $this->Orders
                ->patchEntity($order, $this->request->data, [
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);
            $this->set(compact('order'));
            // 対象エンティティ保存
            if (!$order = $this->Orders->save($order)) {
                throw new BadRequestException(__('登録に失敗しました。確認してやりなおしてください。'));
            }
            // 登録完了
            $this->Connection->commit();
            $this->Flash->success(__('受注情報を登録しました。'));
            return $this->redirect(['action' => 'view', $order->get('id')]);
        }
        // 会員情報から新規作成する場合の初期値設定
        if ($this->request->is('get') && $this->Customers->exists(['id' => $customer_id])) {
            $order->customer_id = $customer_id;
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        // 選択肢データ取得
        $order_states = $this->OrderStates->find('list');
        $payment_types = $this->PaymentTypes->find('list');
        $message_types = $this->MessageTypes->find('list');
        $message_templates = $this->MessageTemplates->find('active')->find('list');
        $prefectures = $this->Prefectures->find('list');
        $delivery_types = $this->DeliveryTypes->find('list');
        $facilities = $this->Facilities->find('list');
        $products = $this->Products->find('active')->find('list');
        $customers = $this->Customers->find('active')->find('formal')->find('list');
        $this->set(compact('order_states', 'payment_types', 'message_types', 'message_templates', 'prefectures', 'delivery_types', 'facilities', 'products', 'customers'));
        // 対象エンティティ取得
        $order = $this->Orders
            ->find('active')
            ->find('detail')
            ->where(['Orders.id' => $id])
            ->first();
        $this->set(compact('order'));
        // JS用商品配列作成
        $json_products = json_encode(Hash::combine($order->toArray(), 'products.{n}._joinData.product_id', 'products.{n}._joinData.quantity'));
        $json_prices = json_encode(Hash::combine($order->toArray(), 'products.{n}._joinData.product_id', 'products.{n}._joinData.price'));
        $this->set(compact('json_products', 'json_prices'));
        // 対象エンティティ取得判定
        if (!$order) {
            throw new NotFoundException(__('データがありません。'));
        }
        // 保存処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // belongsToMany対応
            foreach ($this->request->data['products'] as $key => $product) {
                $this->request->data['products'][$key]['id'] = $product['_joinData']['product_id'];
            }
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ更新(バリデーション実行)
            $order = $this->Orders
                ->patchEntity($order, $this->request->data, [
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);
            // 対象エンティティ保存
            if (!$order = $this->Orders->save($order)) {
                throw new BadRequestException(__('更新に失敗しました。確認してやりなおしてください。'));
            }
            $this->set(compact('order'));
            // 更新完了
            $this->Connection->commit();
            $this->Flash->success(__('受注情報を更新しました。'));
            return $this->redirect(['action' => 'view', $order->get('id')]);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // 対象エンティティ取得
        $order = $this->Orders
            ->find('active')
            ->where(['Orders.id' => $id])
            ->first();
        // 対象エンティティ取得判定
        if (!$order) {
            throw new NotFoundException(__('データがありません。'));
        }
        // 削除処理
        if ($this->request->is(['post', 'delete'])) {
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ削除
            if (!$order = $this->Orders->passive($order)) {
                throw new BadRequestException(__('削除に失敗しました。確認してやりなおしてください。'));
            }
            // 削除完了
            $this->Connection->commit();
            $this->Flash->success(__('受注情報を削除しました。'));
        }
        // リダイレクト
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Message Print method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function messagePrint($id = null)
    {
        set_time_limit(120);
        ini_set('memory_limit', '128M');

        // 対象エンティティ取得
        $order = $this->Orders
            ->find('active')
            ->find('detail')
            ->where(['Orders.id' => $id])
            ->first();
        $this->set(compact('order'));

        // テンプレートPDF取得
        $template_path = WWW_ROOT . 'pdf' . DS . 'message_print.pdf';
        $template_file = new File($template_path);
        if (!$template_file->exists()) {
            throw new NotFoundException(__('テンプレートが見つかりません'));
        }

        // フォントディレクトリ取得
        $font_dir = WWW_ROOT . 'fonts' . DS;

        // 組み込みフォント取得
        $regular_font_path = $font_dir . 'ipamp.ttf';
        $regular_font_file = new File($regular_font_path);
        if (!$regular_font_file->exists()) {
            throw new NotFoundException(__('フォントが見つかりません'));
        }
        $regular_font_php = $font_dir . 'ipamp.php';

        // PDF設定
        class_exists('TCPDF', true);    // Composer版を使う場合は必須
        $pdf = new FPDI();

        // フォントを登録
        $tcpdf_font = new TCPDF_FONTS();
        $regular_font = $tcpdf_font->addTTFfont($regular_font_path, '', '', 32, $font_dir);

        // フォントサブセット有効
        $pdf->setFontSubsetting(true);

        // ページ設定
        $pdf->SetMargins(0, 0, 0);      // 余白(上左右)を設定
        $pdf->setPrintHeader(false);    // ヘッダーの出力を無効化
        $pdf->setPrintFooter(false);    // フッターの出力を無効化
        $pdf->SetCellPadding(0);        // セルパディングを設定
        $pdf->setCellHeightRatio(1.8);  // セルの行間を設定

        // ページを追加
        $pdf->AddPage();

        // テンプレートPDFを設定
        $pdf->setSourceFile($template_path);    // テンプレートPDFを設定
        $tplIdx = $pdf->importPage(1);          // テンプレートPDFの1ページ目を取得

        // 取得したテンプレートPDFの1ページ目をテンプレートとして設定
        $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

        // カーソル位置を指定して文字"お届け先"を書き込む
        $text = $order->get('delivery') . __('様');
        $pdf->SetFont($regular_font, '', 12, $regular_font_php);    // 書き込む文字列のフォントを指定
        $pdf->SetTextColor(0, 0, 0);                                // 書き込む文字列の文字色を指定
        $pdf->SetXY(21, 23);                                        // カーソル位置指定(X:21mm/Y:23mm)
        $pdf->Write(0, $text);                                      // カーソル位置に文字列を書き込む

        // カーソル位置を指定して文字"宛先(固定値)"を書き込む
        $text = __('ご遺族様');
        $pdf->SetFont($regular_font, '', 12, $regular_font_php);    // 書き込む文字列のフォントを指定
        $pdf->SetTextColor(0, 0, 0);                                // 書き込む文字列の文字色を指定
        $pdf->SetXY(21, 30);                                        // カーソル位置指定(X:21mm/Y:30mm)
        $pdf->Write(0, $text);                                      // カーソル位置に文字列を書き込む

        // カーソル位置を指定して文字"メッセージ"を書き込む
        $text = $order->get('message');
        $pdf->SetFont($regular_font, '', 12, $regular_font_php);    // 書き込む文字列のフォントを指定
        $pdf->SetTextColor(0, 0, 0);                                // 書き込む文字列の文字色を指定
        $pdf->SetXY(21, 46);                                        // カーソル位置指定(X:21mm/Y:46mm)
        $pdf->MultiCell(174, 0, $text);                             // カーソル位置にセルを書き込む

        // カーソル位置を指定して文字"名札:会社名"を書き込む
        $text = $order->get('company');
        $pdf->SetFont($regular_font, '', 12, $regular_font_php);    // 書き込む文字列のフォントを指定
        $pdf->SetTextColor(0, 0, 0);                                // 書き込む文字列の文字色を指定
        $pdf->SetXY(21, 119);                                       // カーソル位置指定(X:21mm/Y:119mm)
        $pdf->Write(0, $text);                                      // カーソル位置に文字列を書き込む

        // カーソル位置を指定して文字"名札:肩書・差出人名(お名前)"を書き込む
        $text = $order->get('sender');
        $pdf->SetFont($regular_font, '', 12, $regular_font_php);    // 書き込む文字列のフォントを指定
        $pdf->SetTextColor(0, 0, 0);                                // 書き込む文字列の文字色を指定
        $pdf->SetXY(21, 126);                                       // カーソル位置指定(X:21mm/Y:126mm)
        $pdf->Write(0, $text);                                      // カーソル位置に文字列を書き込む

        // テンプレート変数に出力データを設定
        $this->set('content', $pdf->Output('', 'S'));

        // PDF出力設定
        $this->viewBuilder()->layout(false);
        $this->RequestHandler->respondAs('pdf', [
            'charset' => 'UTF-8',
            'attachment' => 'message_' . $order->get('created')->format('Ymd_Hi') . '.pdf'
        ]);
    }

    /**
     * Export method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function export()
    {
        set_time_limit(120);
        ini_set('memory_limit', '128M');

        // CSV出力設定
        $_newline = "\r\n";
        $_dataEncoding = 'UTF-8';
        // $_csvEncoding = 'SJIS-win';
        $_csvEncoding = 'SJIS'; // libiconvのSJIS-win非対応環境用にSJIS出力
        // CSVヘッダー設定
        $_header = [
            'ＩＤ', '受注日', '受注商品', '合計(税込)', '状況', '支払い方法', '備考', 'お届け先', 'お届け先タイプ', 'お届け先:施設', 'お届け先:郵便番号', 'お届け先:住所', '請求先:会社名(個人名)', '請求先:部署名', '請求先:担当者名', '請求先:郵便番号', '請求先:住所', '請求先:TEL', '請求先:FAX', '請求先:メール', '会員/非会員', '名札', 'メッセージ'
        ];

        // データ取得用クエリ生成
        $query = $this->Orders
            ->find('exportCsv');

        // 検索フォームのインスタンスを取得
        $order_search = new OrderSearchForm();
        // GETクエリパラメータ取得
        if ($this->request->query) {
            // パラメータのバリデーション
            $errors = [];
            if (!$order_search->execute($this->request->query)) {
                $errors = $order_search->errors();
            }
        }

        // GETクエリパラメータ確認
        if ($this->request->query('delivery')) {
            $query->where([
                'Orders.delivery LIKE' => '%' . $this->request->query('delivery') . '%'
            ]);
        }
        if ($this->request->query('billing')) {
            $query->where([
                'Orders.billing LIKE' => '%' . $this->request->query('billing') . '%'
            ]);
        }
        if ($this->request->query('facility') && $this->request->query('facility') != 'none') {
            $query->where([
                'Orders.delivery_type_id' => Configure::read('DeliveryType.murata'),
                'Orders.facility_id' => $this->request->query('facility')
            ]);
        }
        if ($this->request->query('facility') && $this->request->query('facility') == 'none') {
            $query->where([
                'Orders.delivery_type_id' => Configure::read('DeliveryType.other')
            ]);
        }
        if ($this->request->query('created_from.year') && $this->request->query('created_from.month') && $this->request->query('created_from.day') && !isset($errors['created_from'])) {
            $created_from = new Datetime();
            $created_from->setDate($this->request->query('created_from.year'), $this->request->query('created_from.month'), $this->request->query('created_from.day'));
            $query->where([
                'Orders.created >= ' => $created_from->format('Y-m-d 00:00:00')
            ]);
        }
        if ($this->request->query('created_to.year') && $this->request->query('created_to.month') && $this->request->query('created_to.day') && !isset($errors['created_to'])) {
            $created_to = new Datetime();
            $created_to->setDate($this->request->query('created_to.year'), $this->request->query('created_to.month'), $this->request->query('created_to.day'));
            $query->where([
                'Orders.created <= ' => $created_to->format('Y-m-d 23:59:59')
            ]);
        }

        // 対象エンティティ取得
        $orders = $query->all();

        // CSV出力データ整形
        $data = [];
        foreach ($orders as $order) {
            $data[] = [
                $order->get('id'),
                $order->get('created'),
                $order->get('products_list'),
                Number::currency($order->get('tax_in_total'), 'JPY'),
                $order->has('order_state') ? $order->get('order_state')->get('order_state') : null,
                $order->has('payment_type') ? $order->get('payment_type')->get('payment_type') : null,
                $order->get('memo'),
                $order->get('delivery'),
                $order->has('delivery_type') ? $order->get('delivery_type')->get('delivery_type') : null,
                $order->has('facility') ? $order->get('facility')->get('facility') : null,
                $order->get('delivery_postal_code'),
                $order->get('delivery_address_full'),
                $order->get('billing'),
                $order->get('position'),
                $order->get('representative'),
                $order->get('postal_code'),
                $order->get('address_full'),
                $order->get('tel'),
                $order->get('fax'),
                $order->get('email'),
                $order->has('customer') ? $order->get('customer')->get('state') : '非会員',
                $order->get('name_tag_nl'),
                $order->get('message'),
            ];
        }
        // CSV出力データセット
        $this->response->download('export.csv');
        $this->set(compact('data', '_header', '_csvEncoding', '_newline'));
        $this->set('_serialize', 'data');
        // CSV出力用のViewクラス設定
        $this->viewBuilder()->className('CsvView.Csv');
        return;
    }
}
