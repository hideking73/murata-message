<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Dashboard Controller
 */
class DashboardController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        // Allow the actions of controller.
        $this->Auth->allow(['login']);
    }

    /**
     * login method
     *
     * @return \Cake\Network\Response|null
     */
    public function login()
    {
        // ログイン済みの場合
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }
        // ログイン処理
        if ($this->request->is('post')) {
            // ログイン情報取得
            $login = $this->Auth->identify();
            // ログイン情報取得判定
            if (!$login) {
                $this->Flash->error(__('入力された項目が正しくありません。確認してからやりなおしてください。'));
            }
            // ログインセッション作成
            $this->Auth->setUser($login);
            // リダイレクト
            return $this->redirect($this->Auth->redirectUrl(false));
        }
    }

    /**
     * logout method
     *
     * @return \Cake\Network\Response|null
     */
    public function logout()
    {
        // ログアウト処理
        $this->Flash->success(__('ログアウトしました。'));
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {}
}
