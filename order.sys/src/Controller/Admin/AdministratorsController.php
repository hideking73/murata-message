<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;

/**
 * Administrators Controller
 *
 * @property \App\Model\Table\AdministratorsTable $Administrators
 */
class AdministratorsController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Administrators = TableRegistry::get('Administrators');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // ページネーション設定取得
        $settings = [
            'maxLimit' => 20
        ];
        // 一覧取得用クエリ生成
        $query = $this->Administrators
            ->find('active');
        // 対象エンティティ取得
        $administrators = $this->paginate($query, $settings);
        $this->set(compact('administrators'));
    }

    /**
     * View method
     *
     * @param string|null $id Administrator id.
     * @return \Cake\Network\Response|null
     */
    public function view($id = null)
    {
        // 対象エンティティ取得
        $administrator = $this->Administrators
            ->find('active')
            ->where(['Administrators.id' => $id])
            ->first();
        $this->set(compact('administrator'));
        // 対象エンティティ取得判定
        if (!$administrator) {
            throw new NotFoundException(__('データがありません。'));
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // 対象エンティティ取得
        $administrator = $this->Administrators
            ->newEntity();
        $this->set(compact('administrator'));
        // 保存処理
        if ($this->request->is('post')) {
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ更新(バリデーション実行)
            $administrator = $this->Administrators
                ->patchEntity($administrator, $this->request->data);
            // 対象エンティティ保存
            if (!$administrator = $this->Administrators->save($administrator)) {
                throw new BadRequestException(__('登録に失敗しました。確認してやりなおしてください。'));
            }
            $this->set(compact('administrator'));
            // 登録完了
            $this->Connection->commit();
            $this->Flash->success(__('管理者情報を登録しました。'));
            return $this->redirect(['action' => 'view', $administrator->get('id')]);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Administrator id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        // 対象エンティティ取得
        $administrator = $this->Administrators
            ->find('active')
            ->find('ignorePassword')
            ->where(['Administrators.id' => $id])
            ->first();
        $this->set(compact('administrator'));
        // 対象エンティティ取得判定
        if (!$administrator) {
            throw new NotFoundException(__('データがありません。'));
        }
        // 保存処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ更新
            $administrator = $this->Administrators
                ->patchEntity($administrator, $this->request->data);
            // 対象エンティティ保存
            if (!$administrator = $this->Administrators->save($administrator)) {
                throw new BadRequestException(__('更新に失敗しました。確認してやりなおしてください。'));
            }
            $this->set(compact('administrator'));
            // 更新完了
            $this->Connection->commit();
            $this->Flash->success(__('管理者情報を更新しました。'));
            return $this->redirect(['action' => 'view', $administrator->get('id')]);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Administrator id.
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function delete($id = null)
    {
        // ログイン情報のユーザーIDと比較
        if ($id == $this->Auth->user('id')) {
            throw new ForbiddenException(__('ログイン中の管理者は削除できません。'));
        }
        // 対象エンティティ取得
        $administrator = $this->Administrators
            ->find('active')
            ->find('ignorePassword')
            ->where(['Administrators.id' => $id])
            ->first();
        // 対象エンティティ取得判定
        if (!$administrator) {
            throw new NotFoundException(__('データがありません。'));
        }
        // 削除処理
        if ($this->request->is(['post', 'delete'])) {
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ削除
            if (!$administrator = $this->Administrators->passive($administrator)) {
                throw new BadRequestException(__('削除に失敗しました。確認してやりなおしてください。'));
            }
            // 削除完了
            $this->Connection->commit();
            $this->Flash->success(__('管理者情報を削除しました。'));
        }
        // リダイレクト
        return $this->redirect(['action' => 'index']);
    }
}
