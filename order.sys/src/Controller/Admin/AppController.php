<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Admin;

use Cake\Controller\Controller;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Exception;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Connection = ConnectionManager::get('default');

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Administrators',
                    'finder' => 'authentication'
                ],
            ],
            'storage' => 'Session',
            'loginAction' => [
                'prefix' => 'admin',
                'controller' => 'Dashboard',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'prefix' => 'admin',
                'controller' => 'Dashboard',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'prefix' => 'admin',
                'controller' => 'Dashboard',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => [
                'prefix' => 'admin',
                'controller' => 'Dashboard',
                'action' => 'login'
            ],
            'authError' => __('ログインが必要なページです。')
        ]);
        $this->Auth->sessionKey = 'Auth.Administrator';

        $this->viewBuilder()->layout('admin');
        $this->viewBuilder()->className('Admin');
    }

    /**
     * Is authorized callback.
     *
     * @param array $user Active user data
     * @return boolean
     */
    public function isAuthorized($user)
    {
        return $user ? true : false;
    }

    /**
     * Dispatches the controller action.
     * Checks that the action exists and isn't private.
     *
     * @return mixed The resulting response.
     * @throws \LogicException When request is not set.
     * @throws \Cake\Controller\Exception\MissingActionException When actions are not defined or inaccessible.
     */
    public function invokeAction()
    {
        try {
            // アクション実行
            return parent::invokeAction();
        } catch (Exception $e) {
            // 例外メッセージをアラートに設定
            $this->Flash->error($e->getMessage());
            // トランザクションが開始されている場合はロールバック実行
            if ($this->Connection->inTransaction()) {
                $this->Connection->rollback();
            }
            // Forbidden/NotFoundの場合は各コントローラのindexへリダイレクト
            if ($e instanceof ForbiddenException || $e instanceof NotFoundException) {
                return $this->redirect(['action' => 'index']);
            }
        }
    }
}
