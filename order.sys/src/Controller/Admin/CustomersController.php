<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 */
class CustomersController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Customers = TableRegistry::get('Customers');
        $this->Prefectures = TableRegistry::get('Prefectures');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // ページネーション設定取得
        $settings = [
            'maxLimit' => 20
        ];
        // 一覧取得用クエリ生成
        $query = $this->Customers
            ->find('active')
            ->find('detail');
        // 対象エンティティ取得
        $customers = $this->paginate($query, $settings);
        $this->set(compact('customers'));
    }

    /**
     * View method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|null
     */
    public function view($id = null)
    {
        // 対象エンティティ取得
        $customer = $this->Customers
            ->find('active')
            ->find('detail')
            ->where(['Customers.id' => $id])
            ->first();
        $this->set(compact('customer'));
        // 対象エンティティ取得判定
        if (!$customer) {
            throw new NotFoundException(__('データがありません。'));
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // 選択肢データ取得
        $customer_states = Configure::read('CustomerState');
        $prefectures = $this->Prefectures->find('list');
        $this->set(compact('customer_states', 'prefectures'));
        // 対象エンティティ取得
        $customer = $this->Customers
            ->newEntity();
        $this->set(compact('customer'));
        // 保存処理
        if ($this->request->is('post')) {
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ更新(バリデーション実行)
            $customer = $this->Customers
                ->patchEntity($customer, $this->request->data, [
                    'validate' => 'adminRegister'
                ]);
            // 対象エンティティ保存
            if (!$customer = $this->Customers->save($customer)) {
                throw new BadRequestException(__('登録に失敗しました。確認してやりなおしてください。'));
            }
            $this->set(compact('customer'));
            // 登録完了
            $this->Connection->commit();
            $this->Flash->success(__('会員情報を登録しました。'));
            return $this->redirect(['action' => 'view', $customer->get('id')]);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        // 選択肢データ取得
        $customer_states = Configure::read('CustomerState');
        $prefectures = $this->Prefectures->find('list');
        $this->set(compact('customer_states', 'prefectures'));
        // 対象エンティティ取得
        $customer = $this->Customers
            ->find('active')
            ->find('ignorePassword')
            ->where(['Customers.id' => $id])
            ->first();
        $this->set(compact('customer'));
        // 対象エンティティ取得判定
        if (!$customer) {
            throw new NotFoundException(__('データがありません。'));
        }
        // 保存処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ更新
            $customer = $this->Customers
                ->patchEntity($customer, $this->request->data, [
                    'validate' => 'adminRegister'
                ]);
            // 対象エンティティ保存
            if (!$customer = $this->Customers->save($customer)) {
                throw new BadRequestException(__('更新に失敗しました。確認してやりなおしてください。'));
            }
            $this->set(compact('customer'));
            // 更新完了
            $this->Connection->commit();
            $this->Flash->success(__('管理者情報を更新しました。'));
            return $this->redirect(['action' => 'view', $customer->get('id')]);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function delete($id = null)
    {
        // 対象エンティティ取得
        $customer = $this->Customers
            ->find('active')
            ->find('ignorePassword')
            ->where(['Customers.id' => $id])
            ->first();
        // 対象エンティティ取得判定
        if (!$customer) {
            throw new NotFoundException(__('データがありません。'));
        }
        // 削除処理
        if ($this->request->is(['post', 'delete'])) {
            // トランザクション開始
            $this->Connection->begin();
            // 対象エンティティ削除
            if (!$customer = $this->Customers->passive($customer)) {
                throw new BadRequestException(__('削除に失敗しました。確認してやりなおしてください。'));
            }
            // メールアドレスの先頭に削除日を追加(UNIQUE対策)
            $email = '[Delete:' . $customer->get('modified')->format('YmdHis') . ']' . $customer->get('email');
            $customer->set(['email' => $email]);
            if (!$customer = $this->Customers->save($customer)) {
                throw new BadRequestException(__('削除に失敗しました。確認してやりなおしてください。'));
            }
            // 削除完了
            $this->Connection->commit();
            $this->Flash->success(__('会員情報を削除しました。'));
        }
        // リダイレクト
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Export method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function export()
    {
        // CSV出力設定
        $_newline = "\r\n";
        $_dataEncoding = 'UTF-8';
        // $_csvEncoding = 'SJIS-win';
        $_csvEncoding = 'SJIS'; // libiconvのSJIS-win非対応環境用にSJIS出力
        // CSVヘッダー設定
        $_header = [
            'ID', '状態', '会社名(個人名)', '部署名', '担当者名', '郵便番号', '住所', '電話番号', 'ファックス番号', 'メールアドレス', '割引率(%)', '締日', '登録日', '更新日'
        ];
        // 対象エンティティ取得
        $customers = $this->Customers
            ->find('exportCsv');
        // CSV出力データ整形
        $data = [];
        foreach ($customers as $customer) {
            $data[] = [
                $customer->get('id'),
                $customer->get('state'),
                $customer->get('billing'),
                $customer->get('position'),
                $customer->get('representative'),
                $customer->get('postal_code'),
                $customer->get('address_full'),
                $customer->get('tel'),
                $customer->get('fax'),
                $customer->get('email'),
                $customer->get('discount_rate'),
                $customer->get('closing_day'),
                $customer->get('created'),
                $customer->get('modified'),
            ];
        }
        // CSV出力データセット
        $this->response->download('export.csv');
        $this->set(compact('data', '_header', '_csvEncoding'));
        $this->set('_serialize', 'data');
        // CSV出力用のViewクラス設定
        $this->viewBuilder()->className('CsvView.Csv');
        return;
    }
}
