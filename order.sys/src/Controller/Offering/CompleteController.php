<?php
namespace App\Controller\Offering;

use App\Controller\Offering\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Mailer\MailerAwareTrait;
use Cake\Network\Exception\BadRequestException;
use Exception;

/**
 * Complete Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\CustomersTable $Customers
 * @property \App\Model\Table\MessageTypesTable $MessageTypes
 * @property \App\Model\Table\PaymentTypesTable $PaymentTypes
 */
class CompleteController extends AppController
{
    use MailerAwareTrait;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index']);

        $this->Orders = TableRegistry::get('Orders');
        $this->Customers = TableRegistry::get('Customers');
        $this->MessageTypes = TableRegistry::get('MessageTypes');
        $this->PaymentTypes = TableRegistry::get('PaymentTypes');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // 注文エンティティ確認(+アクセス方法チェック)
        if ($this->request->session()->check('Offering.Orders')) {
            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));
            $this->set(compact('order'));
        }

        // データがない、もしくは前画面までの完了フラグが立っていない場合
        if (!isset($order) || !$order->has('step_confirm_end')) {
            // 前画面へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Confirm',
                'action' => 'index'
            ]);
        }

        // メッセージありで入力/選択されていない場合
        if (!empty($order->get('message_type_id')) && empty($order->get('message_template_id')) && empty($order->get('original_message'))) {
            // 前画面へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Confirm',
                'action' => 'index'
            ]);
        }

        // 必須データ追加
        $order->set([
            'order_state_id' => Configure::read('OrderState.waiting')
        ]);

        // 注文エンティティのバリデーション実行
        $order = $this->Orders
            ->patchEntity($order, $this->request->data, [
                'validate' => false,
                'associated' => [
                    'Products' => [
                        'validate' => false
                    ],
                    'Products._joinData'
                ]
            ]);

        // 選択肢データのエンティティ取得
        $message_type = $this->MessageTypes
            ->find()
            ->where(['MessageTypes.id' => $order->get('message_type_id')])
            ->first();
        $payment_type = $this->PaymentTypes
            ->find()
            ->where(['PaymentTypes.id' => $order->get('payment_type_id')])
            ->first();

        // トランザクション開始
        $connection = ConnectionManager::get('default');
        $connection->begin();

        // 注文保存
        try {
            // 注文エンティティの保存処理
            if (!$this->Orders->save($order)) {
                throw new BadRequestException('注文に失敗しました。');
            }

            // 請求先へメール送信
            $this->getMailer('Order')
                ->send('orderBilling', [$order, $message_type, $payment_type]);

            // 管理者へメール送信
            $this->getMailer('Order')
                ->send('orderAdmin', [$order, $message_type, $payment_type]);

            // 非会員購入後の会員登録フローの設定
            if ($order->has('signup') && $order->get('signup')) {
                // 会員エンティティを新規作成
                $customer = $this->Customers
                    ->newEntity();

                // 会員エンティティにデータセット
                $customer->set([
                    'email' => $order->get('email'),
                    'billing' => $order->get('billing'),
                    'position' => $order->get('position'),
                    'representative' => $order->get('representative'),
                    'postal_code' => $order->get('postal_code'),
                    'prefecture_id' => $order->get('prefecture_id'),
                    'address' => $order->get('address'),
                    'address_etc' => $order->get('address_etc'),
                    'tel' => $order->get('tel'),
                    'fax' => $order->get('fax')
                ]);

                // 会員エンティティをシリアライズ化してセッションに保存
                $this->request->session()->write('Signup', serialize($customer));
            }
            $this->set('signup', $order->signup);

            // 注文エンティティとカート配列のセッションを削除
            $this->request->session()->delete('Offering');

            // トランザクション:コミット
            $connection->commit();
        } catch (Exception $e) {
            // トランザクション:ロールバック
            $connection->rollback();

            // 前画面へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Confirm',
                'action' => 'index'
            ]);
        }
    }
}
