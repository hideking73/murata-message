<?php
namespace App\Controller\Offering;

use App\Controller\Offering\AppController;
use Cake\ORM\TableRegistry;

/**
 * Billing Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\PrefecturesTable $Prefectures
 */
class BillingController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index']);

        $this->Orders = TableRegistry::get('Orders');
        $this->Prefectures = TableRegistry::get('Prefectures');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // 注文エンティティ確認
        if ($this->request->session()->check('Offering.Orders')) {
            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));
            $this->set(compact('order'));
        }

        // データがない、もしくは前画面までの完了フラグが立っていない場合
        if (
            !isset($order)
            || !$order->has('step_cart_end') && empty($order->get('message_type_id'))
            || !$order->has('step_message_end') && !empty($order->get('message_type_id'))
        ) {
            // 前画面へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Message',
                'action' => 'index'
            ]);
        }

        // 選択肢データのエンティティ取得
        $prefectures = $this->Prefectures
            ->find('list');
        $this->set(compact('prefectures'));

        // 次の画面の設定
        if (!$order->has('step_payment_end')) {
            // "決済方法選択画面"
            $next_controller = 'Payment';
            $next_button = '決済方法の入力';
        } else {
            // "注文確認画面"
            $next_controller = 'Confirm';
            $next_button = '内容を変更する';
        }
        $this->set(compact('next_button'));

        // 注文データ処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // 注文エンティティのバリデーション実行
            $order = $this->Orders
                ->patchEntity($order, $this->request->data, [
                    'validate' => 'orderBilling',
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);
            $this->set(compact('order'));

            // 注文エンティティのバリデーションエラー確認
            if (!empty($order->errors())) {
                return $this->render();
            }

            // "請求先入力画面"完了フラグを立てる
            $order->set('step_billing_end', true);

            // 注文エンティティをシリアライズ化してセッション保存
            $this->request->session()->write('Offering.Orders', serialize($order));

            // リダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => $next_controller,
                'action' => 'index'
            ]);
        }
    }
}
