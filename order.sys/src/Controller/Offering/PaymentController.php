<?php
namespace App\Controller\Offering;

use App\Controller\Offering\AppController;
use Cake\ORM\TableRegistry;

/**
 * Payment Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\PaymentTypesTable $PaymentTypes
 */
class PaymentController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index']);

        $this->Orders = TableRegistry::get('Orders');
        $this->PaymentTypes = TableRegistry::get('PaymentTypes');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // 注文エンティティ確認
        if ($this->request->session()->check('Offering.Orders')) {
            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));
            $this->set(compact('order'));
        }

        // データがない、もしくは前画面までの完了フラグが立っていない場合
        if (!isset($order) || !$order->has('step_billing_end')) {
            // 前画面へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Billing',
                'action' => 'index'
            ]);
        }

        // 選択肢データのエンティティ取得
        $payment_types = $this->PaymentTypes
            ->find('listOffering');
        $this->set(compact('payment_types'));

        // 次の画面の設定
        if (!$order->has('step_payment_end')) {
            // "注文確認画面"
            $next_controller = 'Confirm';
            $next_button = '注文内容の確認';
        } else {
            // "注文確認画面"
            $next_controller = 'Confirm';
            $next_button = '内容を変更する';
        }
        $this->set(compact('next_button'));

        // 注文データ処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // 注文エンティティのバリデーション実行
            $order = $this->Orders
                ->patchEntity($order, $this->request->data, [
                    'validate' => 'orderPayment',
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);
            $this->set(compact('order'));

            // 注文エンティティのバリデーションエラー確認
            if (!empty($order->errors())) {
                return $this->render();
            }

            // "請求先入力画面"完了フラグを立てる
            $order->set('step_payment_end', true);

            // 注文エンティティをシリアライズ化してセッション保存
            $this->request->session()->write('Offering.Orders', serialize($order));

            // リダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => $next_controller,
                'action' => 'index'
            ]);
        }
    }
}
