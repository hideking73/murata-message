<?php
namespace App\Controller\Offering;

use App\Controller\Offering\AppController;
use Cake\ORM\TableRegistry;

/**
 * Confirm Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\DeliveryTypesTable $DeliveryTypes
 * @property \App\Model\Table\FacilitiesTable $Facilities
 * @property \App\Model\Table\PrefecturesTable $Prefectures
 * @property \App\Model\Table\MessageTypesTable $MessageTypes
 * @property \App\Model\Table\MessageTemplatesTable $MessageTemplates
 * @property \App\Model\Table\PaymentTypesTable $PaymentTypes
 */
class ConfirmController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index']);

        $this->Orders = TableRegistry::get('Orders');
        $this->DeliveryTypes = TableRegistry::get('DeliveryTypes');
        $this->Facilities = TableRegistry::get('Facilities');
        $this->Prefectures = TableRegistry::get('Prefectures');
        $this->MessageTypes = TableRegistry::get('MessageTypes');
        $this->MessageTemplates = TableRegistry::get('MessageTemplates');
        $this->PaymentTypes = TableRegistry::get('PaymentTypes');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // 注文エンティティ確認
        if ($this->request->session()->check('Offering.Orders')) {
            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));
            $this->set(compact('order'));
        }

        // データがない、もしくは前画面までの完了フラグが立っていない場合
        if (!isset($order) || !$order->has('step_payment_end')) {
            // 前画面へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Payment',
                'action' => 'index'
            ]);
        }

        // 選択肢データのエンティティ取得
        $message_type = $this->MessageTypes
            ->find()
            ->where(['MessageTypes.id' => $order->get('message_type_id')])
            ->first();
        $payment_type = $this->PaymentTypes
            ->find()
            ->where(['PaymentTypes.id' => $order->get('payment_type_id')])
            ->first();
        $this->set(compact('message_type', 'payment_type'));

        // 次の画面の設定
        $next_controller = 'Complete';
        $next_button = '注文を確定';
        $this->set(compact('next_button'));

        // 注文データ処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // 注文エンティティのバリデーション実行
            $order = $this->Orders
                ->patchEntity($order, $this->request->data, [
                    'validate' => false,
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);
            $this->set(compact('order'));

            // 注文エンティティのバリデーションエラー確認
            if (!empty($order->errors())) {
                return $this->render();
            }

            // "確認画面"完了フラグを立てる
            $order->set('step_confirm_end', true);

            // 注文エンティティをシリアライズ化してセッション保存
            $this->request->session()->write('Offering.Orders', serialize($order));

            // "注文確認画面"へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => $next_controller,
                'action' => 'index'
            ]);
        }
    }
}
