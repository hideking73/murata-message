<?php
namespace App\Controller\Offering;

use App\Controller\Offering\AppController;

/**
 * Home Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 */
class HomeController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // ログイン済みの場合
        if ($this->Auth->user()) {
            // return $this->redirect($this->Auth->redirectUrl());
        }

        // ログイン処理
        if ($this->request->is('post')) {
            // ログイン情報判定
            if (!$login = $this->Auth->identify()) {
                $this->Flash->error(__('ログインできませんでした。' . PHP_EOL . '入力内容に誤りがないかご確認ください。'));
            }

            // ログイン情報セッション保存
            $this->Auth->setUser($login);

            // ログイン後の画面へリダイレクト
            return $this->redirect($this->Auth->redirectUrl(false));
        }
    }

    /**
     * logout method
     *
     * @return \Cake\Network\Response|null
     */
    public function logout()
    {
        // 注文セッション削除
        $this->request->session()->delete('Offering');

        // ログアウト処理
        $this->Flash->success(__('ログアウトしました。'));
        return $this->redirect($this->Auth->logout());
    }
}
