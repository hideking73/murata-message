<?php
namespace App\Controller\Offering;

use App\Controller\Offering\AppController;
use Cake\ORM\TableRegistry;

/**
 * Delivery Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\DeliveryTypesTable $DeliveryTypes
 * @property \App\Model\Table\FacilitiesTable $Facilities
 * @property \App\Model\Table\PrefecturesTable $Prefectures
 */
class DeliveryController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index']);

        $this->Orders = TableRegistry::get('Orders');
        $this->DeliveryTypes = TableRegistry::get('DeliveryTypes');
        $this->Facilities = TableRegistry::get('Facilities');
        $this->Prefectures = TableRegistry::get('Prefectures');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // 注文エンティティ確認
        if ($this->request->session()->check('Offering.Orders')) {
            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));
        } else {
            // 注文エンティティを新規作成
            $order = $this->Orders
                ->newEntity(null, [
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);

            // ログイン中の会員情報をセット
            if ($this->Auth->user()) {
                $order->set([
                    'billing' => $this->Auth->user('billing'),
                    'position' => $this->Auth->user('position'),
                    'representative' => $this->Auth->user('representative'),
                    'postal_code' => $this->Auth->user('postal_code'),
                    'prefecture_id' => $this->Auth->user('prefecture_id'),
                    'address' => $this->Auth->user('address'),
                    'address_etc' => $this->Auth->user('address_etc'),
                    'tel' => $this->Auth->user('tel'),
                    'fax' => $this->Auth->user('fax'),
                    'email' => $this->Auth->user('email'),
                    'customer_id' => $this->Auth->user('id')
                ]);
            }
        }
        $this->set(compact('order'));

        // 選択肢データのエンティティ取得
        $delivery_types = $this->DeliveryTypes
            ->find('list');
        $facilities = $this->Facilities
            ->find('list');
        $prefectures = $this->Prefectures
            ->find('list');
        $this->set(compact('delivery_types', 'facilities', 'prefectures'));

        // 次の画面の設定
        if (!$order->has('step_payment_end')) {
            // "商品選択画面"
            $next_controller = 'Select';
            $next_button = '商品を選択する';
        } else {
            // "注文確認画面"
            $next_controller = 'Confirm';
            $next_button = '内容を変更する';
        }
        $this->set(compact('next_button'));

        // 注文データ処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // 注文エンティティのバリデーション実行
            $order = $this->Orders
                ->patchEntity($order, $this->request->data, [
                    'validate' => 'orderOfferingDelivery',
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);
            $this->set(compact('order'));

            // 注文エンティティのバリデーションエラー確認
            if (!empty($order->errors())) {
                return $this->render();
            }

            // "お届け先・名札名入力画面"完了フラグを立てる
            $order->set('step_delivery_end', true);

            // 注文エンティティをシリアライズ化してセッション保存
            $this->request->session()->write('Offering.Orders', serialize($order));

            // リダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => $next_controller,
                'action' => 'index'
            ]);
        }
    }
}
