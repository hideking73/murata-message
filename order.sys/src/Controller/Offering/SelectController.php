<?php
namespace App\Controller\Offering;

use App\Controller\Offering\AppController;
use Cake\ORM\TableRegistry;

/**
 * Select Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\ProductsTable $Products
 * @property \App\Model\Table\OrdersProductsTable $OrdersProducts
 */
class SelectController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index', 'detail']);

        $this->Orders = TableRegistry::get('Orders');
        $this->Products = TableRegistry::get('Products');
        $this->OrdersProducts = TableRegistry::get('OrdersProducts');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // 注文エンティティ確認
        if ($this->request->session()->check('Offering.Orders')) {
            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));
            $this->set(compact('order'));
        }

        // データがない、もしくは前画面までの完了フラグが立っていない場合
        if (!isset($order) || !$order->has('step_delivery_end')) {
            // 前画面へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => !$this->Auth->user() ? 'Home' : 'Delivery',
                'action' => 'index'
            ]);
        }

        // 商品データのエンティティ取得
        $products = $this->Products
            ->find('offering');
        $this->set(compact('products'));
    }

    /**
     * Detail method
     *
     * @param string|null $id Products id.
     * @return \Cake\Network\Response|null
     */
    public function detail($id = null)
    {
        // カート用配列の初期化
        $cart = [];

        // 注文エンティティ確認
        if ($this->request->session()->check('Offering.Orders')) {
            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));
            $this->set(compact('order'));
            // カート用配列をセッションから取得
            $cart = $this->request->session()->read('Offering.Cart');
        }

        // 前画面までの完了フラグが立っていない場合
        if (!isset($order) || !$order->has('step_delivery_end')) {
            // 前画面へリダイレクト
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => !$this->Auth->user() ? 'Home' : 'Delivery',
                'action' => 'index'
            ]);
        }

        // 商品データのエンティティ取得
        $product = $this->Products
            ->find('offering')
            ->where(['Products.id' => $id])
            ->first();
        $this->set(compact('product'));

        // 商品データのエンティティ取得確認
        if (!$product) {
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Select',
                'action' => 'index'
            ]);
        }

        // カートのバリデーション用のエンティティ取得
        $order_product = $this->OrdersProducts
            ->newEntity();
        $this->set(compact('order_product'));

        // 注文データ処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // カートのバリデーション実行
            $order_product = $this->OrdersProducts
                ->patchEntity($order, $this->request->data);
            $this->set(compact('order_product'));

            // カートのバリデーションエラー確認
            if (!empty($order->errors())) {
                return $this->render();
            }

            // カート用配列の調整
            $product_id = $this->request->data['product_id'];
            $cart['products'][] = [
                'id' => $product_id,
                '_joinData' => $this->request->data
            ];

            // カート用配列をセッションに保存
            $this->request->session()->write('Offering.Cart', $cart);

            // 注文エンティティのバリデーション実行
            $order = $this->Orders
                ->patchEntity($order, $cart, [
                    'validate' => 'orderSelect',
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);
            $this->set(compact('order'));

            // 注文エンティティのバリデーションエラー確認
            if (!empty($order->errors())) {
                return $this->render();
            }

            // "商品選択画面"完了フラグを立てる
            $order->set('step_select_end', true);

            // 注文エンティティをシリアライズ化してセッション保存
            $this->request->session()->write('Offering.Orders', serialize($order)) ;

            // 次画面"カート画面"へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Cart',
                'action' => 'index'
            ]);
        }
    }
}
