<?php
namespace App\Controller\Offering;

use App\Controller\Offering\AppController;
use Cake\ORM\TableRegistry;

/**
 * Cart Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 * @property \App\Model\Table\MessageTypesTable $MessageTypes
 */
class CartController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index', 'delete']);

        $this->Orders = TableRegistry::get('Orders');
        $this->MessageTypes = TableRegistry::get('MessageTypes');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // 注文エンティティ確認
        if ($this->request->session()->check('Offering.Orders')) {
            // 注文エンティティをセッションから取得
            $order = unserialize($this->request->session()->read('Offering.Orders'));
            $this->set(compact('order'));
        }

        // データがない、もしくは前画面までの完了フラグが立っていない場合
        if (!isset($order) || !$order->has('step_select_end')) {
            // 前画面へリダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => 'Select',
                'action' => 'index'
            ]);
        }

        // 選択肢データのエンティティ取得
        $message_types = $this->MessageTypes
            ->find('list');
        $this->set(compact('message_types'));

        // 次の画面の設定
        if (!$order->has('step_payment_end') && !empty($order->get('message_type_id'))) {
            // "メッセージ入力/選択画面"
            $next_controller = 'Message';
            $next_button = 'メッセージを入力する';
            $back_button = 'お買い物を続ける';
        } elseif (!$order->has('step_payment_end') && empty($order->get('message_type_id'))) {
            // "請求先入力画面"
            $next_controller = 'Billing';
            $next_button = 'ご請求先の入力';
            $back_button = 'お買い物を続ける';
        } else {
            // "注文確認画面"
            $next_controller = 'Confirm';
            $next_button = '内容を変更する';
            $back_button = '商品を選択する';
        }
        $this->set(compact('next_button', 'back_button'));

        // 注文データ処理
        if ($this->request->is(['patch', 'post', 'put'])) {
            // カート用配列の調整
            foreach ($this->request->data['products'] as $key => $product) {
                $this->request->data['products'][$key] = [
                    'id' => $product['_joinData']['product_id'],
                    '_joinData' => $product['_joinData']
                ];
            }

            // 注文エンティティのバリデーション実行
            $order = $this->Orders
                ->patchEntity($order, $this->request->data, [
                    'validate' => 'orderCart',
                    'associated' => [
                        'Products' => [
                            'validate' => false
                        ],
                        'Products._joinData'
                    ]
                ]);
            $this->set(compact('order'));

            // 注文エンティティのバリデーションエラー確認
            if (!empty($order->errors())) {
                return $this->render();
            }

            // "カート画面"完了フラグを立てる
            $order->set('step_cart_end', true);

            // 注文エンティティをシリアライズ化してセッション保存
            $this->request->session()->write('Offering.Orders', serialize($order));

            // リダイレクト実行
            return $this->redirect([
                'prefix' => 'offering',
                'controller' => $next_controller,
                'action' => 'index'
            ]);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $key
     * @return \Cake\Network\Response|null
     */
    public function delete($key = null)
    {
        // カート用配列のセッションから指定商品を削除
        $this->request->session()->delete("Offering.Cart.products.{$key}");

        // カート用配列をセッションから取得
        $cart = $this->request->session()->read('Offering.Cart');

        // カート用配列の調整
        $tmp = [];
        foreach ($cart['products'] as $key => $product) {
            $tmp[] = $product;
        }
        $cart['products'] = $tmp;

        // 注文エンティティをセッションから取得
        $order = unserialize($this->request->session()->read('Offering.Orders'));

        // 注文エンティティの商品を削除
        $order->unsetProperty('products');

        // 現在のカート用配列で注文エンティティを更新
        $order = $this->Orders
            ->patchEntity($order, $cart, [
                'validate' => 'orderSelect',
                'associated' => [
                    'Products' => [
                        'validate' => false
                    ],
                    'Products._joinData'
                ]
            ]);

        // カート用配列をセッションに保存
        $this->request->session()->write('Offering.Cart', $cart);

        // 注文エンティティをシリアライズ化してセッション保存
        $this->request->session()->write('Offering.Orders', serialize($order));

        // "カート画面"へリダイレクト実行
        return $this->redirect([
            'prefix' => 'offering',
            'controller' => 'Cart',
            'action' => 'index'
        ]);
    }
}
