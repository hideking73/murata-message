<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Customers',
                    'finder' => 'authentication',
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ],
            ],
            'storage' => 'Session',
            'loginAction' => [
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => [
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'login'
            ],
            'authError' => __('ログインしてください。')
        ]);
        $this->Auth->sessionKey = 'Auth.Customer';

        $this->viewBuilder()->layout('offering');
        $this->viewBuilder()->className('Offering');

        // パスワード再発行トークンが発生している場合
        if (!empty($this->Auth->user('reset_password_token')) && $this->request->action != 'resetPassword') {
            $this->redirect([
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'reset_password',
                'token' => $this->Auth->user('reset_password_token')
            ]);
        }

        // 退会済みの会員でログインしているか
        $customer = TableRegistry::get('Customers')
            ->find('passive')
            ->where(['Customers.id' => $this->Auth->user('id')])
            ->first();
        if ($customer) {
            // 注文セッション削除
            $this->request->session()->delete('Offering');
            // ログアウト処理
            return $this->redirect($this->Auth->logout());
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Is authorized callback.
     *
     * @param array $user Active user data
     * @return boolean
     */
    public function isAuthorized($user)
    {
        return $user ? true : false;
    }
}
