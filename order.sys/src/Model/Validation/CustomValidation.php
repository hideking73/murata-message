<?php
namespace App\Model\Validation;

use Cake\Validation\Validation;

/**
 * Custom Validation
 */
class CustomValidation extends Validation
{

    /**
     * Checks that a string contains only integer or alphabet
     *
     * Returns true if string contains only integer or alphabet
     *
     * $check can be passed as an array:
     * ['check' => 'valueToCheck'];
     *
     * @param string|array $check Value to check
     * @return bool Success
     */
    public static function alphaNumeric($check)
    {
        if (empty($check) && $check !== '0') {
            return false;
        }

        return parent::_check($check, '/^[a-zA-Z0-9]+$/');
    }

    /**
     * Checks that a string contains only integer or alphabet or underscore
     *
     * Returns true if string contains only integer or alphabet or underscore
     *
     * $check can be passed as an array:
     * ['check' => 'valueToCheck'];
     *
     * @param string|array $check Value to check
     * @return bool Success
     */
    public static function alphaNumericUnderscore($check)
    {
        if (empty($check) && $check !== '0') {
            return false;
        }

        return parent::_check($check, '/^[a-zA-Z0-9_]+$/');
    }

    /**
     * Checks that a string contains only integer or alphabet or symbol
     *
     * Returns true if string contains only integer or alphabet or symbol
     *
     * $check can be passed as an array:
     * ['check' => 'valueToCheck'];
     *
     * @param string|array $check Value to check
     * @return bool Success
     */
    public static function alphaNumericSymbol($check)
    {
        if (empty($check) && $check !== '0') {
            return false;
        }

        return parent::_check($check, '/^[a-zA-Z0-9\s\x21-\x2f\x3a-\x40\x5b-\x60\x7b-\x7e]+$/');
    }

    /**
     * Verify the format of japan postal codes.
     *
     * $check can be passed as an array:
     * ['check' => 'valueToCheck'];
     *
     * @param string|array $check Value to check
     * @return bool Success
     */
    public static function jpPostalCode($check)
    {
        if (empty($check) && $check !== '0') {
            return false;
        }

        return parent::_check($check, '/^[0-9]{3}-[0-9]{4}$/');
    }

    /**
     * Verify the format of japan telephone numbers.
     *
     * $check can be passed as an array:
     * ['check' => 'valueToCheck'];
     *
     * @param string|array $check Value to check
     * @return bool Success
     */
    public static function jpPhoneNumber($check)
    {
        if (empty($check) && $check !== '0') {
            return false;
        }

        return parent::_check($check, '/^[0-9]{2,5}-[0-9]{1,4}-[0-9]{3,4}$/');
    }
}
