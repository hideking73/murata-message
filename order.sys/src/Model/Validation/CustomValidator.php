<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

/**
 * Custom Validator
 */
class CustomValidator extends Validator
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->provider('custom', 'App\Model\Validation\CustomValidation');
    }

    /**
     * Add an validateUnique rule to a field.
     *
     * @param string $field The field you want to apply the rule to.
     * @param string|null $scope Scoped to the value of another column.
     * @param string|null $message The error message when the rule fails.
     * @param string|callable|null $when Either 'create' or 'update' or a callable that returns
     *   true when the validation rule should be applied.
     * @see \Cake\ORM\Table::validateUnique()
     * @return $this
     */
    public function unique($field, $scope = null, $message = null, $when = null)
    {
        $extra = array_filter(['on' => $when, 'message' => $message]);

        $rule = is_null($scope) ? 'validateUnique' : ['validateUnique', ['scope' => $scope]];

        return $this->add($field, 'validateUnique', $extra + [
            'rule' => $rule,
            'provider' => 'table'
        ]);
    }

    /**
     * Add an alphaNumeric rule to a field.
     *
     * @param string $field The field you want to apply the rule to.
     * @param string|null $message The error message when the rule fails.
     * @param string|callable|null $when Either 'create' or 'update' or a callable that returns
     *   true when the validation rule should be applied.
     * @see \App\Model\Validation\CustomValidation::alphaNumeric()
     * @return $this
     */
    public function alphaNumeric($field, $message = null, $when = null)
    {
        $extra = array_filter(['on' => $when, 'message' => $message]);

        return $this->add($field, 'alphaNumeric', $extra + [
            'rule' => 'alphaNumeric',
            'provider' => 'custom'
        ]);
    }

    /**
     * Add an alphaNumericUnderscore rule to a field.
     *
     * @param string $field The field you want to apply the rule to.
     * @param string|null $message The error message when the rule fails.
     * @param string|callable|null $when Either 'create' or 'update' or a callable that returns
     *   true when the validation rule should be applied.
     * @see \App\Model\Validation\CustomValidation::alphaNumericUnderscore()
     * @return $this
     */
    public function alphaNumericUnderscore($field, $message = null, $when = null)
    {
        $extra = array_filter(['on' => $when, 'message' => $message]);

        return $this->add($field, 'alphaNumericUnderscore', $extra + [
            'rule' => 'alphaNumericUnderscore',
            'provider' => 'custom'
        ]);
    }

    /**
     * Add an alphaNumericSymbol rule to a field.
     *
     * @param string $field The field you want to apply the rule to.
     * @param string|null $message The error message when the rule fails.
     * @param string|callable|null $when Either 'create' or 'update' or a callable that returns
     *   true when the validation rule should be applied.
     * @see \App\Model\Validation\CustomValidation::alphaNumericSymbol()
     * @return $this
     */
    public function alphaNumericSymbol($field, $message = null, $when = null)
    {
        $extra = array_filter(['on' => $when, 'message' => $message]);

        return $this->add($field, 'alphaNumericSymbol', $extra + [
            'rule' => 'alphaNumericSymbol',
            'provider' => 'custom'
        ]);
    }

    /**
     * Add an jpPostalCode rule to a field.
     *
     * @param string $field The field you want to apply the rule to.
     * @param string|null $message The error message when the rule fails.
     * @param string|callable|null $when Either 'create' or 'update' or a callable that returns
     *   true when the validation rule should be applied.
     * @see \App\Model\Validation\CustomValidation::jpPostalCode()
     * @return $this
     */
    public function jpPostalCode($field, $message = null, $when = null)
    {
        $extra = array_filter(['on' => $when, 'message' => $message]);

        return $this->add($field, 'jpPostalCode', $extra + [
            'rule' => 'jpPostalCode',
            'provider' => 'custom'
        ]);
    }

    /**
     * Add an jpPhoneNumber rule to a field.
     *
     * @param string $field The field you want to apply the rule to.
     * @param string|null $message The error message when the rule fails.
     * @param string|callable|null $when Either 'create' or 'update' or a callable that returns
     *   true when the validation rule should be applied.
     * @see \App\Model\Validation\CustomValidation::jpPhoneNumber()
     * @return $this
     */
    public function jpPhoneNumber($field, $message = null, $when = null)
    {
        $extra = array_filter(['on' => $when, 'message' => $message]);

        return $this->add($field, 'jpPhoneNumber', $extra + [
            'rule' => 'jpPhoneNumber',
            'provider' => 'custom'
        ]);
    }

    /**
     * Add an japan email validation rule to a field.
     *
     * @param string $field The field you want to apply the rule to.
     * @param bool $checkMX Whether or not to check the MX records.
     * @param string|null $message The error message when the rule fails.
     * @param string|callable|null $when Either 'create' or 'update' or a callable that returns
     *   true when the validation rule should be applied.
     * @see \App\Model\Validation\CustomValidation::jpEmail()
     * @return $this
     */
    public function jpEmail($field, $checkMX = false, $message = null, $when = null)
    {
        $extra = array_filter(['on' => $when, 'message' => $message]);

        return $this->add($field, 'email', $extra + [
            'rule' => ['email', $checkMX, '/^[a-z0-9\.\+_-]{3,180}@(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,4})$/i']
        ]);
    }
}
