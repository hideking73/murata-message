<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\Datasource\EntityInterface;

/**
 * Active behavior
 */
class ActiveBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * Finds for active record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findActive(Query $query, array $options = [])
    {
        // テーブル名取得
        $alias = $this->_table->alias();
        return $query
            ->where(["{$alias}.is_active" => true]);
    }

    /**
     * Finds for passive record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findPassive(Query $query, array $options = [])
    {
        // テーブル名取得
        $alias = $this->_table->alias();
        return $query
            ->where(["{$alias}.is_active" => false]);
    }

    /**
     * Change to active a record.
     *
     * @param \Cake\Datasource\EntityInterface $entity
     * @param array $options
     * @return boolean
     */
    public function active(EntityInterface $entity, array $options = [])
    {
        // 対象エンティティのレコード状態フラグに有効をセット
        $entity->set(['is_active' => true]);
        // 保存処理を実行し結果を返却
        return $this->_table->save($entity, $options);
    }

    /**
     * Change to passive a record.
     *
     * @param \Cake\Datasource\EntityInterface $entity
     * @param array $options
     * @return boolean
     */
    public function passive(EntityInterface $entity, array $options = [])
    {
        // 対象エンティティのレコード状態フラグに無効をセット
        $entity->set(['is_active' => false]);
        // 保存処理を実行し結果を返却
        return $this->_table->save($entity, $options);
    }

    /**
     * Change to active all records.
     *
     * @param array $conditions
     * @return integer
     */
    public function activeAll(array $conditions = [])
    {
        // テーブル名取得
        $alias = $this->_table->alias();
        // 更新フィールドと更新対象条件の指定
        $fields = ['is_active' => true];
        $conditions = array_merge(["{$alias}.is_active" => false], $conditions);
        // 更新処理を実行し適用されたレコード数を返却
        return $this->_table->updateAll($fields, $conditions);
    }

    /**
     * Change to passive all records.
     *
     * @param array $conditions
     * @return integer
     */
    public function passiveAll(array $conditions = [])
    {
        // テーブル名取得
        $alias = $this->_table->alias();
        // 更新フィールドと更新対象条件の指定
        $fields = ['is_active' => false];
        $conditions = array_merge(["{$alias}.is_active" => true], $conditions);
        // 更新処理を実行し適用されたレコード数を返却
        return $this->_table->updateAll($fields, $conditions);
    }
}
