<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Utility\Postal;

/**
 * DeliveryArea Entity
 *
 * @property int $id
 * @property string $postal_code
 */
class DeliveryArea extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Setter for the postal_code property.
     *
     * @param string $value
     * @return string
     */
    protected function _setPostalCode($value)
    {
        // 郵便番号ユーティリティ取得
        $postal = new Postal();
        // 番号のみをセット
        return $postal->getNumber($value);
    }

    /**
     * Getter for the postal_code property.
     *
     * @param string $value
     * @return string
     */
    protected function _getPostalCode($value)
    {
        // 郵便番号ユーティリティ取得
        $postal = new Postal();
        // 日本形式に変換して返却
        return $postal->getJapanFormat($value);
    }
}
