<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\Number;
use Cake\Filesystem\File;

/**
 * Product Entity
 *
 * @property int $id
 * @property int $product_type_id
 * @property string $name
 * @property string $comment
 * @property string $spec
 * @property string $unit
 * @property int $price
 * @property string $image
 * @property bool $is_discount
 * @property bool $is_active
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property [virtual] int $tax_from_price
 * @property [virtual] int $tax_in_price
 * @property [virtual] string $image_path
 * @property [virtual] string $label
 *
 * @property \App\Model\Entity\ProductType $product_type
 * @property \App\Model\Entity\Order[] $orders
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * List of computed or virtual fields that **should** be included in JSON or array
     * representations of this Entity. If a field is present in both _hidden and _virtual
     * the field will **not** be in the array/json versions of the entity.
     *
     * @var array
     */
    protected $_virtual = [
        'tax_from_price',
        'tax_in_price',
        'image_path',
        'label'
    ];

    /**
     * [virtual] Getter for the tax_from_price property.
     *
     * @return integer
     */
    protected function _getTaxFromPrice()
    {
        // "単価(税別)"がセットされていなければ0返却
        if (!$this->has('price')) {
            return 0;
        }
        // "単価(税別)"を取得
        $price = $this->get('price');
        // 現在の"消費税率(%)"を取得
        $tax_rate = TableRegistry::get('TaxRates')
            ->find('now')
            ->first()
            ->get('tax_rate');
        // "消費税の端数計算(処理名)"を取得
        $tax_calc = Configure::read('Tax.calc');
        $tax_calc = is_null($tax_calc) || !function_exists($tax_calc) ? 'floor' : $tax_calc;
        // "単価の消費税額"を返却
        return $tax_calc($price * ($tax_rate / 100));
    }

    /**
     * [virtual] Getter for the tax_in_price property.
     *
     * @return integer
     */
    protected function _getTaxInPrice()
    {
        // "単価(税別)"がセットされていなければ0返却
        if (!$this->has('price')) {
            return 0;
        }
        // "単価(税別)","単価の消費税額"を取得
        $price = $this->get('price');
        $tax_from_price = $this->get('tax_from_price');
        // "単価(税込)"を返却
        return $price + $tax_from_price;
    }

    /**
     * [virtual] Getter for the image_path property.
     *
     * @return string|null
     */
    protected function _getImagePath()
    {
        // "画像パス"がセットされていなければnull返却
        if (!$this->has('image')) {
            return null;
        }
        // "画像パス"取得
        $image = $this->get('image');
        $file = new File(WWW_ROOT . Configure::read('App.imageBaseUrl') . $image);
        // チェック済"画像パス"を返却
        return $file ? $image : null;
    }

    /**
     * [virtual] Getter for the label property.
     *
     * @return string|null
     */
    protected function _getLabel()
    {
        // "商品名","単価(税別)"がセットされていなければnull返却
        if (!$this->has('name') && !$this->has('price')) {
            return null;
        }
        // "商品名"を取得
        $name = $this->get('name');
        // "単価(税込)"を取得して表示用フォーマット
        $tax_in_price = Number::currency($this->get('tax_in_price'), 'JPY');
        // "表示用ラベル"を返却
        return $this->get('product_type_id') != Configure::read('ProductType.other') ? "{$name} ({$tax_in_price})" : "{$name}";
    }
}
