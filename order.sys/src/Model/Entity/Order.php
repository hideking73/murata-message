<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\Number;
use App\Utility\Postal;
use App\Utility\Phone;

/**
 * Order Entity
 *
 * @property int $id
 * @property int $order_state_id
 * @property int $payment_type_id
 * @property string $delivery
 * @property int $delivery_type_id
 * @property int $facility_id
 * @property string $delivery_postal_code
 * @property int $delivery_prefecture_id
 * @property string $delivery_address
 * @property string $delivery_address_etc
 * @property string $company
 * @property string $sender
 * @property bool $is_sender_external
 * @property string $memo
 * @property int $message_type_id
 * @property int $message_template_id
 * @property string $original_message
 * @property int $message_price
 * @property string $tax_calc
 * @property int $tax_rate
 * @property string $discount_calc
 * @property int $discount_rate
 * @property int $carriage
 * @property string $billing
 * @property bool $is_billing_external
 * @property string $position
 * @property string $representative
 * @property string $postal_code
 * @property int $prefecture_id
 * @property string $address
 * @property string $address_etc
 * @property string $tel
 * @property string $fax
 * @property string $email
 * @property int $customer_id
 * @property bool $is_active
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property [virtual] string $payment_type_name
 * @property [virtual] string $delivery_address_full
 * @property [virtual] string $delivery_to
 * @property [virtual] string $billing_full
 * @property [virtual] string $billing_full_nl
 * @property [virtual] string $address_full
 * @property [virtual] string $name_tag
 * @property [virtual] string $name_tag_nl
 * @property [virtual] string $message
 * @property [virtual] int $tax_from_message_price
 * @property [virtual] int $tax_in_message_price
 * @property [virtual] int $subtotal
 * @property [virtual] int $tax_from_subtotal
 * @property [virtual] int $tax_in_subtotal
 * @property [virtual] int $discount
 * @property [virtual] int $tax_from_discount
 * @property [virtual] int $tax_in_discount
 * @property [virtual] int $apply_discount
 * @property [virtual] int $tax_from_apply_discount
 * @property [virtual] int $tax_in_apply_discount
 * @property [virtual] int $tax_from_carriage
 * @property [virtual] int $tax_in_carriage
 * @property [virtual] int $total
 * @property [virtual] int $tax_from_total
 * @property [virtual] int $tax_in_total
 * @property [virtual] boolean $is_other_product
 * @property [virtual] string $products_list
 *
 * @property \App\Model\Entity\OrderState $order_state
 * @property \App\Model\Entity\PaymentType $payment_type
 * @property \App\Model\Entity\DeliveryType $delivery_type
 * @property \App\Model\Entity\Facility $facility
 * @property \App\Model\Entity\Prefecture $delivery_prefecture
 * @property \App\Model\Entity\MessageType $message_type
 * @property \App\Model\Entity\MessageTemplate $message_template
 * @property \App\Model\Entity\Prefecture $prefecture
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Product[] $products
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * List of computed or virtual fields that **should** be included in JSON or array
     * representations of this Entity. If a field is present in both _hidden and _virtual
     * the field will **not** be in the array/json versions of the entity.
     *
     * @var array
     */
    protected $_virtual = [
        'payment_type_name',
        'delivery_address_full',
        'delivery_to',
        'billing_full',
        'billing_full_nl',
        'address_full',
        'name_tag',
        'name_tag_nl',
        'message',
        'tax_from_message_price',
        'tax_in_message_price',
        'subtotal',
        'tax_from_subtotal',
        'tax_in_subtotal',
        'discount',
        'tax_from_discount',
        'tax_in_discount',
        'apply_discount',
        'tax_from_apply_discount',
        'tax_in_apply_discount',
        'tax_from_carriage',
        'tax_in_carriage',
        'total',
        'tax_from_total',
        'tax_in_total',
        'is_other_product',
        'products_list'
    ];

    /**
     * Setter for the delivery property.
     *
     * @param integer $value
     * @return integer
     */
    protected function _setDelivery($value)
    {
        // "消費税率(%)"がセットされていない場合
        if (!$this->has('tax_rate')) {
            // 現在の"消費税率(%)"を設定
            $tax_rate = TableRegistry::get('TaxRates')
                ->find('now')
                ->first();
            $this->set([
                'tax_calc' => Configure::read('Tax.calc'),
                'tax_rate' => $tax_rate->get('tax_rate')
            ]);
        }
        // "お届け先"はそのまま設定
        return $value;
    }

    /**
     * Setter for the delivery_type_id property.
     *
     * @param string $value
     * @return string
     */
    protected function _setDeliveryTypeId($value)
    {
        // "お届け先タイプ"によって、データを切り替え
        switch ($value) {
            // "村田葬儀社の葬祭式場"の場合
            case Configure::read('DeliveryType.murata'):
                // "他社の式場"の項目をクリア
                $this->set([
                    'delivery_postal_code' => null,
                    'delivery_prefecture_id' => null,
                    'delivery_address' => null,
                    'delivery_address_etc' => null,
                    'carriage' => 0
                ]);
                // "割引率(%)"取得のため"会員情報"取得
                if ($this->has('customer_id')) {
                    $customer = TableRegistry::get('Customers')
                        ->get($this->get('customer_id'));
                    // "割引率(%)"セット
                    $this->set([
                        'discount_rate' => $customer->get('discount_rate')
                    ]);
                }
            break;
            // "他社の式場"の場合
            case Configure::read('DeliveryType.other'):
                // "村田葬儀社の葬祭式場","弔意メッセージ"の項目をクリア
                $this->set([
                    'facility_id' => null,
                    'message_type_id' => null,
                    'message_template_id' => null,
                    'original_message' => null,
                    'message_price' => 0,
                    'carriage' => Configure::read('Carriage.carriage')
                ]);
            break;
        }
        // "お届け先タイプID"を設定
        return $value;
    }

    /**
     * Setter for the delivery_postal_code property.
     *
     * @param string $value
     * @return string
     */
    protected function _setDeliveryPostalCode($value)
    {
        // 郵便番号ユーティリティ取得
        $postal = new Postal();
        // "配達エリア"を検索
        if (!empty($value)) {
            // 検索用に"郵便番号"を整形
            $postal_code = $postal->getNumber($value);
            $postal_code = $postal->getJapanFormat($postal_code);
            $delivery_area = TableRegistry::get('DeliveryAreas')
                ->find()
                ->where(['DeliveryAreas.postal_code' => $postal_code])
                ->first();
            // "割引率(%)"取得のため"会員情報"取得
            if ($this->has('customer_id')) {
                $customer = TableRegistry::get('Customers')
                    ->get($this->get('customer_id'));
            }
            // "送料(税別)"を設定
            if (!$delivery_area) {
                // "配達エリア圏外"の場合
                // "送料(税別)"を設定し"メッセージタイプID","割引率(%)"をクリア
                $this->set([
                    'carriage' => Configure::read('Carriage.carriage'),
                    'message_type_id' => null,
                    'discount_rate' => 0
                ]);
            } else {
                // "配達エリア圏内"の場合
                // "送料(税別)"をクリア
                $this->set(['carriage' => 0]);
                // "割引率(%)"を設定
                if (isset($customer) && $customer) {
                    $this->set(['discount_rate' => $customer->get('discount_rate')]);
                }
            }
        }
        // "番号のみ"を設定
        return $postal->getNumber($value);
    }

    /**
     * Setter for the message_type_id property.
     *
     * @param integer $value
     * @return integer
     */
    protected function _setMessageTypeId($value)
    {
        // "メッセージタイプ"を取得
        $message_type = TableRegistry::get('MessageTypes')
                ->find()
                ->where(['MessageTypes.id' => $value])
                ->first();
        if (!$message_type) {
            // "メッセージテンプレートID","オリジナルメッセージ","メッセージ料(税別)"をクリア
            $this->set([
                'message_template_id' => null,
                'original_message' => null,
                'message_price' => 0
            ]);
        } else {
            // "メッセージ料(税別)"を設定
            $this->set([
                'message_price' => $message_type->get('message_price')
            ]);
            // "メッセージタイプ"によって、データを切り替え
            switch ($value) {
                // "オリジナルメッセージ"をクリア
                case Configure::read('MessageType.template'):
                    $this->set([
                        'original_message' => null
                    ]);
                break;
                // "メッセージテンプレートID"をクリア
                case Configure::read('MessageType.original'):
                    $this->set([
                        'message_template_id' => null
                    ]);
                break;
            }
        }
        // "メッセージタイプID"はそのまま設定
        return $value;
    }

    /**
     * Setter for the postal_code property.
     *
     * @param string $value
     * @return string
     */
    protected function _setPostalCode($value)
    {
        // 郵便番号ユーティリティ取得
        $postal = new Postal();
        // 番号のみをセット
        return $postal->getNumber($value);
    }

    /**
     * Setter for the tel property.
     *
     * @param string $value
     * @return string
     */
    protected function _setTel($value)
    {
        // 電話番号ユーティリティ取得
        $phone = new Phone();
        // 番号のみをセット
        return $phone->getNumber($value);
    }

    /**
     * Setter for the fax property.
     *
     * @param string $value
     * @return string
     */
    protected function _setFax($value)
    {
        // 電話番号ユーティリティ取得
        $phone = new Phone();
        // 番号のみをセット
        return $phone->getNumber($value);
    }

    /**
     * Setter for the customer_id property.
     *
     * @param integer $value
     * @return integer
     */
    protected function _setCustomerId($value)
    {
        // "会員情報"を取得
        $customer = TableRegistry::get('Customers')
                ->get($value);
        if ($customer) {
            // "割引率(%)","割引の端数計算(処理名)"を設定
            $this->set([
                'discount_calc' => Configure::read('Discount.calc'),
                'discount_rate' => $customer->get('discount_rate')
            ]);
        }
        // "メッセージタイプID"はそのまま設定
        return $value;
    }

    /**
     * Getter for the delivery_postal_code property.
     *
     * @param string $value
     * @return string
     */
    protected function _getDeliveryPostalCode($value)
    {
        // 郵便番号ユーティリティ取得
        $postal = new Postal();
        // 日本形式に変換して返却
        return $postal->getJapanFormat($value);
    }

    /**
     * Getter for the postal_code property.
     *
     * @param string $value
     * @return string
     */
    protected function _getPostalCode($value)
    {
        // 郵便番号ユーティリティ取得
        $postal = new Postal();
        // 日本形式に変換して返却
        return $postal->getJapanFormat($value);
    }

    /**
     * Getter for the tel property.
     *
     * @param string $value
     * @return string
     */
    protected function _getTel($value)
    {
        // 電話番号ユーティリティ取得
        $phone = new Phone();
        // 日本形式に変換して返却
        return $phone->getJapanFormat($value);
    }

    /**
     * Getter for the fax property.
     *
     * @param string $value
     * @return string
     */
    protected function _getFax($value)
    {
        // 電話番号ユーティリティ取得
        $phone = new Phone();
        // 日本形式に変換して返却
        return $phone->getJapanFormat($value);
    }

    /**
     * [virtual] Getter for the payment_type_name property.
     *
     * @param string $value
     * @return string
     */
    protected function _getPaymentTypeName($value)
    {
        // "決済方法ID"がセットされていなければ[null]返却
        if (!$this->has('payment_type_id')) {
            return null;
        } elseif ($this->has('payment_type')) {
            // "都道府県"を取得
            return $this->get('payment_type')
                ->get('payment_type');
        } else {
            // "都道府県"を取得
            return TableRegistry::get('PaymentTypes')
                ->get($this->get('payment_type_id'))
                ->get('payment_type');
        }
    }

    /**
     * [virtual] Getter for the delivery_address_full property.
     *
     * @return string
     */
    protected function _getDeliveryAddressFull()
    {
        // "都道府県"がセットされていなければ[null]返却
        if (!$this->has('delivery_prefecture_id')) {
            return null;
        } elseif ($this->has('delivery_prefecture')) {
            // "都道府県"を取得
            $prefecture = $this->get('delivery_prefecture')
                ->get('prefecture');
        } else {
            // "都道府県"を取得
            $prefecture = TableRegistry::get('Prefectures')
                ->get($this->get('delivery_prefecture_id'))
                ->get('prefecture');
        }
        // "住所(市区町村名)"を取得
        $address = $this->get('delivery_address');
        // "その他の住所(番地・ビル名)"を取得
        $address_etc = $this->get('delivery_address_etc');
        // "住所"を返却
        return "{$prefecture}{$address}{$address_etc}";
    }

    /**
     * [virtual] Getter for the delivery_to property.
     *
     * @return string
     */
    protected function _getDeliveryTo()
    {
        // "お届け先"を取得
        if ($this->has('facility')) {
            // "施設名"を取得
            $delivery_to = $this->get('facility')
                ->get('facility');
        } elseif ($this->has('facility_id')) {
            // "施設名"を取得
            $delivery_to = TableRegistry::get('Facilities')
                ->get($this->get('facility_id'))
                ->get('facility');
        } else {
            // "お届け先住所"を取得
            $delivery_to = $this->get('delivery_address_full');
        }
        // "お届け先"を返却
        return $delivery_to;
    }

    /**
     * [virtual] Getter for the billing_full property.
     *
     * @return string
     */
    protected function _getBillingFull()
    {
        // 初期化
        $billing_full = '';
        // "会社名(個人名)"取得
        $billing_full .= $this->get('billing');
        $billing_full .= (!empty($billing_full) && $this->has('position')) ? ' ' : null;
        // "部署名"取得
        $billing_full .= $this->get('position');
        $billing_full .= (!empty($billing_full) && $this->has('representative')) ? ' ' : null;
        // "担当者名"取得
        $billing_full .= $this->get('representative');
        // "外字の有無"取得
        $billing_full .= $this->get('is_billing_external') ? ' ' . __('(外字あり)') : null;
        // "請求先"を返却
        return $billing_full;
    }

    /**
     * [virtual] Getter for the billing_full_nl property.
     *
     * @return string
     */
    protected function _getBillingFullNl()
    {
        // 初期化
        $billing_full = '';
        // "会社名(個人名)"取得
        $billing_full .= $this->get('billing');
        $billing_full .= (!empty($billing_full) && $this->has('position')) ? PHP_EOL : null;
        // "部署名"取得
        $billing_full .= $this->get('position');
        $billing_full .= (!empty($billing_full) && $this->has('representative')) ? PHP_EOL : null;
        // "担当者名"取得
        $billing_full .= $this->get('representative');
        // "外字の有無"取得
        $billing_full .= $this->get('is_billing_external') ? ' ' . __('(外字あり)') : null;
        // "請求先"を返却
        return $billing_full;
    }

    /**
     * [virtual] Getter for the address_full property.
     *
     * @return string
     */
    protected function _getAddressFull()
    {
        // "都道府県"がセットされていなければ[null]返却
        if (!$this->has('prefecture_id')) {
            return null;
        } elseif ($this->has('prefecture')) {
            // "都道府県"を取得
            $prefecture = $this->get('prefecture')
                ->get('prefecture');
        } else {
            // "都道府県"を取得
            $prefecture = TableRegistry::get('Prefectures')
                ->get($this->get('prefecture_id'))
                ->get('prefecture');
        }
        // "住所(市区町村名)"を取得
        $address = $this->get('address');
        // "その他の住所(番地・ビル名)"を取得
        $address_etc = $this->get('address_etc');
        // "住所"を返却
        return "{$prefecture}{$address}{$address_etc}";
    }

    /**
     * [virtual] Getter for the name_tag property.
     *
     * @return string
     */
    protected function _getNameTag()
    {
        // 初期化
        $name_tag = '';
        // "名札:会社名"取得
        $name_tag .= $this->get('company');
        $name_tag .= (!empty($name_tag) && $this->has('sender')) ? ' ' : null;
        // "名札:差出人"取得
        $name_tag .= $this->get('sender');
        // "外字の有無"取得
        $name_tag .= $this->get('is_sender_external') ? ' ' . __('(外字あり)') : null;
        // "名札"を返却
        return $name_tag;
    }

    /**
     * [virtual] Getter for the name_tag_nl property.
     *
     * @return string
     */
    protected function _getNameTagNl()
    {
        // 初期化
        $name_tag = '';
        // "名札:会社名"取得
        $name_tag .= $this->get('company');
        $name_tag .= (!empty($name_tag) && $this->has('sender')) ? PHP_EOL : null;
        // "名札:差出人"取得
        $name_tag .= $this->get('sender');
        // "外字の有無"取得
        $name_tag .= $this->get('is_sender_external') ? ' ' . __('(外字あり)') : null;
        // "名札"を返却
        return $name_tag;
    }

    /**
     * [virtual] Getter for the message property.
     *
     * @return string
     */
    protected function _getMessage()
    {
        // "メッセージタイプID"がセットされていなければ[null]返却
        if (!$this->has('message_type_id')) {
            return null;
        }
        // "メッセージタイプ"によって分岐
        switch ($this->get('message_type_id')) {
            // "メッセージテンプレート"の場合
            case Configure::read('MessageType.template'):
                if ($this->has('message_template')) {
                    // "メッセージテンプレート"取得
                    $message = $this->get('message_template')
                        ->get('message_template');
                } elseif ($this->has('message_template_id')) {
                    // "メッセージテンプレート"取得
                    $message = TableRegistry::get('MessageTemplates')
                        ->get($this->get('message_template_id'))
                        ->get('message_template');
                } else {
                    $message = null;
                }
                $message = preg_replace('/\s/', PHP_EOL, $message);
            break;
            // "オリジナルメッセージ"の場合
            case Configure::read('MessageType.original'):
                // "オリジナルメッセージ"取得
                $message = $this->get('original_message');
            break;
        }
        // "メッセージ"返却
        return $message;
    }

    /**
     * [virtual] Getter for the tax_from_message_price property.
     *
     * @return integer|null
     */
    protected function _getTaxFromMessagePrice()
    {
        // 購入時の"メッセージ料(税別)"がセットされていなければ[0]返却
        if (!$this->has('message_price')) {
            return 0;
        }
        // 購入時の"メッセージ料(税別)"を取得
        $message_price = $this->get('message_price');
        // "消費税率(%)"を取得
        if ($this->has('tax_rate')) {
            // 購入時の"消費税率(%)"
            $tax_rate = $this->get('tax_rate');
        } else {
            // 現在の"消費税率(%)"
            $tax_rate = TableRegistry::get('TaxRates')
                ->find('now')
                ->first()
                ->get('tax_rate');
        }
        // "消費税の端数計算(処理名)"を取得
        if ($this->has('tax_calc')) {
            // 購入時の"消費税の端数計算(処理名)"
            $tax_calc = $this->get('tax_calc');
        } else {
            // システムの"消費税の端数計算(処理名)"
            $tax_calc = Configure::read('Tax.calc');
        }
        $tax_calc = is_null($tax_calc) || !function_exists($tax_calc) ? 'floor' : $tax_calc;
        // 購入時の"メッセージ料の消費税額"を返却
        return $tax_calc($message_price * ($tax_rate / 100));
    }

    /**
     * [virtual] Getter for the tax_in_message_price property.
     *
     * @return integer|null
     */
    protected function _getTaxInMessagePrice()
    {
        // "メッセージ料(税別)"がセットされていなければ[0]返却
        if (!$this->has('message_price')) {
            return 0;
        }
        // "メッセージ料(税別)","メッセージ料の消費税額"を取得
        $message_price = $this->get('message_price');
        $tax_from_message_price = $this->get('tax_from_message_price');
        // "メッセージ料(税込)"を返却
        return $message_price + $tax_from_message_price;
    }

    /**
     * [virtual] Getter for the subtotal property.
     *
     * @return integer
     */
    protected function _getSubtotal()
    {
        // 購入時の"メッセージ料(税別)"を取得
        $message_price = $this->get('message_price');
        // 購入時の"全商品の小計(税別)"を初期化
        $subtotal = 0 + $message_price;
        // "商品"がセットされていなければ[初期化値]返却
        if (!$this->has('products')) {
            return $subtotal;
        }
        // 購入時の"全商品"を取得
        $products = $this->get('products');
        // 購入時の"全商品の小計(税別)"を算出
        if (is_array($products) && count($products)) {
            foreach ($products as $key => $product) {
                if ($product->get('_joinData')->has('subtotal')) {
                    $subtotal += $product->get('_joinData')->get('subtotal');
                }
            }
        }
        // 購入時の"全商品の小計(税別)"を返却
        return $subtotal;
    }

    /**
     * [virtual] Getter for the tax_from_subtotal property.
     *
     * @return integer|null
     */
    protected function _getTaxFromSubtotal()
    {
        // 購入時の"メッセージ料の消費税額"を取得
        $tax_from_message_price = $this->get('tax_from_message_price');
        // 購入時の"全商品の小計の消費税額"を初期化
        $tax_from_subtotal = 0 + $tax_from_message_price;
        // 購入時の"商品"がセットされていなければ[初期化値]返却
        if (!$this->has('products')) {
            return $tax_from_subtotal;
        }
        // 購入時の"全商品"を取得
        $products = $this->get('products');
        // 購入時の"全商品の小計の消費税額"を算出
        if (is_array($products) && count($products)) {
            foreach ($products as $key => $product) {
                // 購入時の"小計の消費税額"を累計
                if ($product->get('_joinData')->has('tax_from_subtotal')) {
                    $tax_from_subtotal += $product->get('_joinData')->get('tax_from_subtotal');
                }
            }
        }
        // 購入時の"全商品の小計の消費税額"を返却
        return $tax_from_subtotal;
    }

    /**
     * [virtual] Getter for the tax_in_subtotal property.
     *
     * @return integer|null
     */
    protected function _getTaxInSubtotal()
    {
        // 購入時の"メッセージ料(税込)"を取得
        $tax_in_message_price = $this->get('tax_in_message_price');
        // 購入時の"全商品の小計(税込)"を初期化
        $tax_in_subtotal = 0 + $tax_in_message_price;
        // 購入時の"商品"がセットされていなければ[初期化値]返却
        if (!$this->has('products')) {
            return $tax_in_subtotal;
        }
        // 購入時の"全商品"を取得
        $products = $this->get('products');
        // 購入時の"全商品の小計(税込)"を算出
        if (is_array($products) && count($products)) {
            foreach ($products as $key => $product) {
                // 購入時の"小計(税込)"を累計
                if ($product->get('_joinData')->has('tax_in_subtotal')) {
                    $tax_in_subtotal += $product->get('_joinData')->get('tax_in_subtotal');
                }
            }
        }
        // 購入時の"全商品の小計(税込)"を返却
        return $tax_in_subtotal;
    }

    /**
     * [virtual] Getter for the discount property.
     *
     * @return integer|null
     */
    protected function _getDiscount()
    {
        // "商品"がセットされていなければ[0]返却
        if (!$this->has('products')) {
            return 0;
        }
        // 購入時の"割引額(税別)"を初期化
        $discount = 0;
        // "割引率(%)"を取得
        if ($this->has('discount_rate')) {
            // 購入時の"割引率(%)"
            $discount_rate = $this->get('discount_rate');
        } else {
            // 現在の"割引率(%)"
            if ($this->has('customer_id')) {
                $discount_rate = TableRegistry::get('Customers')
                    ->get($this->get('customer_id'))
                    ->get('discount_rate');
            } else {
                $discount_rate = 0;
            }
        }
        // "割引の端数計算(処理名)"を取得
        if ($this->has('discount_calc')) {
            // 購入時の"割引の端数計算(処理名)"
            $discount_calc = $this->get('discount_calc');
        } else {
            // システムの"割引の端数計算(処理名)"
            $discount_calc = Configure::read('Discount.calc');
        }
        $discount_calc = is_null($discount_calc) || !function_exists($discount_calc) ? 'floor' : $discount_calc;
        // 購入時の"全商品"を取得
        $products = $this->get('products');
        // 購入時の"割引額(税別)"を算出(割引対象商品のみ)
        if (is_array($products) && count($products)) {
            foreach ($products as $key => $product) {
                if ($product->get('_joinData')->has('subtotal') && $product->get('_joinData')->get('is_discount')) {
                    $discount += $discount_calc($product->get('_joinData')->get('subtotal') * ($discount_rate / 100));
                }
            }
        }
        // 購入時の"割引額(税別)"を返却
        return $discount;
    }

    /**
     * [virtual] Getter for the tax_from_discount property.
     *
     * @return integer|null
     */
    protected function _getTaxFromDiscount()
    {
        // 購入時の"割引額(税別)"を取得
        $discount = $this->get('discount');
        // "消費税率(%)"を取得
        if ($this->has('tax_rate')) {
            // 購入時の"消費税率(%)"
            $tax_rate = $this->get('tax_rate');
        } else {
            // 現在の"消費税率(%)"
            $tax_rate = TableRegistry::get('TaxRates')
                ->find('now')
                ->first()
                ->get('tax_rate');
        }
        // "消費税の端数計算(処理名)"を取得
        if ($this->has('tax_calc')) {
            // 購入時の"消費税の端数計算(処理名)"
            $tax_calc = $this->get('tax_calc');
        } else {
            // システムの"消費税の端数計算(処理名)"
            $tax_calc = Configure::read('Tax.calc');
        }
        $tax_calc = is_null($tax_calc) || !function_exists($tax_calc) ? 'floor' : $tax_calc;
        // 購入時の"割引額の消費税額"を返却
        return $tax_calc($discount * ($tax_rate / 100));
    }

    /**
     * [virtual] Getter for the tax_in_discount property.
     *
     * @return integer|null
     */
    protected function _getTaxInDiscount()
    {
        // "割引額(税別)","割引額の消費税額"を取得
        $discount = $this->get('discount');
        $tax_from_discount = $this->get('tax_from_discount');
        // "割引額(税込)"を返却
        return $discount + $tax_from_discount;
    }

    /**
     * [virtual] Getter for the apply_discount property.
     *
     * @return integer|null
     */
    protected function _getApplyDiscount()
    {
        // 購入時の"全商品の小計(税別)","割引額(税別)"を取得
        $subtotal = $this->get('subtotal');
        $discount = $this->get('discount');
        // "割引適用額(税別)"を返却
        return $subtotal - $discount;
    }

    /**
     * [virtual] Getter for the tax_from_apply_discount property.
     *
     * @return integer|null
     */
    protected function _getTaxFromApplyDiscount()
    {
        // 購入時の"全商品の小計の消費税額","割引額の消費税額"を取得
        $tax_from_subtotal = $this->get('tax_from_subtotal');
        $tax_from_discount = $this->get('tax_from_discount');
        // "割引適用額の消費税額"を返却
        return $tax_from_subtotal - $tax_from_discount;
    }

    /**
     * [virtual] Getter for the tax_in_apply_discount property.
     *
     * @return integer|null
     */
    protected function _getTaxInApplyDiscount()
    {
        // 購入時の"全商品の小計(税込)","割引額(税込)"を取得
        $tax_in_subtotal = $this->get('tax_in_subtotal');
        $tax_in_discount = $this->get('tax_in_discount');
        // "割引適用額(税込)"を返却
        return $tax_in_subtotal - $tax_in_discount;
    }

    /**
     * [virtual] Getter for the tax_from_carriage property.
     *
     * @return integer
     */
    protected function _getTaxFromCarriage()
    {
        // 購入時の"送料(税別)"がセットされていなければ[0]返却
        if (!$this->has('carriage')) {
            return 0;
        }
        // 購入時の"送料(税別)"を取得
        $carriage = $this->get('carriage');
        // "消費税率(%)","消費税の端数計算(処理名)"を取得
        if ($this->has('order_id')) {
            // 注文データ取得
            $order = TableRegistry::get('Orders')
                ->get($this->get('order_id'));
            // 購入時の"消費税率(%)"
            $tax_rate = $order->get('tax_rate');
            // 購入時の"消費税の端数計算(処理名)"
            $tax_calc = $order->get('tax_calc');
        } else {
            // 現在の"消費税率(%)"
            $tax_rate = TableRegistry::get('TaxRates')
                ->find('now')
                ->first()
                ->get('tax_rate');
            // システムの"消費税の端数計算(処理名)"
            $tax_calc = Configure::read('Tax.calc');
        }
        $tax_calc = is_null($tax_calc) || !function_exists($tax_calc) ? 'floor' : $tax_calc;
        // "送料の消費税額"を返却
        return $tax_calc($carriage * ($tax_rate / 100));
    }

    /**
     * [virtual] Getter for the tax_in_carriage property.
     *
     * @return integer
     */
    protected function _getTaxInCarriage()
    {
        // 購入時の"送料(税別)"がセットされていなければ[0]返却
        if (!$this->has('carriage')) {
            return 0;
        }
        // 購入時の"送料(税別)","送料の消費税額"を取得
        $carriage = $this->get('carriage');
        $tax_from_carriage = $this->get('tax_from_carriage');
        // 購入時の"送料(税込)"を返却
        return $carriage + $tax_from_carriage;
    }

    /**
     * [virtual] Getter for the total property.
     *
     * @return integer|null
     */
    protected function _getTotal()
    {
        // 購入時の"割引適用額(税別)","送料(税別)"を取得
        $apply_discount = $this->get('apply_discount');
        if ($this->has('carriage')) {
            $carriage = $this->get('carriage');
        } else {
            $carriage = 0;
        }
        // "合計(税別)"を返却
        return $apply_discount + $carriage;
    }

    /**
     * [virtual] Getter for the tax_from_total property.
     *
     * @return integer|null
     */
    protected function _getTaxFromTotal()
    {
        // 購入時の"割引適用額の消費税額","送料の消費税額"を取得
        $tax_from_apply_discount = $this->get('tax_from_apply_discount');
        if ($this->has('tax_from_carriage')) {
            $tax_from_carriage = $this->get('tax_from_carriage');
        } else {
            $tax_from_carriage = 0;
        }
        // "合計の消費税額"を返却
        return $tax_from_apply_discount + $tax_from_carriage;
    }

    /**
     * [virtual] Getter for the tax_in_total property.
     *
     * @return integer|null
     */
    protected function _getTaxInTotal()
    {
        // 購入時の"割引適用額(税込)","送料(税込)"を取得
        $tax_in_apply_discount = $this->get('tax_in_apply_discount');
        if ($this->has('tax_in_carriage')) {
            $tax_in_carriage = $this->get('tax_in_carriage');
        } else {
            $tax_in_carriage = 0;
        }
        // "合計(税込)"を返却
        return $tax_in_apply_discount + $tax_in_carriage;
    }

    /**
     * [virtual] Getter for the is_other_product property.
     *
     * @return boolean
     */
    protected function _getIsOtherProduct()
    {
        // "その他"商品存在フラグを初期化
        $is_other_product = false;
        // 購入時の"全商品"を取得
        $products = $this->get('products');
        // 購入時の"全商品"から"その他"商品が存在するかチェック
        if (is_array($products) && count($products)) {
            foreach ($products as $key => $product) {
                // "その他"商品の存在チェック
                if ($product->get('product_type_id') == Configure::read('ProductType.other')) {
                    $is_other_product = true;
                    break;
                }
            }
        }
        // "その他"商品存在フラグを返却
        return $is_other_product;
    }

    /**
     * [virtual] Getter for the products_list property.
     *
     * @return string
     */
    protected function _getProductsList()
    {
        // "その他"商品存在フラグを初期化
        $products_list = '';
        // 購入時の"全商品"を取得
        $products = $this->get('products');
        // 購入時の"全商品"を文字列に起こす
        if (is_array($products) && count($products)) {
            foreach ($products as $key => $product) {
                // 購入時の"商品名"を取得
                $name = h(json_decode($product->get('_joinData')->get('data'), false)->name);
                // 購入時の"単価(税込)"を取得
                $tax_in_price = Number::currency($product->get('_joinData')->get('tax_in_price'), 'JPY');
                // 購入時の"単価(税込)"を取得
                $quantity = Number::format($product->get('_joinData')->get('quantity'));
                // 文字列に代入
                $products_list .= "{$name}({$tax_in_price})×$quantity" . PHP_EOL;
            }
        }
        // "その他"商品が存在する場合
        if ($this->get('is_other_product')) {
            // "その他商品詳細欄"を末尾に追加
            $products_list .= PHP_EOL;
            $products_list .= 'その他の内訳:' . PHP_EOL;
            $products_list .= $this->get('other_detail');
        }
        // 文字列に起こした"全商品"を返却
        return trim($products_list);
    }
}
