<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use App\Utility\Postal;
use App\Utility\Phone;

/**
 * Customer Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $billing
 * @property bool $is_billing_external
 * @property string $position
 * @property string $representative
 * @property string $postal_code
 * @property int $prefecture_id
 * @property string $address
 * @property string $address_etc
 * @property string $tel
 * @property string $fax
 * @property int $discount_rate
 * @property int $closing_day
 * @property bool $is_formal
 * @property bool $is_active
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property [virtual] string $state
 * @property [virtual] string $address_full
 * @property [virtual] string $label
 *
 * @property \App\Model\Entity\Prefecture $prefecture
 * @property \App\Model\Entity\CustomerSignupToken $customer_signup_token
 * @property \App\Model\Entity\CustomerEditToken $customer_edit_token
 * @property \App\Model\Entity\ResetPasswordToken $reset_password_token
 * @property \App\Model\Entity\Order[] $orders
 */
class Customer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    /**
     * List of computed or virtual fields that **should** be included in JSON or array
     * representations of this Entity. If a field is present in both _hidden and _virtual
     * the field will **not** be in the array/json versions of the entity.
     *
     * @var array
     */
    protected $_virtual = [
        'state',
        'billing_full',
        'billing_full_nl',
        'address_full',
        'label'
    ];

    /**
     * Setter for the postal_code property.
     *
     * @param string $value
     * @return string
     */
    protected function _setPostalCode($value)
    {
        // 郵便番号ユーティリティ取得
        $postal = new Postal();
        // 番号のみをセット
        return $postal->getNumber($value);
    }

    /**
     * Setter for the tel property.
     *
     * @param string $value
     * @return string
     */
    protected function _setTel($value)
    {
        // 電話番号ユーティリティ取得
        $phone = new Phone();
        // 番号のみをセット
        return $phone->getNumber($value);
    }

    /**
     * Setter for the fax property.
     *
     * @param string $value
     * @return string
     */
    protected function _setFax($value)
    {
        // 電話番号ユーティリティ取得
        $phone = new Phone();
        // 番号のみをセット
        return $phone->getNumber($value);
    }

    /**
     * Getter for the postal_code property.
     *
     * @param string $value
     * @return string
     */
    protected function _getPostalCode($value)
    {
        // 郵便番号ユーティリティ取得
        $postal = new Postal();
        // 日本形式に変換して返却
        return $postal->getJapanFormat($value);
    }

    /**
     * Getter for the tel property.
     *
     * @param string $value
     * @return string
     */
    protected function _getTel($value)
    {
        // 電話番号ユーティリティ取得
        $phone = new Phone();
        // 日本形式に変換して返却
        return $phone->getJapanFormat($value);
    }

    /**
     * Getter for the fax property.
     *
     * @param string $value
     * @return string
     */
    protected function _getFax($value)
    {
        // 電話番号ユーティリティ取得
        $phone = new Phone();
        // 日本形式に変換して返却
        return $phone->getJapanFormat($value);
    }

    /**
     * [virtual] Getter for the state property.
     *
     * @return string
     */
    protected function _getState()
    {
        // "会員の状態"を取得
        $customer_state = Configure::read('CustomerState');
        $is_formal = $this->get('is_formal');
        // "会員の状態"を返却
        return $customer_state[$is_formal];
    }

    /**
     * [virtual] Getter for the billing_full property.
     *
     * @return string
     */
    protected function _getBillingFull()
    {
        // 初期化
        $billing_full = '';
        // "会社名(個人名)"取得
        $billing_full .= $this->get('billing');
        $billing_full .= (!empty($billing_full) && $this->has('position')) ? ' ' : null;
        // "部署名"取得
        $billing_full .= $this->get('position');
        $billing_full .= (!empty($billing_full) && $this->has('representative')) ? ' ' : null;
        // "担当者名"取得
        $billing_full .= $this->get('representative');
        // "請求先"を返却
        return $billing_full;
    }

    /**
     * [virtual] Getter for the billing_full_nl property.
     *
     * @return string
     */
    protected function _getBillingFullNl()
    {
        // 初期化
        $billing_full = '';
        // "会社名(個人名)"取得
        $billing_full .= $this->get('billing');
        $billing_full .= (!empty($billing_full) && $this->has('position')) ? PHP_EOL : null;
        // "部署名"取得
        $billing_full .= $this->get('position');
        $billing_full .= (!empty($billing_full) && $this->has('representative')) ? PHP_EOL : null;
        // "担当者名"取得
        $billing_full .= $this->get('representative');
        // "請求先"を返却
        return $billing_full;
    }

    /**
     * [virtual] Getter for the address_full property.
     *
     * @return string
     */
    protected function _getAddressFull()
    {
        // "都道府県"がセットされていなければ[null]返却
        if (!$this->has('prefecture_id')) {
            return null;
        } elseif ($this->has('prefecture')) {
            // "都道府県"を取得
            $prefecture = $this->get('prefecture')
                ->get('prefecture');
        } else {
            // "都道府県"を取得
            $prefecture = TableRegistry::get('Prefectures')
                ->get($this->get('prefecture_id'))
                ->get('prefecture');
        }
        // "住所(市区町村名)"を取得
        $address = $this->get('address');
        // "その他の住所(番地・ビル名)"を取得
        $address_etc = $this->get('address_etc');
        // "住所"を返却
        return "{$prefecture}{$address}{$address_etc}";
    }

    /**
     * [virtual] Getter for the label property.
     *
     * @return string|null
     */
    protected function _getLabel()
    {
        // "商品名","単価(税別)"がセットされていなければ[null]返却
        if (!$this->has('billing') || !$this->has('email')) {
            return null;
        }
        // "商品名","単価(税別)"を取得
        $billing = $this->get('billing');
        $email = $this->get('email');
        // "表示用ラベル"を返却
        return "{$billing} ({$email})";
    }
}
