<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\Number;

/**
 * MessageType Entity
 *
 * @property int $id
 * @property string $message_type
 * @property int $message_price
 * @property [virtual] int $tax_from_message_price
 * @property [virtual] int $tax_in_message_price
 * @property [virtual] string $label
 *
 * @property \App\Model\Entity\Order[] $orders
 */
class MessageType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * List of computed or virtual fields that **should** be included in JSON or array
     * representations of this Entity. If a field is present in both _hidden and _virtual
     * the field will **not** be in the array/json versions of the entity.
     *
     * @var array
     */
    protected $_virtual = [
        'tax_from_message_price',
        'tax_in_message_price',
        'label'
    ];

    /**
     * [virtual] Getter for the tax_from_message_price property.
     *
     * @return integer|null
     */
    protected function _getTaxFromMessagePrice()
    {
        // "メッセージ料(税別)"がセットされていなければnull返却
        if (!$this->has('message_price')) {
            return null;
        }
        // "メッセージ料(税別)"を取得
        $message_price = $this->get('message_price');
        // 現在の"消費税率(%)"を取得
        $tax_rate = TableRegistry::get('TaxRates')
            ->find('now')
            ->first()
            ->get('tax_rate');
        // "消費税の端数計算(処理名)"を取得
        $tax_calc = Configure::read('Tax.calc');
        $tax_calc = is_null($tax_calc) || !function_exists($tax_calc) ? 'floor' : $tax_calc;
        // "メッセージ料の消費税額"を返却
        return $tax_calc($message_price * ($tax_rate / 100));
    }

    /**
     * [virtual] Getter for the tax_in_message_price property.
     *
     * @return integer|null
     */
    protected function _getTaxInMessagePrice()
    {
        // "メッセージ料(税別)"がセットされていなければnull返却
        if (!$this->has('message_price')) {
            return null;
        }
        // "メッセージ料(税別)","メッセージ料の消費税額"を取得
        $message_price = $this->get('message_price');
        $tax = $this->get('tax_from_message_price');
        // "メッセージ料(税込)"を返却
        return $message_price + $tax;
    }

    /**
     * [virtual] Getter for the label property.
     *
     * @return string
     */
    protected function _getLabel()
    {
        // "メッセージタイプ","メッセージ料(税別)"がセットされていなければnull返却
        if (!$this->has('message_type') || !$this->has('message_price')) {
            return null;
        }
        // "メッセージタイプ","メッセージ料(税込)"を取得して表示用フォーマット
        $message_type = $this->get('message_type');
        $tax_in_message_price = Number::currency($this->get('tax_in_message_price'), 'JPY');
        // "表示用ラベル"を返却
        return "{$message_type} (+{$tax_in_message_price})";
    }
}
