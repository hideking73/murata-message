<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CustomerSignupToken Entity
 *
 * @property int $id
 * @property int $customer_id
 * @property string $token
 * @property \Cake\I18n\Time $expires
 *
 * @property \App\Model\Entity\Customer $customer
 */
class CustomerSignupToken extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
