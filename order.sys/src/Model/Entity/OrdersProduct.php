<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * OrdersProduct Entity
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $quantity
 * @property int $price
 * @property bool $is_discount
 * @property string $data
 * @property [virtual] int $tax_from_price
 * @property [virtual] int $tax_in_price
 * @property [virtual] int $subtotal
 * @property [virtual] int $tax_from_subtotal
 * @property [virtual] int $tax_in_subtotal
 *
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\Product $product
 */
class OrdersProduct extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * List of computed or virtual fields that **should** be included in JSON or array
     * representations of this Entity. If a field is present in both _hidden and _virtual
     * the field will **not** be in the array/json versions of the entity.
     *
     * @var array
     */
    protected $_virtual = [
        'tax_from_price',
        'tax_in_price',
        'subtotal',
        'tax_from_subtotal',
        'tax_in_subtotal',
    ];

    /**
     * Setter for the product_id property.
     *
     * @param integer $value
     * @return integer
     */
    protected function _setProductId($value)
    {
        // "商品情報"取得
        $product = TableRegistry::get('Products')
            ->find('active')
            ->where(['Products.id' => $value])
            ->first();
        // 購入時の"単価(税別)","割引対象商品フラグ","商品情報(シリアライズ)"をセット
        if ($product) {
            $this->set([
                'price' => $product->get('price'),
                'is_discount' => $product->get('is_discount'),
                'data' => json_encode($product->toArray())
            ]);
        }
        // "商品ID"はそのまま設定
        return $value;
    }

    /**
     * [virtual] Getter for the tax_from_price property.
     *
     * @return integer
     */
    protected function _getTaxFromPrice()
    {
        // 購入時の"単価(税別)"がセットされていなければ[0]返却
        if (!$this->has('price') && !$this->has('order_id')) {
            return 0;
        }
        // 購入時の"単価(税別)"を取得
        $price = $this->get('price');
        // "消費税率(%)","消費税の端数計算(処理名)"を取得
        if ($this->has('order_id') && !empty($this->get('order_id'))) {
            // 注文データ取得
            $order = TableRegistry::get('Orders')
                ->get($this->get('order_id'));
            // 購入時の"消費税率(%)"
            $tax_rate = $order->get('tax_rate');
            // 購入時の"消費税の端数計算(処理名)"
            $tax_calc = $order->get('tax_calc');
        } else {
            // 現在の"消費税率(%)"
            $tax_rate = TableRegistry::get('TaxRates')
                ->find('now')
                ->first()
                ->get('tax_rate');
            // システムの"消費税の端数計算(処理名)"
            $tax_calc = Configure::read('Tax.calc');
        }
        $tax_calc = is_null($tax_calc) || !function_exists($tax_calc) ? 'floor' : $tax_calc;
        // 購入時の"単価の消費税額"を返却
        return $tax_calc($price * ($tax_rate / 100));
    }

    /**
     * [virtual] Getter for the tax_in_price property.
     *
     * @return integer
     */
    protected function _getTaxInPrice()
    {
        // 購入時の"単価(税別)"がセットされていなければ[0]返却
        if (!$this->has('price')) {
            return 0;
        }
        // 購入時の"単価(税別)","単価の消費税額"を取得
        $price = $this->get('price');
        $tax = $this->get('tax_from_price');
        // 購入時の"単価(税込)"を返却
        return $price + $tax;
    }

    /**
     * [virtual] Getter for the subtotal property.
     *
     * @return integer
     */
    protected function _getSubtotal()
    {
        // 購入時の"単価(税別)","数量"がセットされていなければ[0]返却
        if (!$this->has('price') && !$this->has('quantity')) {
            return 0;
        }
        // 購入時の"単価(税別)","数量"を取得
        $price = $this->get('price');
        $quantity = $this->get('quantity');
        // 購入時の"小計(税別)"を返却
        return $price * $quantity;
    }

    /**
     * [virtual] Getter for the tax_from_subtotal property.
     *
     * @return integer
     */
    protected function _getTaxFromSubtotal()
    {
        // 購入時の"単価(税別)","数量"がセットされていなければ[0]返却
        if (!$this->has('price') && !$this->has('quantity')) {
            return 0;
        }
        // 購入時の"単価の消費税額","数量"を取得
        $tax = $this->get('tax_from_price');
        $quantity = $this->get('quantity');
        // 購入時の"小計の消費税額"を返却
        return $tax * $quantity;
    }

    /**
     * [virtual] Getter for the tax_in_subtotal property.
     *
     * @return integer
     */
    protected function _getTaxInSubtotal()
    {
        // 購入時の"単価(税別)","数量"がセットされていなければ[0]返却
        if (!$this->has('price') && !$this->has('quantity')) {
            return 0;
        }
        // 購入時の"単価(税別)","単価の消費税額","数量"を取得
        $price = $this->get('price');
        $tax = $this->get('tax_from_price');
        $quantity = $this->get('quantity');
        // 購入時の"小計(税込)"を返却
        return ($price + $tax) * $quantity;
    }
}
