<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ProductTypes
 * @property \Cake\ORM\Association\BelongsToMany $Orders
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null)
 *
 * @mixin \App\Model\Behavior\ActiveBehavior
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('products');
        $this->primaryKey('id');
        $this->displayField('label');

        $this->addBehavior('Active');
        $this->addBehavior('Timestamp');

        $this->belongsTo('ProductTypes', [
            'foreignKey' => 'product_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Orders', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'order_id',
            'joinTable' => 'orders_products'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('product_type_id', __('商品タイプが不正です。'))
            ->notEmpty('product_type_id', __('商品タイプを選択してください。'));

        $validator
            ->requirePresence('name', true, __('商品名を入力してください。'))
            ->notEmpty('name', __('商品名を入力してください。'));

        $validator
            ->requirePresence('comment', true, __('コメントを入力してください。'))
            ->notEmpty('comment', __('コメントを入力してください。'));

        $validator
            ->requirePresence('spec', true, __('仕様(容量・本数など)を入力してください。'))
            ->notEmpty('spec', __('仕様(容量・本数など)を入力してください。'));

        $validator
            ->requirePresence('unit', true, __('単位を入力してください。'))
            ->notEmpty('unit', __('単位を入力してください。'));

        $validator
            ->integer('price', __('単価(税別)には数値を入力してください。'))
            ->requirePresence('price', true, __('単価(税別)を入力してください。'))
            ->notEmpty('price', __('単価(税別)を入力してください。'));

        $validator
            ->allowEmpty('image');

        $validator
            ->boolean('is_discount', __('正しい割引対象状態を選択してください。'))
            ->inList('is_discount', [0, 1], __('正しい割引対象状態を選択してください。'))
            ->allowEmpty('is_discount', true);

        $validator
            ->boolean('is_active', __('正しいレコード状態を選択してください。'))
            ->inList('is_active', [0, 1], __('正しいレコード状態を選択してください。'))
            ->allowEmpty('is_active', true);

        return $validator;
    }

    /**
     * Edit validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationEdit(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_type_id'], 'ProductTypes'));

        return $rules;
    }

    /**
     * Finds for offering record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findOffering(Query $query, array $options)
    {
        return $query
            ->find('active')
            ->where(['Products.product_type_id' => Configure::read('ProductType.offering')]);
    }

    /**
     * Finds for celebrate record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findCelebrate(Query $query, array $options)
    {
        return $query
            ->find('active')
            ->where(['Products.product_type_id' => Configure::read('ProductType.celebrate')]);
    }
}
