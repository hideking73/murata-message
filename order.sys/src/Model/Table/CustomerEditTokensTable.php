<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CustomerEditTokens Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Customers
 *
 * @method \App\Model\Entity\CustomerEditToken get($primaryKey, $options = [])
 * @method \App\Model\Entity\CustomerEditToken newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CustomerEditToken[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CustomerEditToken|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CustomerEditToken patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CustomerEditToken[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CustomerEditToken findOrCreate($search, callable $callback = null)
 */
class CustomerEditTokensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('customer_edit_tokens');
        $this->primaryKey('id');
        $this->displayField('customer_id');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('data', true, __('編集内容を入力してください。'))
            ->notEmpty('data', __('編集内容を入力してください。'));

        $validator
            ->requirePresence('token', true, __('トークンを入力してください。'))
            ->notEmpty('token', __('トークンを入力してください。'));

        $validator
            ->dateTime('expires', __('正しい有効期限を入力してください。'))
            ->requirePresence('expires', true, __('有効期限を入力してください。'))
            ->notEmpty('expires', __('有効期限を入力してください。'));

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }
}
