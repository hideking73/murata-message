<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MessageTypes Model
 *
 * @property \Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\MessageType get($primaryKey, $options = [])
 * @method \App\Model\Entity\MessageType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MessageType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MessageType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MessageType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MessageType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MessageType findOrCreate($search, callable $callback = null)
 */
class MessageTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('message_types');
        $this->primaryKey('id');
        $this->displayField('label');

        $this->hasMany('Orders', [
            'foreignKey' => 'message_type_id'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('message_type', true, __('メッセージタイプを入力してください。'))
            ->notEmpty('message_type', __('メッセージタイプを入力してください。'));

        $validator
            ->integer('message_price', __('メッセージ料(税別)を入力してください。'))
            ->requirePresence('message_price', true, __('メッセージ料(税別)を入力してください。'))
            ->notEmpty('message_price', __('メッセージ料(税別)を入力してください。'));

        return $validator;
    }
}
