<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;
use DateTime;

/**
 * Customers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Prefectures
 * @property \Cake\ORM\Association\HasOne $CustomerSignupTokens
 * @property \Cake\ORM\Association\HasOne $CustomerEditTokens
 * @property \Cake\ORM\Association\HasOne $ResetPasswordTokens
 * @property \Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Customer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Customer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Customer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Customer findOrCreate($search, callable $callback = null)
 *
 * @mixin \App\Model\Behavior\ActiveBehavior
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustomersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('customers');
        $this->primaryKey('id');
        $this->displayField('label');

        $this->addBehavior('Active');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Prefectures', [
            'foreignKey' => 'prefecture_id',
            'joinType' => 'INNER'
        ]);
        $this->hasOne('CustomerSignupTokens', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasOne('CustomerEditTokens', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasOne('ResetPasswordTokens', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'customer_id'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // メールアドレスを再入力する場合
        $email_again = function ($context) {
            // 新規は必須、更新はメールアドレスが変更されている場合
            // return $context['newRecord'] || !$context['newRecord'] && $context['providers']['entity']->dirty('email');
            // TODO: (臨時) 新規は必須、更新はメールアドレスが空じゃない場合
            return $context['newRecord'] || !$context['newRecord'] && !empty($context['data']['email']);
        };

        // パスワードを再入力する場合
        $password_again = function ($context) {
            // 新規は必須、更新はパスワードが空じゃない場合
            return $context['newRecord'] || !$context['newRecord'] && !empty($context['data']['password']);
        };

        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->jpEmail('email', false, __('正しいメールアドレスを入力してください。'))
            ->requirePresence('email', true, __('メールアドレスを入力してください。'))
            ->notEmpty('email', __('メールアドレスを入力してください。'))
            ->unique('email', null, __('このメールアドレスは使用できません。'));

        $validator
            ->sameAs('email_again', 'email', __('メールアドレスが一致しません。'))
            ->notEmpty('email_again', __('メールアドレスを再入力してください。'), $email_again);

        $validator
            ->alphaNumericSymbol('password', __('パスワードは半角英数記号で入力してください。'))
            ->minLength('password', 8, __('パスワードは8文字から32文字までの間で入力してください。'))
            ->maxLength('password', 32, __('パスワードは8文字から32文字までの間で入力してください。'))
            ->requirePresence('password', 'create', __('パスワードを入力してください。'))
            ->allowEmpty('password', 'update', __('パスワードを入力してください。'));

        $validator
            ->sameAs('password_again', 'password', __('パスワードが一致しません。'))
            ->notEmpty('password_again', __('パスワードを再入力してください。'), $password_again);

        $validator
            ->requirePresence('billing', 'create', __('請求先(会社名または個人名)を入力してください。'))
            ->notEmpty('billing', __('請求先(会社名または個人名)を入力してください。'));

        $validator
            ->boolean('is_billing_external', __('正しい外字設定を選択してください。'))
            ->inList('is_billing_external', [0, 1], __('正しい外字設定を選択してください。'))
            ->allowEmpty('is_billing_external', true);

        $validator
            ->allowEmpty('position');

        $validator
            ->allowEmpty('representative');

        $validator
            ->jpPostalCode('postal_code', __('正しい郵便番号を入力してください。'))
            ->requirePresence('postal_code', true, __('郵便番号を入力してください。'))
            ->notEmpty('postal_code', __('郵便番号を入力してください。'));

        $validator
            ->integer('prefecture_id', __('正しい都道府県を選択してください。'))
            ->requirePresence('prefecture_id', true, __('都道府県を選択してください。'))
            ->notEmpty('prefecture_id', __('都道府県を選択してください。'));

        $validator
            ->requirePresence('address', true, __('市区町村名を入力してください。'))
            ->notEmpty('address', __('市区町村名を入力してください。'));

        $validator
            ->requirePresence('address_etc', true, __('番地・ビル名を入力してください。'))
            ->notEmpty('address_etc', __('番地・ビル名を入力してください。'));

        $validator
            ->jpPhoneNumber('tel', __('正しい電話番号を入力してください。'))
            ->requirePresence('tel', true, __('電話番号を入力してください。'))
            ->notEmpty('tel', __('電話番号を入力してください。'));

        $validator
            ->jpPhoneNumber('fax', __('正しいファックス番号を入力してください。'))
            ->allowEmpty('fax');

        $validator
            ->integer('discount_rate', __('正しい割引率(%)を入力してください。'))
            ->requirePresence('discount_rate', true, __('割引率(%)を入力してください。'))
            ->notEmpty('discount_rate', __('割引率(%)を入力してください。'));

        $validator
            ->integer('closing_day', __('正しい締日を入力してください。'))
            ->allowEmpty('closing_day');

        $validator
            ->boolean('is_formal', __('正しい会員状態を選択してください。'))
            ->inList('is_formal', [0, 1], __('正しい会員状態を選択してください。'))
            ->allowEmpty('is_formal', true);

        $validator
            ->boolean('is_active', __('正しいレコード状態を選択してください。'))
            ->inList('is_active', [0, 1], __('正しいレコード状態を選択してください。'))
            ->allowEmpty('is_active', true);

        return $validator;
    }

    /**
     * Signup validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationSignup(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('discount_rate')
            ->remove('closing_day')
            ->remove('is_formal')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Edit validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationEdit(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('discount_rate')
            ->remove('closing_day')
            ->remove('is_formal')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Reset Password validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationResetPassword(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('email')
            ->remove('email_again')
            ->remove('billing')
            ->remove('position')
            ->remove('representative')
            ->remove('postal_code')
            ->remove('prefecture_id')
            ->remove('address')
            ->remove('address_etc')
            ->remove('tel')
            ->remove('fax')
            ->remove('discount_rate')
            ->remove('closing_day')
            ->remove('is_formal')
            ->remove('is_active');

        $validator
            ->notEmpty('password', __('パスワードを入力してください。'));

        $validator
            ->notEmpty('password_again', __('パスワードを再入力してください。'));

        return $validator;
    }

    /**
     * Admin Register validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationAdminRegister(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('email_again')
            ->remove('discount_rate')
            ->remove('closing_day')
            ->remove('is_formal')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['prefecture_id'], 'Prefectures'));

        return $rules;
    }

    /**
     * Before save listener.
     *
     * @param \Cake\Event\Event $event The beforeSave event that was fired
     * @param \Cake\Datasource\EntityInterface $entity The entity that is going to be saved
     * @param \ArrayObject $options the options passed to the save method
     * @return void
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        // パスワード更新判断
        $password = $entity->get('password');
        if (!empty($password)) {
            // "パスワード"の暗号化
            $hasher = new DefaultPasswordHasher();
            $entity->set(['password' => $hasher->hash($password)]);
        } else {
            // "パスワード"が空の場合は変更されていないことにする
            $entity->dirty('password', false);
        }
    }

    /**
     * Finds for contain detail.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findDetail(Query $query, array $options)
    {
        return $query
            ->contain([
                'Prefectures',
                'Orders' => [
                    'OrderStates',
                    'PaymentTypes',
                    'DeliveryTypes',
                    'Facilities',
                    'DeliveryPrefectures',
                    'MessageTypes',
                    'MessageTemplates',
                    'Prefectures',
                    'Products'
                ],
            ]);
    }

    /**
     * Finds for ignore password.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findIgnorePassword(Query $query, array $options)
    {
        return $query
            ->select([
                'id' => 'Customers.id',
                'email' => 'Customers.email',
                'billing' => 'Customers.billing',
                'is_billing_external' => 'Customers.is_billing_external',
                'position' => 'Customers.position',
                'representative' => 'Customers.representative',
                'postal_code' => 'Customers.postal_code',
                'prefecture_id' => 'Customers.prefecture_id',
                'address' => 'Customers.address',
                'address_etc' => 'Customers.address_etc',
                'tel' => 'Customers.tel',
                'fax' => 'Customers.fax',
                'discount_rate' => 'Customers.discount_rate',
                'closing_day' => 'Customers.closing_day',
                'is_formal' => 'Customers.is_formal',
                'created' => 'Customers.created',
                'modified' => 'Customers.modified'
            ]);
    }

    /**
     * Finds for activation.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findActivation(Query $query, array $options)
    {
        $token = $options['token'];
        return $query
            ->find('active')
            ->find('temporary')
            ->find('ignorePassword')
            ->matching('CustomerSignupTokens', function ($query) use ($token) {
                return $query
                    ->where(['CustomerSignupTokens.token' => $token]);
            })
            ->select([
                'token_id' => 'CustomerSignupTokens.id',
                'token' => 'CustomerSignupTokens.token',
                'expires' => 'CustomerSignupTokens.expires'
            ]);
    }

    /**
     * Finds for apply.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findApply(Query $query, array $options)
    {
        $token = $options['token'];
        return $query
            ->find('active')
            ->find('formal')
            ->find('ignorePassword')
            ->matching('CustomerEditTokens', function ($query) use ($token) {
                return $query
                    ->where(['CustomerEditTokens.token' => $token]);
            })
            ->select([
                'token_id' => 'CustomerEditTokens.id',
                'token' => 'CustomerEditTokens.token',
                'expires' => 'CustomerEditTokens.expires'
            ]);
    }

    /**
     * Finds for reset password.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findResetPassword(Query $query, array $options)
    {
        $token = $options['token'];
        return $query
            ->find('active')
            ->find('formal')
            ->find('ignorePassword')
            ->matching('ResetPasswordTokens', function ($query) use ($token) {
                return $query
                    ->where(['ResetPasswordTokens.token' => $token]);
            })
            ->select([
                'token_id' => 'ResetPasswordTokens.id',
                'token' => 'ResetPasswordTokens.token',
                'expires' => 'ResetPasswordTokens.expires'
            ]);
    }

    /**
     * Finds for csv export.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findExportCsv(Query $query, array $options)
    {
        return $query
            ->find('active')
            ->contain([
                'Prefectures',
            ]);
    }

    /**
     * Finds for authentication record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findAuthentication(Query $query, array $options)
    {
        return $query
            ->find('active')
            ->find('formal')
            ->find('ignorePassword')
            ->contain([
                'Prefectures',
                'CustomerSignupTokens' => function (Query $query) {
                    return $query
                        ->where(['CustomerSignupTokens.expires >=' => new DateTime('now')]);
                },
                'CustomerEditTokens' => function (Query $query) {
                    return $query
                        ->where(['CustomerEditTokens.expires >=' => new DateTime('now')]);
                },
                'ResetPasswordTokens' => function (Query $query) {
                    return $query
                        ->where(['ResetPasswordTokens.expires >=' => new DateTime('now')]);
                }
            ])
            ->select([
                'password' => 'Customers.password',
                'customer_signup_token' => 'CustomerSignupTokens.token',
                'customer_edit_token' => 'CustomerEditTokens.token',
                'reset_password_token' => 'ResetPasswordTokens.token'
            ]);
    }

    /**
     * Finds for formal record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findFormal(Query $query, array $options)
    {
        return $query
            ->where(['Customers.is_formal' => true]);
    }

    /**
     * Finds for temporary record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findTemporary(Query $query, array $options)
    {
        return $query
            ->where(['Customers.is_formal' => false]);
    }

    /**
     * Change to formal a record.
     *
     * @param \Cake\Datasource\EntityInterface $entity
     * @param array $options
     * @return boolean
     */
    public function formal(EntityInterface $entity, array $options = [])
    {
        // 対象エンティティの正会員フラグに正会員をセット
        $entity->set(['is_formal' => true]);
        // 保存処理を実行し結果を返却
        return $this->save($entity, $options);
    }

    /**
     * Change to temporary a record.
     *
     * @param \Cake\Datasource\EntityInterface $entity
     * @param array $options
     * @return boolean
     */
    public function temporary(EntityInterface $entity, array $options = [])
    {
        // 対象エンティティの正会員フラグに仮会員をセット
        $entity->set(['is_formal' => false]);
        // 保存処理を実行し結果を返却
        return $this->save($entity, $options);
    }

    /**
     * Change to formal all records.
     *
     * @param array $conditions
     * @return integer
     */
    public function formalAll(array $conditions = [])
    {
        // 更新フィールドと更新対象条件の指定
        $fields = ['is_formal' => true];
        $conditions = array_merge(['Customers.is_formal' => false], $conditions);
        // 更新処理を実行し適用されたレコード数を返却
        return $this->updateAll($fields, $conditions);
    }

    /**
     * Change to temporary all records.
     *
     * @param array $conditions
     * @return integer
     */
    public function temporaryAll(array $conditions = [])
    {
        // 更新フィールドと更新対象条件の指定
        $fields = ['is_formal' => false];
        $conditions = array_merge(['Customers.is_formal' => true], $conditions);
        // 更新処理を実行し適用されたレコード数を返却
        return $this->updateAll($fields, $conditions);
    }
}
