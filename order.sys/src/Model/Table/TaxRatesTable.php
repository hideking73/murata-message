<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DateTime;

/**
 * TaxRates Model
 *
 * @method \App\Model\Entity\TaxRate get($primaryKey, $options = [])
 * @method \App\Model\Entity\TaxRate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TaxRate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TaxRate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaxRate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TaxRate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TaxRate findOrCreate($search, callable $callback = null)
 */
class TaxRatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tax_rates');
        $this->primaryKey('id');
        $this->displayField('tax_rate');

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('tax_rate', __('消費税率(%)が不正です。'))
            ->requirePresence('tax_rate', true, __('消費税率(%)を入力してください。'))
            ->notEmpty('tax_rate', __('消費税率(%)を入力してください。'));

        $validator
            ->dateTime('apply', __('適用日時が不正です。'))
            ->requirePresence('apply', true, __('適用日時を入力してください。'))
            ->notEmpty('apply', __('適用日時を入力してください。'));

        return $validator;
    }

    /**
     * Finds for now tax-rate.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findNow(Query $query, array $options)
    {
        return $query
            ->where(['apply <=' => new DateTime('now')])
            ->order(['apply' => 'DESC'])
            ->limit(1);
    }
}
