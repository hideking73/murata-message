<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrderStates Model
 *
 * @property \Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\OrderState get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrderState newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrderState[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrderState|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderState patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrderState[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrderState findOrCreate($search, callable $callback = null)
 */
class OrderStatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('order_states');
        $this->primaryKey('id');
        $this->displayField('order_state');

        $this->hasMany('Orders', [
            'foreignKey' => 'order_state_id'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('order_state', true, __('受注対応状況を入力してください。'))
            ->notEmpty('order_state', __('受注対応状況を入力してください。'));

        return $validator;
    }
}
