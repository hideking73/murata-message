<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * MessageTemplates Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ProductTypes
 * @property \Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\MessageTemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\MessageTemplate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MessageTemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MessageTemplate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MessageTemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MessageTemplate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MessageTemplate findOrCreate($search, callable $callback = null)
 *
 * @mixin \App\Model\Behavior\ActiveBehavior
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MessageTemplatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('message_templates');
        $this->primaryKey('id');
        $this->displayField('message_template');

        $this->addBehavior('Active');
        $this->addBehavior('Timestamp');

        $this->belongsTo('ProductTypes', [
            'foreignKey' => 'product_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'message_template_id'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('product_type_id', __('正しい商品タイプを選択してください。'))
            ->notEmpty('product_type_id', __('商品タイプを選択してください。'));

        $validator
            ->requirePresence('message_template', true, __('メッセージテンプレートを入力してください。'))
            ->notEmpty('message_template', __('メッセージテンプレートを入力してください。'));

        $validator
            ->boolean('is_active', __('正しいレコード状態を選択してください。'))
            ->inList('is_active', [0, 1], __('正しいレコード状態を選択してください。'))
            ->allowEmpty('is_active', true);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_type_id'], 'ProductTypes'));

        return $rules;
    }

    /**
     * Finds for offering record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findOffering(Query $query, array $options)
    {
        return $query
            ->find('active')
            ->where(['MessageTemplates.product_type_id' => Configure::read('ProductType.offering')]);
    }

    /**
     * Finds for celebrate record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findCelebrate(Query $query, array $options)
    {
        return $query
            ->find('active')
            ->where(['MessageTemplates.product_type_id' => Configure::read('ProductType.celebrate')]);
    }
}
