<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliveryTypes Model
 *
 * @method \App\Model\Entity\DeliveryType get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeliveryType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DeliveryType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliveryType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryType findOrCreate($search, callable $callback = null)
 */
class DeliveryTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('delivery_types');
        $this->primaryKey('id');
        $this->displayField('delivery_type');

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('delivery_type', true, __('お届け先タイプを入力してください。'))
            ->notEmpty('delivery_type', __('お届け先タイプを入力してください。'));

        return $validator;
    }
}
