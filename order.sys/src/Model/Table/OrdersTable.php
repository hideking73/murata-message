<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * Orders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OrderStates
 * @property \Cake\ORM\Association\BelongsTo $PaymentTypes
 * @property \Cake\ORM\Association\BelongsTo $DeliveryTypes
 * @property \Cake\ORM\Association\BelongsTo $Facilities
 * @property \Cake\ORM\Association\BelongsTo $DeliveryPrefectures
 * @property \Cake\ORM\Association\BelongsTo $MessageTypes
 * @property \Cake\ORM\Association\BelongsTo $MessageTemplates
 * @property \Cake\ORM\Association\BelongsTo $Prefectures
 * @property \Cake\ORM\Association\BelongsTo $Customers
 * @property \Cake\ORM\Association\BelongsToMany $Products
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null)
 *
 * @mixin \App\Model\Behavior\ActiveBehavior
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('orders');
        $this->primaryKey('id');
        $this->displayField('sender');

        $this->addBehavior('Active');
        $this->addBehavior('Timestamp');

        $this->belongsTo('OrderStates', [
            'foreignKey' => 'order_state_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('PaymentTypes', [
            'foreignKey' => 'payment_type_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('DeliveryTypes', [
            'foreignKey' => 'delivery_type_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Facilities', [
            'foreignKey' => 'facility_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('DeliveryPrefectures', [
            'className' => 'Prefectures',
            'foreignKey' => 'delivery_prefecture_id',
            'propertyName' => 'delivery_prefecture',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('MessageTypes', [
            'foreignKey' => 'message_type_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('MessageTemplates', [
            'foreignKey' => 'message_template_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Prefectures', [
            'foreignKey' => 'prefecture_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsToMany('Products', [
            'foreignKey' => 'order_id',
            'targetForeignKey' => 'product_id',
            'joinTable' => 'orders_products'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // "村田葬儀社の葬祭式場"の場合
        $delivery_type_murata = function ($context) {
            if (isset($context['data']['delivery_type_id'])) {
                return $context['data']['delivery_type_id'] == Configure::read('DeliveryType.murata');
            } else {
                return false;
            }
        };

        // "他社の式場"の場合
        $delivery_type_other = function ($context) {
            if (isset($context['data']['delivery_type_id'])) {
                return $context['data']['delivery_type_id'] == Configure::read('DeliveryType.other');
            } else {
                return false;
            }
        };

        // 名札の"会社名"が空の場合
        $empty_company = function ($context) {
            return empty($context['data']['company']);
        };

        // 名札の"肩書・差出人名(お名前)"が空の場合
        $empty_sender = function ($context) {
            return empty($context['data']['sender']);
        };

        // "3つの文章から選択"の場合
        $message_type_template = function ($context) {
            if (isset($context['data']['message_type_id'])) {
                return $context['data']['message_type_id'] == Configure::read('MessageType.template');
            } else {
                return false;
            }
        };

        // "オリジナルで作成"の場合
        $message_type_original = function ($context) {
            if (isset($context['data']['message_type_id'])) {
                return $context['data']['message_type_id'] == Configure::read('MessageType.original');
            } else {
                return false;
            }
        };

        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('order_state_id', __('正しい受注状態を選択してください。'))
            ->requirePresence('order_state_id', true, __('受注状態を選択してください。'))
            ->notEmpty('order_state_id', __('受注状態を選択してください。'));

        $validator
            ->integer('payment_type_id', __('正しい決済方法を選択してください。'))
            ->requirePresence('payment_type_id', true, __('決済方法を選択してください。'))
            ->notEmpty('payment_type_id', __('決済方法を選択してください。'));

        $validator
            ->requirePresence('delivery', true, __('お届け先を入力してください。'))
            ->notEmpty('delivery', __('お届け先を入力してください。'));

        $validator
            ->integer('delivery_type_id', __('正しいお届け先タイプを選択してください。'))
            ->requirePresence('delivery_type_id', true, __('お届け先タイプを選択してください。'))
            ->notEmpty('delivery_type_id', __('お届け先タイプを選択してください。'));

        $validator
            ->integer('facility_id', __('正しい施設を選択してください。'))
            ->requirePresence('facility_id', $delivery_type_murata, __('施設を選択してください。'))
            ->notEmpty('facility_id', __('施設を選択してください。'), $delivery_type_murata);

        $validator
            ->allowEmpty('delivery_postal_code', true);

        $validator
            ->integer('delivery_prefecture_id', __('正しい都道府県を選択してください。'))
            ->requirePresence('delivery_prefecture_id', $delivery_type_other, __('都道府県を選択してください。'))
            ->notEmpty('delivery_prefecture_id', __('都道府県を選択してください。'), $delivery_type_other);

        $validator
            ->requirePresence('delivery_address', $delivery_type_other, __('市区町村名を入力してください。'))
            ->notEmpty('delivery_address', __('市区町村名を入力してください。'), $delivery_type_other);

        $validator
            ->requirePresence('delivery_address_etc', $delivery_type_other, __('番地・ビル名を入力してください。'))
            ->notEmpty('delivery_address_etc', __('番地・ビル名を入力してください。'), $delivery_type_other);

        $validator
            ->requirePresence('company', $empty_sender, __('会社名または肩書・差出人名(お名前)を入力してください。'))
            ->notEmpty('company', __('会社名または肩書・差出人名(お名前)を入力してください。'), $empty_sender);

        $validator
            ->requirePresence('sender', $empty_company, __('会社名または肩書・差出人名(お名前)を入力してください。'))
            ->notEmpty('sender', __('会社名または肩書・差出人名(お名前)を入力してください。'), $empty_company);

        $validator
            ->boolean('is_sender_external', __('正しい外字設定を選択してください。'))
            ->inList('is_sender_external', [0, 1], __('正しい外字設定を選択してください。'))
            ->allowEmpty('is_sender_external', true);

        $validator
            ->allowEmpty('memo', true);

        $validator
            ->allowEmpty('other_detail', true);

        $validator
            ->allowEmpty('message_type_id', true);

        $validator
            ->integer('message_template_id', __('正しいメッセージ内容を選択してください。'))
            ->notEmpty('message_template_id', __('メッセージ内容を選択してください。'), $message_type_template);

        $validator
            ->maxLength('original_message', 300, __('メッセージ内容は300文字以内で入力してください。'))
            ->requirePresence('original_message', $message_type_original, __('メッセージ内容を入力してください。'))
            ->notEmpty('original_message', __('メッセージ内容を入力してください。'), $message_type_original);

        $validator
            ->integer('message_price', __('正しいメッセージ料を入力してください。'))
            ->allowEmpty('message_price', true);

        $validator
            ->inList('tax_calc', ['floor', 'ceil', 'round'], __('正しい消費税の端数計算方法を入力してください。'))
            ->allowEmpty('tax_calc', true);

        $validator
            ->integer('tax_rate', __('正しい消費税率を入力してください。'))
            ->allowEmpty('tax_rate', true);

        $validator
            ->integer('carriage', __('正しい送料を入力してください。'))
            ->allowEmpty('carriage', true);

        $validator
            ->requirePresence('billing', true, __('会社名(個人名)を入力してください。'))
            ->notEmpty('billing', __('会社名(個人名)を入力してください。'));

        $validator
            ->boolean('is_billing_external', __('正しい外字設定を選択してください。'))
            ->inList('is_billing_external', [0, 1], __('正しい外字設定を選択してください。'))
            ->allowEmpty('is_billing_external', true);

        $validator
            ->allowEmpty('position', true);

        $validator
            ->allowEmpty('representative', true);

        $validator
            ->jpPostalCode('postal_code', __('正しい電話番号を入力してください。'))
            ->requirePresence('postal_code', true, __('郵便番号を入力してください。'))
            ->notEmpty('postal_code', __('郵便番号を入力してください。'));

        $validator
            ->integer('prefecture_id', __('正しい都道府県を選択してください。'))
            ->requirePresence('prefecture_id', true, __('都道府県を選択してください。'))
            ->notEmpty('prefecture_id', __('都道府県を選択してください。'));

        $validator
            ->requirePresence('address', true, __('市区町村名を入力してください。'))
            ->notEmpty('address', __('市区町村名を入力してください。'));

        $validator
            ->requirePresence('address_etc', true, __('番地・ビル名を入力してください。'))
            ->notEmpty('address_etc', __('番地・ビル名を入力してください。'));

        $validator
            ->jpPhoneNumber('tel', __('正しい電話番号を入力してください。'))
            ->requirePresence('tel', true, __('電話番号を入力してください。'))
            ->notEmpty('tel', __('電話番号を入力してください。'));

        $validator
            ->jpPhoneNumber('fax', __('正しいファックス番号を入力してください。'))
            ->allowEmpty('fax', true);

        $validator
            ->jpEmail('email', false, __('正しいメールアドレスを入力してください。'))
            ->requirePresence('email', true, __('メールアドレスを入力してください。'))
            ->notEmpty('email', __('メールアドレスを入力してください。'));

        $validator
            ->boolean('is_active', __('正しいレコード状態を選択してください。'))
            ->inList('is_active', [0, 1], __('正しいレコード状態を選択してください。'))
            ->allowEmpty('is_active', true);

        return $validator;
    }

    /**
     * Order Delivery validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationOrderDelivery(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('order_state_id')
            ->remove('payment_type_id')
            ->remove('message_type_id')
            ->remove('message_template_id')
            ->remove('original_message')
            ->remove('message_price')
            ->remove('tax_calc')
            ->remove('tax_rate')
            ->remove('discount_calc')
            ->remove('discount_rate')
            ->remove('carriage')
            ->remove('billing')
            ->remove('position')
            ->remove('representative')
            ->remove('postal_code')
            ->remove('prefecture_id')
            ->remove('address')
            ->remove('address_etc')
            ->remove('tel')
            ->remove('fax')
            ->remove('email')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Order Offering Delivery validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationOrderOfferingDelivery(Validator $validator)
    {
        $validator = $this->validationOrderDelivery($validator);

        $validator
            ->requirePresence('delivery', true, __('故人名を入力してください。'))
            ->notEmpty('delivery', __('故人名を入力してください。'));

        return $validator;
    }

    /**
     * Order Celebrate Delivery validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationOrderCelebrateDelivery(Validator $validator)
    {
        $validator = $this->validationOrderDelivery($validator);

        $validator
            ->requirePresence('delivery', true, __('ご家族名を入力してください。'))
            ->notEmpty('delivery', __('ご家族名を入力してください。'));

        return $validator;
    }

    /**
     * Order Select validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationOrderSelect(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('order_state_id')
            ->remove('payment_type_id')
            ->remove('delivery')
            ->remove('delivery_type_id')
            ->remove('facility_id')
            ->remove('delivery_postal_code')
            ->remove('delivery_prefecture_id')
            ->remove('delivery_address')
            ->remove('delivery_address_etc')
            ->remove('carriage')
            ->remove('company')
            ->remove('sender')
            ->remove('memo')
            ->remove('other_detail')
            ->remove('message_type_id')
            ->remove('message_template_id')
            ->remove('original_message')
            ->remove('message_price')
            ->remove('tax_calc')
            ->remove('tax_rate')
            ->remove('discount_calc')
            ->remove('discount_rate')
            ->remove('billing')
            ->remove('position')
            ->remove('representative')
            ->remove('postal_code')
            ->remove('prefecture_id')
            ->remove('address')
            ->remove('address_etc')
            ->remove('tel')
            ->remove('fax')
            ->remove('email')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Order Cart validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationOrderCart(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('order_state_id')
            ->remove('payment_type_id')
            ->remove('delivery')
            ->remove('delivery_type_id')
            ->remove('facility_id')
            ->remove('delivery_postal_code')
            ->remove('delivery_prefecture_id')
            ->remove('delivery_address')
            ->remove('delivery_address_etc')
            ->remove('carriage')
            ->remove('company')
            ->remove('sender')
            ->remove('memo')
            ->remove('other_detail')
            ->remove('message_template_id')
            ->remove('original_message')
            ->remove('message_price')
            ->remove('tax_calc')
            ->remove('tax_rate')
            ->remove('discount_calc')
            ->remove('discount_rate')
            ->remove('billing')
            ->remove('position')
            ->remove('representative')
            ->remove('postal_code')
            ->remove('prefecture_id')
            ->remove('address')
            ->remove('address_etc')
            ->remove('tel')
            ->remove('fax')
            ->remove('email')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Order Message validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationOrderMessage(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('order_state_id')
            ->remove('payment_type_id')
            ->remove('delivery')
            ->remove('delivery_type_id')
            ->remove('facility_id')
            ->remove('delivery_postal_code')
            ->remove('delivery_prefecture_id')
            ->remove('delivery_address')
            ->remove('delivery_address_etc')
            ->remove('carriage')
            ->remove('company')
            ->remove('sender')
            ->remove('memo')
            ->remove('other_detail')
            ->remove('message_type_id')
            ->remove('tax_calc')
            ->remove('tax_rate')
            ->remove('discount_calc')
            ->remove('discount_rate')
            ->remove('billing')
            ->remove('position')
            ->remove('representative')
            ->remove('postal_code')
            ->remove('prefecture_id')
            ->remove('address')
            ->remove('address_etc')
            ->remove('tel')
            ->remove('fax')
            ->remove('email')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Order Billing validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationOrderBilling(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('order_state_id')
            ->remove('payment_type_id')
            ->remove('delivery')
            ->remove('delivery_type_id')
            ->remove('facility_id')
            ->remove('delivery_postal_code')
            ->remove('delivery_prefecture_id')
            ->remove('delivery_address')
            ->remove('delivery_address_etc')
            ->remove('carriage')
            ->remove('company')
            ->remove('sender')
            ->remove('memo')
            ->remove('other_detail')
            ->remove('message_type_id')
            ->remove('message_template_id')
            ->remove('original_message')
            ->remove('message_price')
            ->remove('tax_calc')
            ->remove('tax_rate')
            ->remove('discount_calc')
            ->remove('discount_rate')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Order Payment validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationOrderPayment(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->remove('order_state_id')
            ->remove('delivery')
            ->remove('delivery_type_id')
            ->remove('facility_id')
            ->remove('delivery_postal_code')
            ->remove('delivery_prefecture_id')
            ->remove('delivery_address')
            ->remove('delivery_address_etc')
            ->remove('carriage')
            ->remove('company')
            ->remove('sender')
            ->remove('memo')
            ->remove('other_detail')
            ->remove('message_type_id')
            ->remove('message_template_id')
            ->remove('original_message')
            ->remove('message_price')
            ->remove('tax_calc')
            ->remove('tax_rate')
            ->remove('discount_calc')
            ->remove('discount_rate')
            ->remove('billing')
            ->remove('position')
            ->remove('representative')
            ->remove('postal_code')
            ->remove('prefecture_id')
            ->remove('address')
            ->remove('address_etc')
            ->remove('tel')
            ->remove('fax')
            ->remove('email')
            ->remove('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_state_id'], 'OrderStates'));
        $rules->add($rules->existsIn(['payment_type_id'], 'PaymentTypes'));
        $rules->add($rules->existsIn(['delivery_type_id'], 'DeliveryTypes'));
        $rules->add($rules->existsIn(['facility_id'], 'Facilities'));
        $rules->add($rules->existsIn(['delivery_prefecture_id'], 'DeliveryPrefectures'));
        $rules->add($rules->existsIn(['message_type_id'], 'MessageTypes'));
        $rules->add($rules->existsIn(['message_template_id'], 'MessageTemplates'));
        $rules->add($rules->existsIn(['prefecture_id'], 'Prefectures'));
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }

    /**
     * Finds for contain detail.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findDetail(Query $query, array $options)
    {
        return $query
            ->contain([
                'OrderStates',
                'PaymentTypes',
                'DeliveryTypes',
                'Facilities',
                'DeliveryPrefectures',
                'MessageTypes',
                'MessageTemplates',
                'Prefectures',
                'Products' => [
                    'ProductTypes'
                ],
                'Customers' => [
                    'Prefectures'
                ]
            ]);
    }

    /**
     * Finds for csv export.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findExportCsv(Query $query, array $options)
    {
        return $query
            ->find('detail')
            ->find('active');
    }
}
