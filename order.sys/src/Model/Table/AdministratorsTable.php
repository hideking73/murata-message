<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

/**
 * Administrators Model
 *
 * @method \App\Model\Entity\Administrator get($primaryKey, $options = [])
 * @method \App\Model\Entity\Administrator newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Administrator[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Administrator|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Administrator patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Administrator[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Administrator findOrCreate($search, callable $callback = null)
 *
 * @mixin \App\Model\Behavior\ActiveBehavior
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdministratorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('administrators');
        $this->primaryKey('id');
        $this->displayField('name');

        $this->addBehavior('Active');
        $this->addBehavior('Timestamp');

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // パスワードを再入力する場合
        $password_again = function ($context) {
            // 新規は必須、更新はパスワードが空じゃない場合
            return $context['newRecord'] || !$context['newRecord'] && !empty($context['data']['password']);
        };

        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->alphaNumericUnderscore('username', __('ログインIDは半角英数およびアンダーバー(_)で入力してください。'))
            ->requirePresence('username', true, __('ログインIDを入力してください。'))
            ->notEmpty('username', __('ログインIDを入力してください。'))
            ->unique('username', null, __('このログインIDは使用できません。'));

        $validator
            ->alphaNumericSymbol('password', __('パスワードは半角英数記号で入力してください。'))
            ->minLength('password', 8, __('パスワードは8文字から32文字までの間で入力してください。'))
            ->maxLength('password', 32, __('パスワードは8文字から32文字までの間で入力してください。'))
            ->requirePresence('password', 'create', __('パスワードを入力してください。'))
            ->notEmpty('password', __('パスワードを入力してください。'), 'create');

        $validator
            ->sameAs('password_again', 'password', __('パスワードが一致しません。'))
            ->notEmpty('password_again', __('パスワードを再入力してください。'), $password_again);

        $validator
            ->requirePresence('name', true, __('管理者名を入力してください。'))
            ->notEmpty('name', __('管理者名を入力してください。'));

        $validator
            ->boolean('is_active', __('正しいレコード状態を選択してください。'))
            ->inList('is_active', [0, 1], __('正しいレコード状態を選択してください。'))
            ->allowEmpty('is_active', true);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }

    /**
     * Before save listener.
     *
     * @param \Cake\Event\Event $event The beforeSave event that was fired
     * @param \Cake\Datasource\EntityInterface $entity The entity that is going to be saved
     * @param \ArrayObject $options the options passed to the save method
     * @return void
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        // パスワード更新判断
        $password = $entity->get('password');
        if (!empty($password)) {
            // "パスワード"の暗号化
            $hasher = new DefaultPasswordHasher();
            $entity->set(['password' => $hasher->hash($password)]);
        } else {
            // "パスワード"が空の場合は変更されていないことにする
            $entity->dirty('password', false);
        }
    }

    /**
     * Finds for ignore password.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findIgnorePassword(Query $query, array $options)
    {
        return $query
            ->select([
                'id' => 'Administrators.id',
                'username' => 'Administrators.username',
                'name' => 'Administrators.name'
            ]);
    }

    /**
     * Finds for authentication record.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findAuthentication(Query $query, array $options)
    {
        return $query
            ->find('active')
            ->select([
                'id' => 'Administrators.id',
                'username' => 'Administrators.username',
                'password' => 'Administrators.password',
                'name' => 'Administrators.name'
            ]);
    }
}
