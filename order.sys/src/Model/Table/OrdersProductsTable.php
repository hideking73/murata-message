<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdersProducts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Orders
 * @property \Cake\ORM\Association\BelongsTo $Products
 *
 * @method \App\Model\Entity\OrdersProduct get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrdersProduct newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrdersProduct[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrdersProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrdersProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersProduct findOrCreate($search, callable $callback = null)
 */
class OrdersProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('orders_products');
        $this->primaryKey('id');
        $this->displayField('order_id');

        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('product_id', __('正しい商品情報を選択してください。'))
            ->requirePresence('product_id', true, __('商品情報を選択してください。'))
            ->notEmpty('product_id', __('商品情報を選択してください。'));

        $validator
            ->integer('quantity', __('正しい数量を入力してください。'))
            ->range('quantity', [1, 999], __('数量は最小1、最大999になります。'))
            ->requirePresence('quantity', true, __('数量を入力してください。'))
            ->notEmpty('quantity', __('数量を入力してください。'));

        $validator
            ->integer('price', __('正しい単価を入力してください。'))
            ->allowEmpty('price', true);

        $validator
            ->boolean('is_discount', __('正しい割引対象状態を選択してください。'))
            ->inList('is_discount', [0, 1], __('正しい割引対象状態を選択してください。'))
            ->allowEmpty('is_discount', true);

        $validator
            ->allowEmpty('data', true);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
}
