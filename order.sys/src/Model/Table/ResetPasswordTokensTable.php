<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ResetPasswordTokens Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Customers
 *
 * @method \App\Model\Entity\ResetPasswordToken get($primaryKey, $options = [])
 * @method \App\Model\Entity\ResetPasswordToken newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ResetPasswordToken[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ResetPasswordToken|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ResetPasswordToken patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ResetPasswordToken[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ResetPasswordToken findOrCreate($search, callable $callback = null)
 */
class ResetPasswordTokensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reset_password_tokens');
        $this->primaryKey('id');
        $this->displayField('customer_id');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('token', true, __('トークンを入力してください。'))
            ->notEmpty('token', __('トークンを入力してください。'));

        $validator
            ->dateTime('expires', __('有効期限が不正です。'))
            ->requirePresence('expires', true, __('有効期限を入力してください。'))
            ->notEmpty('expires', __('有効期限を入力してください。'));

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }
}
