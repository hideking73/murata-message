<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * PaymentTypes Model
 *
 * @property \Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\PaymentType get($primaryKey, $options = [])
 * @method \App\Model\Entity\PaymentType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PaymentType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PaymentType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentType findOrCreate($search, callable $callback = null)
 */
class PaymentTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('payment_types');
        $this->primaryKey('id');
        $this->displayField('payment_type');

        $this->hasMany('Orders', [
            'foreignKey' => 'payment_type_id'
        ]);

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('payment_type', true, __('決済方法を入力してください。'))
            ->notEmpty('payment_type', __('決済方法を入力してください。'));

        return $validator;
    }

    /**
     * Finds for offering record list.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findListOffering(Query $query, array $options)
    {
        $this->displayField('offering');
        return $query
            ->find('list');
    }

    /**
     * Finds for celebrate record list.
     *
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\Datasource\EntityInterface
     */
    public function findListCelebrate(Query $query, array $options)
    {
        $this->displayField('celebrate');
        return $query
            ->find('list');
    }
}
