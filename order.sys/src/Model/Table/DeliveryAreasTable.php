<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliveryAreas Model
 *
 * @method \App\Model\Entity\DeliveryArea get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeliveryArea newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DeliveryArea[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryArea|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliveryArea patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryArea[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryArea findOrCreate($search, callable $callback = null)
 */
class DeliveryAreasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('delivery_areas');
        $this->primaryKey('id');
        $this->displayField('postal_code');

        $this->_validatorClass = '\App\Model\Validation\CustomValidator';
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('postal_code', true, __('郵便番号を入力してください。'))
            ->notEmpty('postal_code', __('郵便番号を入力してください。'));

        return $validator;
    }
}
