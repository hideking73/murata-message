<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;
use Cake\Routing\Router;
use Cake\Network\Exception\BadRequestException;

/**
 * Order Mailer
 */
class OrderMailer extends Mailer
{

    /**
     * Order Billing Mail
     *
     * @param \App\Model\Entity\Order $order
     * @param \App\Model\Entity\MessageType $message_type
     * @param \App\Model\Entity\PaymentType $payment_type
     */
    public function orderBilling($order, $message_type, $payment_type)
    {
        // メール設定
        $this
            ->emailPattern('/^[a-z0-9\.\+_-]{3,100}@(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,4})$/i')
            ->profile('order_billing')
            ->emailFormat('text')
            ->to([$order->get('email') => $order->get('billing') . '様'])
            ->viewVars(compact('order', 'message_type', 'payment_type'));
    }

    /**
     * Order Admin Mail
     *
     * @param \App\Model\Table\OrdersTable $Orders
     * @param \App\Model\Entity\MessageType $message_type
     * @param \App\Model\Entity\PaymentType $payment_type
     */
    public function orderAdmin($order, $message_type, $payment_type)
    {
        // メール設定
        $this
            ->emailPattern('/^[a-z0-9\.\+_-]{3,100}@(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,4})$/i')
            ->profile('order_admin')
            ->emailFormat('text')
            ->viewVars(compact('order', 'message_type', 'payment_type'));
    }
}
