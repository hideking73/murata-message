<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;
use Cake\Routing\Router;
use Cake\Network\Exception\BadRequestException;

/**
 * Customer Mailer
 */
class CustomerMailer extends Mailer
{

    /**
     * Customer Signup Mail
     *
     * @param \App\Model\Entity\Customer $customer
     */
    public function signup($customer)
    {
        // アクティベートURL作成
        $url = Router::url([
            'prefix' => false,
            'controller' => 'Member',
            'action' => 'activation',
            'token' => $customer->customer_signup_token->token
        ], true);

        // メール設定
        $this
            ->emailPattern('/^[a-z0-9\.\+_-]{3,100}@(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,4})$/i')
            ->profile('signup')
            ->emailFormat('text')
            ->to([$customer->get('email') => $customer->get('billing') . '様'])
            ->viewVars(compact('customer', 'url'));
    }

    /**
     * Customer Edit Mail
     *
     * @param \App\Model\Table\CustomersTable $Customers
     * @param \App\Model\Table\CustomerEditTokensTable $CustomerEditTokens
     */
    public function edit($customer, $customer_edit_token)
    {
        // アクティベートURL作成
        $url = Router::url([
            'prefix' => false,
            'controller' => 'Member',
            'action' => 'apply',
            'token' => $customer_edit_token->token
        ], true);

        // メール設定
        $this
            ->emailPattern('/^[a-z0-9\.\+_-]{3,100}@(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,4})$/i')
            ->profile('edit')
            ->emailFormat('text')
            ->to([$customer->get('email') => $customer->get('billing') . '様'])
            ->viewVars(compact('customer', 'customer_edit_token', 'url'));
    }

    /**
     * Forgot Mail
     *
     * @param \App\Model\Table\CustomersTable $Customers
     * @param \App\Model\Table\ResetPasswordTokensTable $ResetPasswordTokens
     */
    public function forgot($customer, $reset_password_token)
    {
        // アクティベートURL作成
        $url = Router::url([
            'prefix' => false,
            'controller' => 'Member',
            'action' => 'reset_password',
            'token' => $reset_password_token->token
        ], true);

        // メール設定
        $this
            ->emailPattern('/^[a-z0-9\.\+_-]{3,100}@(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,4})$/i')
            ->profile('forgot')
            ->emailFormat('text')
            ->to([$customer->get('email') => $customer->get('billing') . '様'])
            ->viewVars(compact('customer', 'reset_password_token', 'url'));
    }
}
