<?php
/**
 * Layout::offering
 */

?><!DOCTYPE html>
<html lang="ja">
<head>
    <?= $this->Html->charset() ?>
    <title><?= $this->fetch('title') ?></title>
    <?= $this->fetch('icon') ?>

    <?= $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css') ?>
    <?= $this->Html->css('offering/screen.css') ?>
    <?= $this->Html->css('offering/drift-basic.css') ?>
    <?= $this->Html->css('offering/fix.css') ?>
    <?= $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') ?>
    <?= $this->Html->script('//cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div class="wrapper">
        <header class="header">
            <div id="header" class="clearfix">
                <div id="logo">
                    <h1 title="愛媛・松山 村田葬儀社"><?= $this->Html->image('/img/offering/common/header_logo.png', [
                        'width' => '360',
                        'height' => '43',
                        'alt' => '愛媛・松山 村田葬儀社',
                        'title' => '愛媛・松山 村田葬儀社',
                        'url' => ['prefix' => 'offering', 'controller' => 'Home', 'action' => 'index']
                    ]) ?></a></h1>
                </div>
                <div id="link">
                    <div class="tel"><?= $this->Html->image('/img/offering/common/header_tel.gif', [
                        'width' => '199',
                        'height' => '16',
                        'alt' => 'TEL089-941-4444',
                        'title' => 'TEL089-941-4444'
                    ]) ?></div>
                    <div><a class="contact" href="//v8.rentalserver.jp/murata-group.co.jp/inquiry/">WEBからのお問い合わせ</a></div>
                    <ul>
                        <!-- <li><a href="//www.murata-group.co.jp/access/index.html" target="_blank">アクセスマップ</a></li> -->
                        <!-- <li><a href="//www.murata-group.co.jp/privacy.html" target="_blank">プライバシーポリシー</a></li> -->
                        <li><a href="//www.murata-group.co.jp/">村田葬儀社サイトへ戻る</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <?= $this->fetch('main-image') ?>
        <main class="contents">
            <p class="title-level1">供花・供物・弔意メッセージの注文</p>
            <div class="member">
                <ul class="member_link">
                <?php if (!$this->request->session()->read('Auth.Customer.id')): ?>
                    <li><?= $this->Html->link('<i class="fa fa-user" aria-hidden="true"></i>' . __('新規会員登録'), [
                        'prefix' => false,
                        'controller' => 'Member',
                        'action' => 'signup'
                    ], ['class' => 'register', 'escape' => false]) ?></li>
                    <li><?= $this->Html->link('<i class="fa fa-lock" aria-hidden="true"></i>' . __('ログイン'), [
                        'prefix' => false,
                        'controller' => 'Member',
                        'action' => 'login'
                    ], ['class' => 'login', 'escape' => false]) ?></li>
                <?php else: ?>
                    <li><?= $this->Html->link('<i class="fa fa-user" aria-hidden="true"></i>' . __('会員情報変更・退会'), [
                        'prefix' => false,
                        'controller' => 'Member',
                        'action' => 'index'
                    ], ['class' => 'register', 'escape' => false]) ?></li>
                    <li><?= $this->Html->link('<i class="fa fa-unlock" aria-hidden="true"></i>' . __('ログアウト'), [
                        'prefix' => false,
                        'controller' => 'Member',
                        'action' => 'logout'
                    ], ['class' => 'login', 'escape' => false]) ?></li>
                <?php endif; ?>
                </ul>
            </div>
            <?= $this->fetch('content') ?>
        </main>
        <footer class="footer">
            <p>Copyright(C) 村田葬儀社. All Rights Reserved.</p>
        </footer>
    </div>
</body>
</html>
