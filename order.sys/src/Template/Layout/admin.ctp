<?php
/**
 * Layout::admin
 */

?><!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <title><?= $this->fetch('title') ?></title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('admin.css') ?>
    <?= $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <?php if ($this->request->session()->read('Auth.Administrator.id')): ?>
    <h1><?= $this->Html->link(__('弔意メッセージ受注管理'), ['prefix' => 'admin', 'controller' => 'Dashboard', 'action' => 'index']) ?></h1>
    <?php else: ?>
    <h1><?= $this->Html->link(__('弔意メッセージ受注管理'), ['prefix' => 'admin', 'controller' => 'Dashboard', 'action' => 'login']) ?></h1>
    <?php endif; ?>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-2 medium-3 columns">
            <li class="name">
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <?php if ($this->request->session()->read('Auth.Administrator.id')): ?>
                <li><?= $this->Html->link(__('ダッシュボード'), ['prefix' => 'admin', 'controller' => 'Dashboard', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('会員情報'), ['prefix' => 'admin', 'controller' => 'Customers', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('受注情報'), ['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('管理者'), ['prefix' => 'admin', 'controller' => 'Administrators', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('ログアウト'), ['prefix' => 'admin', 'controller' => 'Dashboard', 'action' => 'logout']) ?></li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
