<?php
use Cake\Core\Configure;
use Cake\I18n\Number;
?>
----------------------------------------
村田葬儀社 受注完了通知
----------------------------------------
受注日: <?= $order->get('created')->format('Y年m月d日 H時i分') . PHP_EOL ?>
----------------------------------------

<?= $order->get('billing') ?>さんから注文がありました。

----------------------------------------
選択商品
----------------------------------------
<?php foreach ($order->get('products') as $product): ?>
商品名: <?= h($product->get('label')) . PHP_EOL ?>
数量: <?= Number::format($product->get('_joinData')->get('quantity')) . PHP_EOL ?>
小計: <?= Number::format($product->get('_joinData')->get('tax_in_subtotal')) . PHP_EOL ?>
----------------------------------------
<?php endforeach; ?>
<?php if ($order->has('message_type_id')): ?>
弔意メッセージ: <?= h($message_type->get('label')) . PHP_EOL ?>
----------------------------------------
<?php elseif ($order->get('delivery_type_id') == Configure::read('DeliveryType.murata')): ?>
弔意メッセージ: なし (<?= $this->Number->currency(0, 'JPY') ?>)
----------------------------------------
<?php endif; ?>
商品小計: <?= Number::currency($order->get('tax_in_subtotal'), 'JPY') . PHP_EOL ?>
<?php if (0 < $order->get('discount_rate') && !empty($order->products)): ?>
割引(<?= h($order->get('discount_rate')) ?>％): <?= Number::currency($order->get('tax_in_discount'), 'JPY') . PHP_EOL ?>
<?php endif; ?>
<?php if (0 < $order->get('carriage')): ?>
送料: <?= Number::currency($order->get('tax_in_carriage'), 'JPY') . PHP_EOL ?>
<?php endif; ?>
----------------------------------------
合計: <?= Number::currency($order->get('tax_in_total'), 'JPY') . PHP_EOL ?>
----------------------------------------

----------------------------------------
お届け先・名札名
----------------------------------------
お届け先: <?= h($order->get('delivery')) . PHP_EOL ?>
<?= $order->has('delivery_postal_code') ? '〒' . h($order->get('delivery_postal_code')) . PHP_EOL : null ?>
<?= h($order->get('delivery_to')) . PHP_EOL ?>
名札名: <?= h($order->get('name_tag_nl')) . PHP_EOL ?>
----------------------------------------

<?php if ($order->has('message_type_id')): ?>
----------------------------------------
メッセージの内容
----------------------------------------
<?= h($order->get('message')) . PHP_EOL ?>
----------------------------------------
<?php endif; ?>

----------------------------------------
ご請求先
----------------------------------------
会社名(個人名): <?= h($order->get('billing')) . PHP_EOL ?>
<?php if (!empty($order->get('position'))): ?>
部署名: <?= h($order->get('position')) . PHP_EOL ?>
<?php endif; ?>
<?php if (!empty($order->get('representative'))): ?>
担当者名: <?= h($order->get('representative')) . PHP_EOL ?>
<?php endif; ?>
送付住所: 〒<?= h($order->get('postal_code')) . PHP_EOL ?>
<?= h($order->get('address_full')) . PHP_EOL ?>
メールアドレス: <?= h($order->get('email')) . PHP_EOL ?>
TEL: <?= h($order->get('tel')) . PHP_EOL ?>
<?php if (!empty($order->get('fax'))): ?>
FAX: <?= h($order->get('fax')) . PHP_EOL ?>
<?php endif; ?>
----------------------------------------

----------------------------------------
決済方法
----------------------------------------
<?= h($payment_type->get('offering')) . PHP_EOL ?>
----------------------------------------

このメールに直接返信することはできませんのでご注意ください。

<?= $content ?>
