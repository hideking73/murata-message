<?php
use Cake\Core\Configure;
use Cake\I18n\Number;

$data = unserialize($customer_edit_token->data);
?>
----------------------------------------
村田葬儀社 会員情報変更
----------------------------------------

こんにちは、<?= h($customer->billing) ?>さん

以下のURLをクリックし、メールアドレスの確認を完了させて下さい！
<?= $url ?>

有効期限: <?= $customer_edit_token->expires->format('Y年m月d日 H時i分') ?>

変更後のアドレスは <?= h($data->email) ?> になります。
上記の確認作業が完了しないと、変更したデータが適用されません。

有効期限を過ぎますと、変更したデータは削除されますのでご注意ください。

このメールに直接返信することはできませんのでご注意ください。

<?= $content ?>
