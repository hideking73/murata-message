<?php
/**
 * MemberController::forgot
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo 'パスワードの再発行｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($forgot, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('パスワードの再発行 メールの送信完了') ?></h1>
        <p class="lead"><?= __('パスワード再発行メールの送信が完了しました。') ?><br>
            <?= __('ご登録メールアドレスにパスワードを再発行するためのメールを送信いたしました。') ?><br>
            <?= __('メールの内容をご確認いただきますよう、お願いいたします。') ?><br>
            <?= __('※メールが届かない場合はメールアドレスをご確認の上、再度お試しください。') ?></p>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Html->link(__('供花・供物・弔意メッセージの注文ページヘ'), [
                    'prefix' => 'offering',
                    'controller' => 'Home',
                    'action' => 'index'
                ]) ?></li>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
