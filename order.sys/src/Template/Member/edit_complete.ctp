<?php
/**
 * MemberController::edit_complete
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '会員情報変更｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($customer, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('会員情報変更') ?></h1>
        <div class="flowline memberflow">
            <ul>
                <li><span class="flow_number"></span><br><?= __('会員情報変更') ?></li>
                <li><span class="flow_number"></span><br><?= __('変更内容確認') ?></li>
                <li class="active"><span class="flow_number"></span><br><?= __('変更確認メール') ?></li>
                <li><span class="flow_number"></span><br><?= __('変更完了') ?></li>
            </ul>
        </div>
        <h2 class="title-level3"><?= __('会員情報変更(完了)') ?></h2>
        <p><?= __('現在仮変更の状態です。ご入力いただいたメールアドレス宛てに、ご連絡が届いておりますので、会員情報変更になった上でお買い物をお楽しみください。') ?></p>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Html->link(__('戻る'), [
                    'prefix' => false,
                    'controller' => 'Member',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
