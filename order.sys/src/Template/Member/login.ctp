<?php
/**
 * MemberController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo 'ログイン｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div>
    <?= $this->Form->create(null, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('会員情報') ?></h1>
        <h2 class="title-level3"><?= __('ログイン') ?></h2>
        <div class="block-2-none">
            <div class="box card">
                <div id="customer_box__body_inner" class="column_inner clearfix">
                    <div class="form-group">
                        <?= $this->Form->input('email', [
                            'label' => false,
                            'type' => 'email',
                            'placeholder' => __('メールアドレス'),
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input('password', [
                            'label' => false,
                            'placeholder' => __('パスワード'),
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                    <?= $this->Flash->render('flash', [
                        'element' => 'Flash/offering_login_error'
                    ]) ?>
                    <div class="btn_area center">
                        <p><?= $this->Form->button(__('ログイン'), [
                            'class' => 'btn'
                        ]) ?></p>
                        <ul class="sub-link">
                            <li><?= $this->Html->link(__('ログイン情報をお忘れですか？'), [
                                'prefix' => false,
                                'controller' => 'Member',
                                'action' => 'forgot'
                            ]) ?></li>
                            <li><?= $this->Html->link(__('新規会員登録'), [
                                'prefix' => false,
                                'controller' => 'Member',
                                'action' => 'signup'
                            ]) ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Form->end() ?>
</div>
<ul class="btn_area center">
    <li><?= $this->Html->link(__('供花・供物・弔意メッセージの注文 トップページに戻る'), [
    'prefix' => 'offering',
    'controller' => 'Home',
    'action' => 'index'
	], [
	    'class' => 'btn continue back-btn'
	]) ?></li>
 </ul>
