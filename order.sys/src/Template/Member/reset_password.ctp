<?php
/**
 * MemberController::edit
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo 'パスワード再発行｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($customer, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('パスワード再発行') ?></h1>
        <h2 class="title-level3"><?= __('パスワード再発行') ?></h2>
        <p><?= __('パスワードの再発行を行います。') ?></p>
        <?= $this->Flash->render('flash', [
            'element' => 'Flash/offering_login_error'
        ]) ?>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('パスワード') ?></label></dt>
            <dd class="form-group">
                <?= $this->Form->input('password', [
                    'label' => false,
                    'placeholder' => '半角英数字記号8～32文字 (変更する場合のみ)',
                    'class' => 'form-control'
                ]) ?>
                <?= $this->Form->input('password_again', [
                    'label' => false,
                    'type' => 'password',
                    'placeholder' => '確認のためもう一度入力してください',
                    'class' => 'form-control'
                ]) ?>
            </dd>
        </dl>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Form->button(__('再発行'), [
                    'class' => 'btn'
                ]) ?></li>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
<?debug($customer)?>
