<?php
/**
 * MemberController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '会員情報｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<script>
(function ($) {
    // 退会確認ダイアログ
    var leaveConfirm = function () {
        if (window.confirm("<?= __('本当に退会しますか？退会すると登録情報や履歴はすべて消去されます。') ?>")) {
            location.href = "<?= $this->Url->build(['action' => 'leave'], true) ?>";
        } else {
            window.alert("<?= __('キャンセルされました。') ?>");
        }
    }
    // 描画後
    $(function () {
        $('#leave').on('click', function (e) {
            leaveConfirm();
        });
    });
})(jQuery);
</script>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create(null, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('会員情報') ?></h1>
        <?= $this->Flash->render('flash', [
            'element' => 'Flash/offering_login_error'
        ]) ?>
        <div class="block-2">
            <?= $this->Html->link(__('会員情報の編集'), [
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'edit'
            ], [
                'class' => 'box card'
            ]) ?>
            <?= $this->Html->link(__('注文履歴'), [
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'history'
            ], [
                'class' => 'box card'
            ]) ?>
        </div>
        <div class="block-2 text-right">
            退会を希望の方は、<button type="button" id="leave">退会</button>をクリックください。
        </div>
    <?= $this->Form->end() ?>
    <p>注文内容の修正・キャンセルは開式の3時間前まで承ります。<br>ご注文内容の修正・キャンセルのご希望がございましたら<br>ムラタ葬儀社（089-941-4444）までお電話にてご連絡いただきますようお願い申し上げます。</p>
    <ul class="btn_area center">
        <li><?= $this->Html->link(__('供花・供物・弔意メッセージの注文 トップページに戻る'), [
        'prefix' => 'offering',
        'controller' => 'Home',
        'action' => 'index'
		], [
		    'class' => 'btn continue'
		]) ?></li>
     </ul>
</div>
