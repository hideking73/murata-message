<?php
/**
 * MemberController::forgot
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo 'パスワードの再発行｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($forgot, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('パスワードの再発行') ?></h1>
        <p><?= __('ご登録時のメールアドレスを入力して「次へ」ボタンをクリックしてください。') ?><br>
            <?= __('※新しくパスワードを発行いたしますので、お忘れになったパスワードはご利用できなくなります。') ?></p>
        <?= $this->Flash->render('flash', [
            'element' => 'Flash/offering_login_error'
        ]) ?>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('メールアドレス') ?></label><span class="required"><?= __('必須') ?></span></dt>
            <dd class="form-group">
                <?= $this->Form->input('email', [
                    'label' => false,
                    'type' => 'email',
                    'class' => 'form-control'
                ]) ?>
            </dd>
        </dl>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Form->button(__('次のページへ'), [
                    'class' => 'btn'
                ]) ?></li>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
