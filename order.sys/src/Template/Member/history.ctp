<?php
/**
 * MemberController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '会員情報｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<script>
(function ($) {
    // 退会確認ダイアログ
    var leaveConfirm = function () {
        if (window.confirm("<?= __('本当に退会しますか？退会すると登録情報や履歴はすべて消去されます。') ?>")) {
            location.href = "<?= $this->Url->build(['action' => 'leave'], true) ?>";
        } else {
            window.alert("<?= __('キャンセルされました。') ?>");
        }
    }
    // 描画後
    $(function () {
        $('#leave').on('click', function (e) {
            leaveConfirm();
        });
    });
})(jQuery);
</script>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create(null, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('会員情報') ?></h1>

        <table class="order-list">
            <thead>
                <tr>
                    <th>注文日</th>
                    <th>送り先</th>
                    <th>商品名【金額】数量</th>
                    <th>請求金額</th>
                </tr>
            </thead>
            <tbody>
                <?php if (0 < count($orders)): ?>
                <?php foreach ($orders as $order): ?>
                <tr>
                    <td><?= h($order->get('created')->format('Y-m-d')) ?></td>
                    <td>
                        <p><?= h($order->get('delivery')) ?></p>
                        <?php if ($order->get('delivery_type_id') == Configure::read('DeliveryType.other')): ?>
                        <p><?= h($order->get('delivery_postal_code')) ?></p>
                        <?php endif; ?>
                        <p><?= h($order->get('delivery_to')) ?></p>
                    </td>
                    <td>
                        <?php foreach ($order->get('products') as $product): ?>
                        <dl class="item_detail">
                            <dt class="item_name text-default">
                                <?= __('{0}【{1}】', h($product->get('name')), $this->Number->currency($product->get('_joinData')->get('tax_in_price'), 'JPY')) ?>
                            </dt>
                            <dd class="item_quantity">
                                <?= $this->Number->format($product->get('_joinData')->get('quantity')) ?>
                            </dd>
                        </dl>
                        <?php endforeach; ?>
                    </td>
                    <td class="item_subtotal">
                        <?= $this->Number->currency($order->get('tax_in_total'), 'JPY') ?>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td colspan="4"><?= __('データがありません') ?></td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <div class="text-center">
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo;', ['escape' => false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('&raquo;', ['escape' => false]) ?>
            </ul>
        </div>
         <ul class="btn_area center">
            <li><?= $this->Html->link(__('戻る'), [
                'prefix' => false,
                'controller' => 'Member',
                'action' => 'index'
            ], [
                'class' => 'btn continue'
            ]) ?></li>
        </ul>
   <?= $this->Form->end() ?>
    <p>注文内容の修正・キャンセルは開式の3時間前まで承ります。<br>ご注文内容の修正・キャンセルのご希望がございましたら<br>ムラタ葬儀社（089-941-4444）までお電話にてご連絡いただきますようお願い申し上げます。</p>
</div>
