<?php
/**
 * MemberController::edit_confirm
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '会員情報変更｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($customer, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('会員情報変更') ?></h1>
        <div class="flowline memberflow">
            <ul>
                <li><span class="flow_number"></span><br><?= __('会員情報変更') ?></li>
                <li class="active"><span class="flow_number"></span><br><?= __('変更内容確認') ?></li>
                <li><span class="flow_number"></span><br><?= __('変更確認メール') ?></li>
                <li><span class="flow_number"></span><br><?= __('変更完了') ?></li>
            </ul>
        </div>
        <h2 class="title-level3"><?= __('変更内容確認') ?></h2>
        <p><?= __('下記の内容で送信してもよろしいでしょうか？ よろしければ、一番下の「上記内容で変更する」ボタンをクリックしてください。') ?></p>
        <div class="block-2">
            <div class="box">
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('会社名(個人名)') ?></label></dt>
                    <dd class="form-group">
                        <?= h($customer->get('billing')) ?>
                        <?= $this->Form->hidden('billing') ?>
                        <?= $customer->get('is_billing_external') ? __('(外字あり)') : null ?>
                        <?= $this->Form->hidden('is_billing_external') ?>
                    </dd>
                </dl>
                <?php if ($customer->get('position')): ?>
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('部署名') ?></label></dt>
                    <dd class="form-group">
                        <?= h($customer->get('position')) ?>
                        <?= $this->Form->hidden('position') ?>
                    </dd>
                </dl>
                <?php endif; ?>
                <?php if ($customer->get('representative')): ?>
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('担当者名') ?></label></dt>
                    <dd class="form-group">
                        <?= h($customer->get('representative')) ?>
                        <?= $this->Form->hidden('representative') ?>
                    </dd>
                </dl>
                <?php endif; ?>
                <dl class="dl_table">
                    <dt><label class="control-label required">送付住所</label></dt>
                    <dd>
                        <div class="form-group">
                            <div class="form-group form-inline">
                                〒<?= h($customer->get('postal_code')) ?>
                                <?= $this->Form->hidden('postal_code') ?>
                            </div>
                            <div class="form-group">
                                <?= h($customer->get('address_full')) ?>
                                <?= $this->Form->hidden('prefecture_id') ?>
                                <?= $this->Form->hidden('address') ?>
                                <?= $this->Form->hidden('address_etc') ?>
                            </div>
                        </div>
                    </dd>
                </dl>
            </div>
            <div class="box">
                <dl class="dl_table">
                    <dt><label class="control-label">メールアドレス</label></dt>
                    <dd class="form-group">
                        <?= h($customer->get('email')) ?>
                        <?= $this->Form->hidden('email') ?>
                        <?= $this->Form->hidden('email_again') ?>
                    </dd>
                </dl>
                <?php if ($customer->get('password')): ?>
                <dl class="dl_table">
                    <dt><label class="control-label">パスワード</label></dt>
                    <dd class="form-group">
                        <?= str_repeat('*', mb_strlen($customer->get('password'))) ?>
                        <?= $this->Form->hidden('password') ?>
                        <?= $this->Form->hidden('password_again') ?>
                    </dd>
                </dl>
                <?php endif; ?>
                <dl class="dl_table">
                    <dt><label class="control-label">TEL</label></dt>
                    <dd class="form-group">
                        <?= h($customer->get('tel')) ?>
                        <?= $this->Form->hidden('tel') ?>
                    </dd>
                </dl>
                <?php if ($customer->get('fax')): ?>
                <dl class="dl_table">
                    <dt><label class="control-label">FAX</label></dt>
                    <dd class="form-group">
                        <?= h($customer->get('fax')) ?>
                        <?= $this->Form->hidden('fax') ?>
                    </dd>
                </dl>
                <?php endif; ?>
            </div>
        </div>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Form->button(__('上記内容で変更する'), [
                    'name' => 'complete',
                    'class' => 'btn'
                ]) ?></li>
                <li><?= $this->Form->button(__('戻る'), [
                    'name' => 'signup',
                    'class' => 'btn continue'
                ]) ?></li>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
<?debug($customer)?>
