<?php
/**
 * BillingController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '注文内容｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?= $this->Html->script('//ajaxzip3.github.io/ajaxzip3.js', ['charset' => 'UTF-8']) ?>
<script>
(function ($) {
    // 描画後
    $(function () {
        // 請求先の住所検索
        $('[name="postal_code"]').on('blur', function (e) {
            // 郵便番号調整
            var postal_code = $(this).val().replace(/[^0-9]/g, '').replace(/(\d{3})(\d{4})/g, '$1-$2');
            $(this).val(postal_code);
            // 住所検索
            AjaxZip3.zip2addr('postal_code', '', 'prefecture_id', 'address');
            $('[name="address_etc"]').val('');
        });
        // 電話番号のフォーマット
        $('[name="tel"], [name="fax"]').on('blur', function (e) {
            $.ajax({
                url: '<?= $this->Url->build(["prefix" => "api", "controller" => "Format", "action" => "get_phone", "_ext" => "json"], true) ?>',
                type: 'get',
                data: { 'number': $(this).val() },
                dataType: 'json'
            }).done(function (data) {
                if (data.result.status) {
                    $.each(data.result.data, function (field, val) {
                        if (field == 'number') {
                            $(e.target).val(val);
                        }
                    });
                }
            });
        });
    });
})(jQuery);
</script>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($order, ['novalidate' => true]) ?>
        <?php if (!$order->has('step_payment_end')): ?>
        <h1 class="title-level2"><?= __('注文内容') ?></h1>
        <div class="flowline">
            <ul>
                <li><span class="flow_number"></span><br><?= __('お届け先') ?><br><?= __('名札名入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('カートの商品') ?></li>
                <li><span class="flow_number"></span><br><?= __('メッセージ入力') ?></li>
                <li class="active"><span class="flow_number"></span><br><?= __('ご請求先入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('決済方法') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご注文内容確認') ?></li>
                <li><span class="flow_number"></span><br><?= __('完了') ?></li>
            </ul>
        </div>
        <?php else: ?>
        <h1 class="title-level2"><?= __('注文内容 : 変更(ご請求先)') ?></h1>
        <?php endif; ?>
        <h2 class="title-level3"><?= __('ご請求先入力') ?></h2>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('会社名(個人名)') ?></label><span class="required"><?= __('必須') ?></span></dt>
            <dd class="form-group">
                <?= $this->Form->input('billing', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
                <?= $this->Form->input('is_billing_external', [
                    'label' => __('お名前に外字がある場合チェックしてください'),
                    'type' => 'checkbox'
                ]) ?>
            </dd>
        </dl>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('部署名') ?></label></dt>
            <dd class="form-group">
                <?= $this->Form->input('position', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </dd>
        </dl>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('担当者名') ?></label></dt>
            <dd class="form-group">
                <?= $this->Form->input('representative', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </dd>
        </dl>
        <dl class="dl_table">
            <dt><label class="control-label required"><?= __('送付住所') ?></label><span class="required"><?= __('必須') ?></span></dt>
            <dd>
                <div class="form-group">
                    <div class="form-group form-inline">
                        〒<?= $this->Form->input('postal_code', [
                            'label' => false,
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input('prefecture_id', [
                            'label' => false,
                            'options' => $prefectures,
                            'empty' => '都道府県を選択する',
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input('address', [
                            'label' => false,
                            'placeholder' => __('市区町村名 (例：松山市湊町)'),
                            'class' => 'form-control',
                            'style' => 'ime-mode:active;'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input('address_etc', [
                            'label' => false,
                            'placeholder' => __('番地・ビル名 (例：6丁目4-5)'),
                            'class' => 'form-control',
                            'style' => 'ime-mode:active;'
                        ]) ?>
                    </div>
                </div>
            </dd>
        </dl>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('メールアドレス') ?></label><span class="required"><?= __('必須') ?></span></dt>
            <dd class="form-group">
                <?= $this->Form->input('email', [
                    'label' => false,
                    'type' => 'email',
                    'class' => 'form-control'
                ]) ?>
            </dd>
        </dl>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('TEL') ?></label><span class="required"><?= __('必須') ?></span></dt>
            <dd class="form-group">
                <?= $this->Form->input('tel', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </dd>
        </dl>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('FAX') ?></label></dt>
            <dd class="form-group">
                <?= $this->Form->input('fax', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </dd>
        </dl>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Form->button(__($next_button), [
                    'class' => 'btn'
                ]) ?></li>
                <?php if (!$order->has('step_payment_end')): ?>
                <li><?= $this->Html->link(__('戻る'), [
                    'prefix' => 'offering',
                    'controller' => empty($order->get('message_type_id')) ? 'Cart' : 'Message',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php else: ?>
                <li><?= $this->Html->link(__('キャンセル'), [
                    'prefix' => 'offering',
                    'controller' => 'Confirm',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php endif; ?>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
