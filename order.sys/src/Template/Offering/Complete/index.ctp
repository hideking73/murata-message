<?php
/**
 * CompleteController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '注文内容｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($order, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('注文内容') ?></h1>
        <div class="flowline">
            <ul>
                <li><span class="flow_number"></span><br><?= __('お届け先') ?><br><?= __('名札名入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('カートの商品') ?></li>
                <li><span class="flow_number"></span><br><?= __('メッセージ入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご請求先入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('決済方法') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご注文内容確認') ?></li>
                <li class="active"><span class="flow_number"></span><br><?= __('完了') ?></li>
            </ul>
        </div>
        <h2 class="title-level3"><?= __('注文完了') ?></h2>
        <p>ご注文ありがとうございました。早速、手配をさせていただきます。<br>
            また、メールを送信させていただきました。<br>
            メールには注文内容、支払い方法の詳細など明記しておりますのでご確認をお願いいたします。<br>
            メールが届かない場合は、お手数ですが以下までご連絡をお願いいたします。</p>
        <p><div class="tel"><?= $this->Html->image('/img/offering/common/header_tel.gif', [
            'width' => '199',
            'height' => '16',
            'alt' => 'TEL089-941-4444',
            'title' => 'TEL089-941-4444'
        ]) ?></div></p>
        <div class="total_box">
            <ul class="btn_area center">
                <?php if ($this->request->session()->check('Signup')): ?>
                <li>
                    <p><?= __('会員登録を行っていただけると次回注文時の入力が楽になります。<br>この機会に登録はいかがでしょうか？') ?></p>
                    <?= $this->Html->link(__('会員登録をする'), [
                        'prefix' => false,
                        'controller' => 'Member',
                        'action' => 'signup'
                    ], [
                        'class' => 'btn'
                    ]) ?>
                </li>
                <?php endif; ?>
                <li><?= $this->Html->link(__('供花・供物・弔意メッセージの注文 トップページに戻る'), [
                    'prefix' => 'offering',
                    'controller' => 'Home',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
