<?php
/**
 * CartController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '注文内容｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?= $this->Html->script('ordersys.number-format.js') ?>
<script>
(function ($) {
    // カート更新
    var updateOrderSession = function () {
        $.ajax({
            url: '<?= $this->Url->build(["prefix" => "api", "controller" => "Cart", "action" => "update_order_session", "_ext" => "json"], true) ?>',
            type: $('#cart_form').attr('method'),
            data: $('#cart_form').serialize(),
            dataType: 'json'
        }).done(function (data) {
            if (data.result.status) {
                $.each(data.result.data, function (field, val) {
                    if (field != 'products') {
                        $('.' + field).text(currency(val));
                    } else {
                        $.each(val, function (key, product) {
                            $('.product_' + key + '_tax_in_subtotal').text(currency(product.tax_in_subtotal));
                        });
                    }
                });
            }
        });
    };
    // 描画後
    $(function () {
        // 数量変更時
        $('[name$="[quantity]"]').on('change keyup', function (e) {
            if ($(this).val() == '') {
                $(this).val(1);
            } else if (parseInt($(this).val()) < 1) {
                $(this).val(1);
            } else if (999 < parseInt($(this).val())) {
                $(this).val(999);
            }
            updateOrderSession();
        });
        // メッセージタイプ変更時
        $('[name="message_type_id"]').on('change', function (e) {
            // カート変更
            updateOrderSession();
            // ボタン変更
            if ($(this).val() != '') {
                $('[type="submit"].no-confirm').text("<?= __('メッセージを入力する') ?>");
            } else {
                $('[type="submit"].no-confirm').text("<?= __('ご請求先の入力') ?>");
            }
        });
    });
})(jQuery);
</script>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($order, ['novalidate' => true, 'id' => 'cart_form']) ?>
        <?php if (!$order->has('step_payment_end')): ?>
        <h1 class="title-level2"><?= __('注文内容') ?></h1>
        <div class="flowline">
            <ul>
                <li><span class="flow_number"></span><br><?= __('お届け先') ?><br><?= __('名札名入力') ?></li>
                <li class="active"><span class="flow_number"></span><br><?= __('カートの商品') ?></li>
                <li><span class="flow_number"></span><br><?= __('メッセージ入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご請求先入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('決済方法') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご注文内容確認') ?></li>
                <li><span class="flow_number"></span><br><?= __('完了') ?></li>
            </ul>
        </div>
        <?php else: ?>
        <h1 class="title-level2"><?= __('注文内容 : 変更(商品選択)') ?></h1>
        <?php endif; ?>
        <p class="message"><?= __('商品の合計金額は「<strong class="tax_in_total">{0}</strong>」です。', $this->Number->currency($order->get('tax_in_total'), 'JPY')) ?></p>
        <table class="cart_item">
            <thead>
                <tr>
                    <th><?= __('削除') ?></th>
                    <th><?= __('商品内容') ?></th>
                    <th><?= __('数量') ?></th>
                    <th><?= __('小計') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($order->products)): ?>
                <?php foreach ($order->products as $key => $product): ?>
                <tr>
                    <td>
                        <?= $this->Html->link(__('<i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>'), ['action' => 'delete', $key], [
                            'escape' => false
                        ]) ?>
                    </td>
                    <td>
                        <div class="item_photo">
                            <?php if ($product->has('image_path')): ?>
                            <?= $this->Html->image($product->get('image_path'), [
                                'width' => '150',
                                'height' => '200',
                                'alt' => h($product->get('name')),
                                'title' => h($product->get('name')),
                                'url' => [
                                    'prefix' => 'offering',
                                    'controller' => 'Select',
                                    'action' => 'detail',
                                    $product->get('id')
                                ]
                            ]) ?>
                            <?php endif; ?>
                        </div>
                        <dl class="item_detail">
                            <dt class="item_name text-default">
                                <?= $this->Html->link(h($product->get('name')), [
                                    'prefix' => 'offering',
                                    'controller' => 'Select',
                                    'action' => 'detail',
                                    $product->get('id')
                                ]) ?>
                            </dt>
                            <dd class="item_price"><?= $this->Number->currency($product->get('_joinData')->get('tax_in_price'), 'JPY') ?></dd>
                        </dl>
                    </td>
                    <td class="item_quantity">
                        <?= $this->Form->input("products.{$key}._joinData.product_id", [
                            'type' => 'hidden'
                        ]) ?>
                        <?= $this->Form->input("products.{$key}._joinData.quantity", [
                            'label' => false,
                            'type' => 'number',
                            'default' => 1,
                            'class' => 'form-control'
                        ]) ?>
                    </td>
                    <td class="item_subtotal">
                        <span class="product_<?= $key ?>_tax_in_subtotal"><?= $this->Number->currency($product->get('_joinData')->get('tax_in_subtotal'), 'JPY') ?></span>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                    <td colspan="4"><?= __('カートは空です。') ?></td>
                <?php endif; ?>
            </tbody>
        </table>
        <?php if ($order->get('delivery_type_id') == Configure::read('DeliveryType.murata')): ?>
        <dl class="item_message">
            <dt class="item_name text-default">
                <h2>弔意メッセージ</h2>
                <p>チェックすることで、供花・供物と一緒に弔意メッセージを贈ることができます。
                    <br>メッセージは3つの例文もしくはオリジナルで作成することができます。</p>
                <div id="selectBox" class="form-group">
                    <?= $this->Form->input('message_type_id', [
                        'label' => false,
                        'type' => 'radio',
                        'options' => $message_types,
                        'empty' => 'なし',
                        'default' => '',
                    ]) ?>
                </div>
            </dt>
            <dd class="item_price">
                +<span class="tax_in_message_price"><?= $this->Number->currency($order->get('tax_in_message_price'), 'JPY') ?></span>
            </dd>
        </dl>
        <?php endif; ?>
        <div class="price-area">
            <div class="box">
                <dl class="item-all-price">
                    <dt>商品小計&nbsp;:</dt>
                    <dd class="text-primary">
                        <span class="tax_in_subtotal"><?= $this->Number->currency($order->get('tax_in_subtotal'), 'JPY') ?></span>
                    </dd>
                </dl>
            </div>
            <?php if (0 < $order->get('discount_rate') && !empty($order->products)): ?>
            <div class="box">
                <dl class="discount">
                    <dt>割引(<?= h($order->get('discount_rate')) ?>％)&nbsp;:</dt>
                    <dd class="text-primary">
                        -<span class="tax_in_discount"><?= $this->Number->currency($order->get('tax_in_discount'), 'JPY') ?></span>
                    </dd>
                </dl>
            </div>
            <?php endif; ?>
            <?php if (0 < $order->get('carriage')): ?>
            <div class="box">
                <dl class="shipping">
                    <dt>送料&nbsp;:</dt>
                    <dd class="text-primary">
                        <span class="tax_in_carriage"><?= $this->Number->currency($order->get('tax_in_carriage'), 'JPY') ?></span>
                    </dd>
                </dl>
            </div>
            <?php endif; ?>
            <div class="box">
                <dl class="total_price">
                    <dt>合計&nbsp;:</dt>
                    <dd class="text-primary">
                        <span class="tax_in_total"><?= $this->Number->currency($order->get('tax_in_total'), 'JPY') ?></span>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="total_box">
            <ul class="btn_area center">
                <?php if (!empty($order->products)): ?>
                <li><?= $this->Form->button(__($next_button), [
                    'class' => !$order->has('step_payment_end') ? 'btn no-confirm' : 'btn'
                ]) ?></li>
                <?php endif; ?>
                <li><?= $this->Html->link(__($back_button), [
                    'prefix' => 'offering',
                    'controller' => 'Select',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php if ($order->has('step_payment_end')): ?>
                <li><?= $this->Html->link(__('キャンセル'), [
                    'prefix' => 'offering',
                    'controller' => 'Confirm',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php endif; ?>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
