<?php
/**
 * SelectController::detail
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?= $this->Html->script('offering/jquery.matchHeight-min.js') ?>
<script>
(function ($) {
    // 描画後
    $(function () {
        $(".block-2 .box").matchHeight();
        // 数量変更時
        $('[name="quantity"]').on('change keyup', function (e) {
            if ($(this).val() == '') {
                $(this).val(1);
            } else if (parseInt($(this).val()) < 1) {
                $(this).val(1);
            } else if (999 < parseInt($(this).val())) {
                $(this).val(999);
            }
        });
    });
})(jQuery);
</script>
<?php $this->end() ?>

<div>
    <p>供花・供物のご注文は、以下の商品をクリックし画面の指示にしたがってご記入をお願いいたします。<br>
        弔意メッセージはカート内で選択することで注文ができます。</p>
    <div class="item block-2">
        <div class="box bound-box">
            <?php if ($product->has('image_path')): ?>
            <?= $this->Html->image($product->get('image_path'), [
                'width' => '420',
                'height' => '520',
                'alt' => h($product->get('name')),
                'title' => h($product->get('name'))
            ]) ?>
            <?php endif; ?>
        </div>
        <div class="box">
            <div class="detail">
                <h1 class="item-name"><?= h($product->get('name')) ?></h1>
                <p class="item-price">価格&nbsp;:&nbsp;1<?= h($product->get('unit')) ?>&nbsp;<span><?= $this->Number->currency($product->get('tax_in_price'), 'JPY') ?></span><small>(税込)</small></p>
                <p class="item-size"><?= h($product->get('spec')) ?></p>
                <p class="item-description">一対の場合は数量を2と入力ください。<br>
                    <?= nl2br(h($product->get('comment'))) ?></p>
                <?= $this->Form->create($order_product) ?>
                    <div class="cart_area">
                        <?= $this->Form->input('product_id', [
                            'type' => 'hidden',
                            'default' => h($product->get('id'))
                        ]) ?>
                        <dl class="quantity">
                            <dt class="quantity-name">数量</dt>
                            <dd>
                                <?= $this->Form->input('quantity', [
                                    'label' => false,
                                    'type' => 'number',
                                    'default' => 1,
                                    'class' => 'form-control'
                                ]) ?>
                            </dd>
                        </dl>
                        <div class="btn_area">
                            <?= $this->Form->button(__('カートに入れる'), [
                                'class' => 'btn'
                            ]) ?>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
