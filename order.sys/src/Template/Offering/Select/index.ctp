<?php
/**
 * SelectController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div>
    <p>供花・供物のご注文は、以下の商品をクリックし画面の指示にしたがってご記入をお願いいたします。<br>
        供花・供物と合わせて弔意メッセージも同時にお申し込みいただくことが可能となりました。弔意メッセージはカート内で選択することで注文ができます。</p>
    <div class="item-list">
        <?php foreach ($products as $product): ?>
        <div class="card">
            <a href="<?= $this->Url->build(['action' => 'detail', $product->get('id')], true) ?>">
                <div class="card-image">
                    <?php if ($product->has('image_path')): ?>
                    <?= $this->Html->image($product->get('image_path'), [
                        'width' => '204',
                        'height' => '252',
                        'alt' => h($product->get('name')),
                        'title' => h($product->get('name'))
                    ]) ?>
                    <?php endif; ?>
                </div>
                <dl class="card-content">
                    <dt class="title"><?= h($product->get('name')) ?></dt>
                    <dd class="price">価格&nbsp;:&nbsp;1<?= h($product->get('unit')) ?>&nbsp;<span><?= $this->Number->currency($product->get('tax_in_price'), 'JPY') ?></span></dd>
                </dl>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
</div>
