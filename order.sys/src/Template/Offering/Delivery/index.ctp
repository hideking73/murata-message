<?php
/**
 * DeliveryController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo 'お届け先・名札名入力｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?= $this->Html->script('//ajaxzip3.github.io/ajaxzip3.js', ['charset' => 'UTF-8']) ?>
<script>
(function ($) {
    // お届け先タイプ切り替え処理
    var changeDeliveryType = function (e) {
        $('[id^="section-delivery-type-"]').hide();
        if ($(e).filter(':checked').val() == '<?= \Cake\Core\Configure::read('DeliveryType.murata') ?>') {
            $('[id="section-delivery-type-murata"]').show();
            $('[id="section-delivery-type-murata"]').find('input,select').prop('disabled', false);
            $('[id="section-delivery-type-other"]').find('input,select').prop('disabled', true);
        } else if ($(e).filter(':checked').val() == '<?= \Cake\Core\Configure::read('DeliveryType.other') ?>') {
            $('[id="section-delivery-type-other"]').show();
            $('[id="section-delivery-type-murata"]').find('input,select').prop('disabled', true);
            $('[id="section-delivery-type-other"]').find('input,select').prop('disabled', false);
        }
    };
    // 描画後
    $(function () {
        // お届け先の住所検索
        $('[name="delivery_postal_code"]').on('blur', function (e) {
            // 郵便番号調整
            var postal_code = $(this).val().replace(/[^0-9]/g, '').replace(/(\d{3})(\d{4})/g, '$1-$2');
            $(this).val(postal_code);
            // 住所検索
            AjaxZip3.zip2addr('delivery_postal_code', '', 'delivery_prefecture_id', 'delivery_address');
            $('[name="delivery_address_etc"]').val('');
        });
        // お届け先タイプ切り替え
        changeDeliveryType($('[name="delivery_type_id"]'));
        $('[name="delivery_type_id"]').on('change', function (e) {
            changeDeliveryType(this);
        });
    });
})(jQuery);
</script>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($order, ['novalidate' => true]) ?>
        <?php if (!$order->has('step_payment_end')): ?>
        <h1 class="title-level2"><?= __('注文内容') ?></h1>
        <div class="flowline">
            <ul>
                <li class="active"><span class="flow_number"></span><br><?= __('お届け先') ?><br><?= __('名札名入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('カートの商品') ?></li>
                <li><span class="flow_number"></span><br><?= __('メッセージ入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご請求先入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('決済方法') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご注文内容確認') ?></li>
                <li><span class="flow_number"></span><br><?= __('完了') ?></li>
            </ul>
        </div>
        <?php else: ?>
        <h1 class="title-level2"><?= __('注文内容 : 変更(お届け先・名札名入力)') ?></h1>
        <?php endif; ?>
        <h2 class="title-level3"><?= __('お届け先・名札名入力') ?></h2>
        <h3><span class="title-level4"><?= __('お届け先') ?></span></h3>
        <dl class="dl_table">
            <dt><label class="control-label required"><?= __('故人名') ?></label><span class="required"><?= __('必須') ?></span></dt>
            <dd class="form-group">
                <?= $this->Form->input('delivery', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
                <p class="caution">お通夜にお供えする供花・供物の注文については当日１５時までとさせていただきます。</p>
            </dd>
        </dl>
        <dl class="dl_table">
            <dt><label class="control-label required"><?= __('送付住所') ?></label><span class="required"><?= __('必須') ?></span></dt>
            <dd>
                <div id="selectBox" class="form-group">
                    <?= $this->Form->input('delivery_type_id', [
                        'label' => false,
                        'type' => 'radio',
                        'options' => $delivery_types,
                        'default' => \Cake\Core\Configure::read('DeliveryType.murata'),
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div id="section-delivery-type-murata" class="form-group">
                    <div class="form-group">
                        <?= $this->Form->input('facility_id', [
                            'label' => false,
                            'type' => 'select',
                            'options' => $facilities,
                            'empty' => __('葬祭式場を選択'),
                            'default' => '',
                            'class' => 'form-control',

                        ]) ?>
                    </div>
                    <p><small><?= __('弊社葬祭式場でしたら、弔意メッセージをつけることができます。') ?></small></p>
                </div>
                <div id="section-delivery-type-other" class="form-group">
                    <div class="form-group form-inline">
                        〒<?= $this->Form->input('delivery_postal_code', [
                            'label' => false,
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input('delivery_prefecture_id', [
                            'label' => false,
                            'options' => $prefectures,
                            'empty' => '都道府県を選択する',
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input('delivery_address', [
                            'label' => false,
                            'placeholder' => __('市区町村名 (例：松山市湊町)'),
                            'class' => 'form-control',
                            'style' => 'ime-mode:active;'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input('delivery_address_etc', [
                            'label' => false,
                            'placeholder' => __('番地・ビル名 (例：6丁目4-5・葬儀社名)'),
                            'class' => 'form-control',
                            'style' => 'ime-mode:active;'
                        ]) ?>
                    </div>

                </div>
            </dd>
        </dl>
        <h3><span class="title-level4"><?= __('名札名入力(お名前)') ?></span></h3>
        <p><span class="required"><?= __('注') ?></span>&nbsp;:&nbsp;<?= __('会社名または肩書・差出人名(お名前)のどちらかの入力は必須です。') ?></p>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('会社名') ?></label><span class="required"><?= __('注') ?></span></dt>
            <dd class="form-group">
                <?= $this->Form->input('company', [
                    'label' => false,
                    'class' => 'form-control',
                    'style' => 'ime-mode:active;'
                ]) ?>
            </dd>
        </dl>
        <dl class="dl_table">
            <dt><label class="control-label required"><?= __('肩書・差出人名(お名前)') ?></label><span class="required"><?= __('注') ?></span></dt>
            <dd class="form-group">
                <?= $this->Form->input('sender', [
                    'label' => false,
                    'class' => 'form-control',
                    'style' => 'ime-mode:active;'
                ]) ?>
                <p><?= __('連名の場合は肩書とお名前を続けてご記入ください。') ?></p>
                <?= $this->Form->input('is_sender_external', [
                    'label' => __('お名前に外字がある場合チェックしてください'),
                    'type' => 'checkbox'
                ]) ?>
            </dd>
        </dl>
        <h3><span class="title-level4"><?= __('備考') ?></span></h3>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('備考') ?></label></dt>
            <dd class="form-group">
                <?= $this->Form->input('memo', [
                    'label' => false,
                    'type' => 'textarea',
                    'class' => 'form-control',
                    'style' => 'ime-mode:active;'
                ]) ?>
            </dd>
        </dl>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Form->button(__($next_button), [
                    'class' => 'btn'
                ]) ?></li>
                <?php if (!$order->has('step_payment_end')): ?>
                <li><?= $this->Html->link(__('戻る'), [
                    'prefix' => 'offering',
                    'controller' => 'Home',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php else: ?>
                <li><?= $this->Html->link(__('キャンセル'), [
                    'prefix' => 'offering',
                    'controller' => 'Confirm',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php endif; ?>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
