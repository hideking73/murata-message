<?php
/**
 * ConfirmController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '注文内容｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div class="cart confirm">
    <?= $this->Form->create($order, ['novalidate' => true]) ?>
        <h1 class="title-level2"><?= __('注文内容') ?></h1>
        <div class="flowline">
            <ul>
                <li><span class="flow_number"></span><br><?= __('お届け先') ?><br><?= __('名札名入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('カートの商品') ?></li>
                <li><span class="flow_number"></span><br><?= __('メッセージ入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご請求先入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('決済方法') ?></li>
                <li class="active"><span class="flow_number"></span><br><?= __('ご注文内容確認') ?></li>
                <li><span class="flow_number"></span><br><?= __('完了') ?></li>
            </ul>
        </div>
        <h2 class="title-level3"><?= __('注文内容の確認') ?></h2>
        <h3 class="title-level3s"><?= __('選択商品') ?><span><?= $this->Html->link(__('変更'), [
            'prefix' => 'offering',
            'controller' => 'Cart',
            'action' => 'index'
        ], [
            'class' => 'btn-s'
        ]) ?></span></h3>
        <table class="cart_item">
            <thead>
                <tr>
                    <th><?= __('商品内容') ?></th>
                    <th><?= __('数量') ?></th>
                    <th><?= __('小計') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($order->get('products') as $key => $product): ?>
                <tr>
                    <td>
                        <div class="item_photo">
                            <?php if ($product->has('image_path')): ?>
                            <?= $this->Html->image($product->get('image_path'), [
                                'width' => '150',
                                'height' => '200',
                                'alt' => h($product->get('name')),
                                'title' => h($product->get('name')),
                                'url' => [
                                    'prefix' => 'offering',
                                    'controller' => 'Select',
                                    'action' => 'detail',
                                    $product->get('id')
                                ]
                            ]) ?>
                            <?php endif; ?>
                        </div>
                        <dl class="item_detail">
                            <dt class="item_name text-default">
                                <?= $this->Html->link(h($product->get('name')), [
                                    'prefix' => 'offering',
                                    'controller' => 'Select',
                                    'action' => 'detail',
                                    $product->get('id')
                                ]) ?>
                            </dt>
                            <dd class="item_price"><?= $this->Number->currency($product->get('_joinData')->get('tax_in_price'), 'JPY') ?></dd>
                        </dl>
                    </td>
                    <td class="item_quantity">
                        <?= $this->Number->format($product->get('_joinData')->get('quantity')) ?>
                    </td>
                    <td class="item_subtotal">
                        <span class="product-<?= $key ?>-subtotal"><?= $this->Number->currency($product->get('_joinData')->get('tax_in_subtotal'), 'JPY') ?></span>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php if (!empty($order->get('message_type_id'))): ?>
                <tr>
                    <td class="item_photo">
                        <?= __('弔意メッセージ') ?> : <?= h($message_type->get('label')) ?>
                    </td>
                    <td class="item_quantity">&nbsp;</td>
                    <td class="item_subtotal">
                        <?= $this->Number->currency($order->get('tax_in_message_price'), 'JPY') ?>
                    </td>
                </tr>
                <?php elseif ($order->get('delivery_type_id') == Configure::read('DeliveryType.murata')): ?>
                <tr>
                    <td class="item_photo">
                        <?= __('弔意メッセージ') ?> : <?= __('なし') ?>
                    </td>
                    <td class="item_quantity">&nbsp;</td>
                    <td class="item_subtotal">
                        <?= $this->Number->currency(0, 'JPY') ?>
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <div class="price-area">
            <div class="box">
                <dl class="item-all-price">
                    <dt>商品小計&nbsp;:</dt>
                    <dd class="text-primary">
                        <span class="order-subtotal"><?= $this->Number->currency($order->get('tax_in_subtotal'), 'JPY') ?></span>
                    </dd>
                </dl>
            </div>
            <?php if (0 < $order->get('discount_rate') && !empty($order->products)): ?>
            <div class="box">
                <dl class="discount">
                    <dt>割引(<?= h($order->get('discount_rate')) ?>％)&nbsp;:</dt>
                    <dd class="text-primary">
                        -<span class="order-discount"><?= $this->Number->currency($order->get('tax_in_discount'), 'JPY') ?></span>
                    </dd>
                </dl>
            </div>
            <?php endif; ?>
            <?php if (0 < $order->get('carriage')): ?>
            <div class="box">
                <dl class="shipping">
                    <dt>送料&nbsp;:</dt>
                    <dd class="text-primary">
                        <span class="order-carriage"><?= $this->Number->currency($order->get('tax_in_carriage'), 'JPY') ?></span>
                    </dd>
                </dl>
            </div>
            <?php endif; ?>
            <div class="box">
                <dl class="total_price">
                    <dt>合計&nbsp;:</dt>
                    <dd class="text-primary">
                        <span class="order-total"><?= $this->Number->currency($order->get('tax_in_total'), 'JPY') ?></span>
                    </dd>
                </dl>
            </div>
        </div>
        <h3 class="title-level3s"><?= __('お届け先・名札名') ?><span><?= $this->Html->link(__('変更'), [
            'prefix' => 'offering',
            'controller' => 'Delivery',
            'action' => 'index'
        ], [
            'class' => 'btn-s'
        ]) ?></span></h3>
        <div class="block-2">
            <div class="box">
                <h3><span class="title-level4"><?= __('お届け先') ?></span></h3>
                <p><?= h($order->get('delivery')) ?><br>
                    <?= h($order->get('delivery_to')) ?></p>
            </div>
            <div class="box">
                <h3><span class="title-level4">名札名</span></h3>
                <p><?= nl2br(h($order->get('name_tag_nl'))) ?></p>
            </div>
        </div>
        <?php if (!empty($order->get('message_type_id'))): ?>
        <h3 class="title-level3s"><?= __('メッセージの内容') ?><span><?= $this->Html->link(__('変更'), [
            'prefix' => 'offering',
            'controller' => 'Message',
            'action' => 'index'
        ], [
            'class' => 'btn-s'
        ]) ?></span></h3>
        <?php if (!empty($order->get('message_template_id')) || !empty($order->get('original_message'))): ?>
        <p class="box"><?= nl2br(h($order->get('message'))) ?></p>
        <?php else: ?>
        <p class="box error_message"><?= __('メッセージがありません。') ?></p>
        <?php endif; ?>
        <?php endif; ?>
        <h3 class="title-level3s"><?= __('ご請求先') ?><span><?= $this->Html->link(__('変更'), [
            'prefix' => 'offering',
            'controller' => 'Billing',
            'action' => 'index'
        ], [
            'class' => 'btn-s'
        ]) ?></span></h3>
        <div class="block-2">
            <div class="box">
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('会社名(個人名)') ?></label></dt>
                    <dd class="form-group">
                        <?= h($order->get('billing')) ?>
                        <?php if ($order->get('is_billing_external')): ?>
                        <br><?= __('(外字あり)') ?>
                        <?php endif; ?>
                    </dd>
                </dl>
                <?php if (!empty($order->get('position'))): ?>
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('部署名') ?></label></dt>
                    <dd class="form-group">
                        <?= h($order->get('position')) ?>
                    </dd>
                </dl>
                <?php endif; ?>
                <?php if (!empty($order->get('representative'))): ?>
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('担当者名') ?></label></dt>
                    <dd class="form-group">
                        <?= h($order->get('representative')) ?>
                    </dd>
                </dl>
                <?php endif; ?>
                <dl class="dl_table">
                    <dt><label class="control-label required"><?= __('送付住所') ?></label></dt>
                    <dd class="form-group">
                        <div class="form-group form-inline">
                            〒<?= h($order->get('postal_code')) ?>
                        </div>
                        <div class="form-group">
                            <?= h($order->get('address_full')) ?>
                        </div>
                    </dd>
                </dl>
            </div>
            <div class="box">
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('メールアドレス') ?></label></dt>
                    <dd class="form-group">
                        <?= h($order->get('email')) ?>
                    </dd>
                </dl>
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('TEL') ?></label></dt>
                    <dd class="form-group">
                        <?= h($order->get('tel')) ?>
                    </dd>
                </dl>
                <?php if (!empty($order->get('fax'))): ?>
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('FAX') ?></label></dt>
                    <dd class="form-group">
                        <?= h($order->get('fax')) ?>
                    </dd>
                </dl>
                <?php endif; ?>
                <?php if (empty($order->get('customer_id'))): ?>
                <dl class="dl_table">
                    <dt><label class="control-label"><?= __('会員登録') ?></label></dt>
                    <dd class="form-group">
                        <?= $this->Form->input('signup', [
                            'label' => __('注文後、この内容で会員登録をする'),
                            'type' => 'checkbox'
                        ]) ?>
                    </dd>
                </dl>
                <?php endif; ?>
            </div>
        </div>
        <h3 class="title-level3s"><?= __('決済方法') ?><span><?= $this->Html->link(__('変更'), [
            'prefix' => 'offering',
            'controller' => 'Payment',
            'action' => 'index'
        ], [
            'class' => 'btn-s'
        ]) ?></span></h3>
        <p class="box"><?= h($payment_type->get('offering')) ?></p>
        <div class="total_box">
            <ul class="btn_area center">
                <?php if (empty($order->get('message_type_id'))): ?>
                <li><?= $this->Form->button(__($next_button), [
                    'class' => 'btn'
                ]) ?></li>
                <?php elseif (!empty($order->get('message_template_id')) || !empty($order->get('original_message'))):?>
                <li><?= $this->Form->button(__($next_button), [
                    'class' => 'btn'
                ]) ?></li>
                <?php else: ?>
                <li><p class="error_message"><?= __('内容に不備があります。') ?></p></li>
                <?php endif; ?>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
<?debug($order)?>
