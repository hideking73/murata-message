<?php
/**
 * MessageController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '注文内容｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?= $this->Html->script('ordersys.message-count.js') ?>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($order, ['novalidate' => true]) ?>
        <?php if (!$order->has('step_payment_end')): ?>
        <h1 class="title-level2"><?= __('注文内容') ?></h1>
        <div class="flowline">
            <ul>
                <li><span class="flow_number"></span><br><?= __('お届け先') ?><br><?= __('名札名入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('カートの商品') ?></li>
                <li class="active"><span class="flow_number"></span><br><?= __('メッセージ入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご請求先入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('決済方法') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご注文内容確認') ?></li>
                <li><span class="flow_number"></span><br><?= __('完了') ?></li>
            </ul>
        </div>
        <?php else: ?>
        <h1 class="title-level2"><?= __('注文内容 : 変更(メッセージの内容)') ?></h1>
        <?php endif; ?>
        <h2 class="title-level3"><?= __('メッセージの内容') ?></h2>
        <?php if ($order->get('message_type_id') == Configure::read('MessageType.template')): ?>
        <p><?= __('例文より選択してください。') ?></p>
        <dl class="dl_table">
            <dt><label class="control-label">文章選択</label></dt>
            <dd class="form-group">
                <?= $this->Form->input('message_template_id', [
                    'label' => false,
                    'options' => $message_templates,
                    'empty' => '例文を選択',
                    'class' => 'form-control'
                ]) ?>
            </dd>
        </dl>
        <?php endif; ?>
        <?php if ($order->get('message_type_id') == Configure::read('MessageType.original')): ?>
        <p><?= __('オリジナルメッセージを作成してください。') ?></p>
        <dl class="dl_table">
            <dt><label class="control-label">オリジナル作成</label></dt>
            <dd class="form-group">
                <div class="column">
                    <?= $this->Form->input('original_message', [
                        'label' => false,
                        'type' => 'textarea',
                        'class' => 'form-control',
                        'rows' => 6,
                        'placeholder' => 'こちらにご入力ください。(300文字まで)'
                    ]) ?>
                    <p id="message_limit">残り<strong id="message_count">300</strong>文字</p>
                </div>
            </dd>
        </dl>
        <?php endif; ?>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Form->button(__($next_button), [
                    'class' => 'btn'
                ]) ?></li>
                <?php if (!$order->has('step_payment_end')): ?>
                <li><?= $this->Html->link(__('戻る'), [
                    'prefix' => 'offering',
                    'controller' => 'Cart',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php else: ?>
                <li><?= $this->Html->link(__('キャンセル'), [
                    'prefix' => 'offering',
                    'controller' => 'Confirm',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php endif; ?>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
