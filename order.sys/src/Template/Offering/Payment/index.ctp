<?php
/**
 * PaymentController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '注文内容｜供花・供物・弔意メッセージの注文｜村田葬儀社 - 愛媛・松山';
$this->end();

?>

<?php $this->start('meta') ?>
    <meta name="keywords" content="生前予約,相談,供花・供物カタログ,特典,葬儀,葬式,葬祭,式場,村田葬儀社,ムラタホール,実績,信頼,家族葬,愛媛県,松山市" />
    <meta name="description" content="安心と信頼のムラタの「供花・供物カタログ」のご案内。村田葬儀社オフィシャルサイト。愛媛県松山市近郊の葬儀・葬祭なら、100年の実績と信頼のムラタへお任せください。" />
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<div class="cart">
    <?= $this->Form->create($order, ['novalidate' => true]) ?>
        <?php if (!$order->has('step_payment_end')): ?>
        <h1 class="title-level2"><?= __('注文内容') ?></h1>
        <div class="flowline">
            <ul>
                <li><span class="flow_number"></span><br><?= __('お届け先') ?><br><?= __('名札名入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('カートの商品') ?></li>
                <li><span class="flow_number"></span><br><?= __('メッセージ入力') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご請求先入力') ?></li>
                <li class="active"><span class="flow_number"></span><br><?= __('決済方法') ?></li>
                <li><span class="flow_number"></span><br><?= __('ご注文内容確認') ?></li>
                <li><span class="flow_number"></span><br><?= __('完了') ?></li>
            </ul>
        </div>
        <?php else: ?>
        <h1 class="title-level2"><?= __('注文内容 : 変更(決済方法)') ?></h1>
        <?php endif; ?>
        <h2 class="title-level3"><?= __('決済方法選択') ?></h2>
        <dl class="dl_table">
            <dt><label class="control-label"><?= __('支払い方法') ?></label></dt>
            <dd class="form-group">
                <div id="payment">
                    <?= $this->Form->input('payment_type_id', [
                        'label' => false,
                        'type' => 'radio',
                        'options' => $payment_types,
                        'default' => Configure::read('PaymentType.bank'),
                    ]) ?>
                </div>
            </dd>
        </dl>
        <div class="total_box">
            <ul class="btn_area center">
                <li><?= $this->Form->button(__($next_button), [
                    'class' => 'btn'
                ]) ?></li>
                <?php if (!$order->has('step_payment_end')): ?>
                <li><?= $this->Html->link(__('戻る'), [
                    'prefix' => 'offering',
                    'controller' => 'Billing',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php else: ?>
                <li><?= $this->Html->link(__('キャンセル'), [
                    'prefix' => 'offering',
                    'controller' => 'Confirm',
                    'action' => 'index'
                ], [
                    'class' => 'btn continue'
                ]) ?></li>
                <?php endif; ?>
            </ul>
        </div>
    <?= $this->Form->end() ?>
</div>
