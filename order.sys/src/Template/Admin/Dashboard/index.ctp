<?php
/**
 * DashboardController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo 'ダッシュボード｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('ログアウト'), ['prefix' => 'admin', 'controller' => 'Dashboard', 'action' => 'logout']) ?></li>
    </ul>
</nav>
<div class="dashboard form large-10 medium-9 columns content">
    <h3><?= __('ダッシュボード') ?></h3>
    <p><?= __('ダッシュボードへようこそ。') ?></p>
    <h4><?= __('メニュー') ?></h4>
    <ul>
        <li><?= $this->Html->link(__('会員情報'), ['prefix' => 'admin', 'controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('受注情報'), ['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('管理者'), ['prefix' => 'admin', 'controller' => 'Administrators', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('ログアウト'), ['prefix' => 'admin', 'controller' => 'Dashboard', 'action' => 'logout']) ?></li>
    </ul>
</div>
