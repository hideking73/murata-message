<?php
/**
 * DashboardController::login
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo 'ログイン｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('ホームへ戻る'), ['prefix' => false, 'controller' => 'Pages', 'action' => 'display', 'home']) ?></li>
    </ul>
</nav>
<div class="login form large-10 medium-9 columns content">
    <?= $this->Form->create(null, ['novalidate' => true]) ?>
    <fieldset>
        <legend><?= __('ログイン') ?></legend>
        <?= $this->Form->input('username', [
            'label' => __('ユーザー名'),
        ]) ?>
        <?= $this->Form->input('password', [
            'label' => __('パスワード'),
        ]) ?>
    </fieldset>
    <?= $this->Form->button(__('ログイン')) ?>
    <?= $this->Form->end() ?>
</div>
