<?php
/**
 * OrdersController::add
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '新しい受注情報｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?= $this->Html->script('//ajaxzip3.github.io/ajaxzip3.js', ['charset' => 'UTF-8']) ?>
<?= $this->Html->script('ordersys.number-format.js') ?>
<?= $this->Html->script('ordersys.message-count.js') ?>
<script>
(function ($) {
    // お届け先タイプ切り替え処理
    var changeDeliveryType = function (e) {
        $('[id^="section-delivery-type-"]').hide();
        if ($(e).val() == '<?= Configure::read('DeliveryType.murata') ?>') {
            $('[id="section-delivery-type-murata"]').show();
            $('[id="section-delivery-type-murata"]').find('input,select,textarea').prop('disabled', false);
            $('[id="section-delivery-type-other"]').find('input,select,textarea').prop('disabled', true);
        } else if ($(e).val() == '<?= Configure::read('DeliveryType.other') ?>') {
            $('[id="section-delivery-type-other"]').show();
            $('[id="section-delivery-type-murata"]').find('input,select,textarea').prop('disabled', true);
            $('[id="section-delivery-type-other"]').find('input,select,textarea').prop('disabled', false);
        }
    };
    // メッセージタイプ切り替え処理
    var changeMessageType = function (e) {
        $('[id^="section-message-type-"]').hide();
        if ($(e).val() == '<?= Configure::read('MessageType.template') ?>') {
            $('[id="section-message-type-template"]').show();
            $('[id="section-message-type-template"]').find('input,select,textarea').prop('disabled', false);
            $('[id="section-message-type-original"]').find('input,select,textarea').prop('disabled', true);
        } else if ($(e).val() == '<?= Configure::read('MessageType.original') ?>') {
            $('[id="section-message-type-original"]').show();
            $('[id="section-message-type-template"]').find('input,select,textarea').prop('disabled', true);
            $('[id="section-message-type-original"]').find('input,select,textarea').prop('disabled', false);
        } else {
            $('[id="section-message-type-template"]').find('input,select,textarea').prop('disabled', true);
            $('[id="section-message-type-original"]').find('input,select,textarea').prop('disabled', true);
        }
    };
    // 会員検索処理
    var getCustomer = function (e) {
        // 会員IDがセットされた場合に会員情報を取得
        if ($(e).val() != '') {
            $.ajax({
                url: '<?= $this->Url->build(["prefix" => "api", "controller" => "Customers", "action" => "get_customer", "_ext" => "json"], true) ?>',
                type: 'get',
                dataType: 'json',
                data: { 'customer_id': $(e).val() }
            }).done(function (data) {
                if (data.result.status) {
                    $.each(data.result.data, function (key, val) {
                        if (key != 'is_billing_external') {
                            $('[name="' + key + '"]').val(val);
                        } else {
                            $('[name="' + key + '"]').prop('checked', val);
                        }
                    });
                }
            });
        }
    };
    // 商品選択処理
    var selectProduct = function (e) {
        // 商品IDから商品情報を取得して入力欄の調整をする
        $.ajax({
            url: '<?= $this->Url->build(["prefix" => "api", "controller" => "Products", "action" => "get_product", "_ext" => "json"], true) ?>',
            type: 'get',
            dataType: 'json',
            data: { 'product_id': $(e).val() }
        }).done(function (data) {
            if (data.result.status && data.result.data.product_type_id == <?= Configure::read('ProductType.other') ?>) {
                // "その他"商品の場合、数量入力欄非表示/価格入力欄表示
                $('.add_product_quantity').hide();
                $('.add_product_quantity').find('[name="quantity"]').val(1);
                $('.add_product_price').show();
                $('.add_product_price').find('input,select,textarea').prop('disabled', false);
            } else {
                // "通常"商品の場合、数量入力欄表示/価格入力欄非表示
                $('.add_product_quantity').show();
                $('.add_product_quantity').find('[name="quantity"]').val(1);
                $('.add_product_price').hide();
                $('.add_product_price').find('input,select,textarea').prop('disabled', true);
            }
        });
    };
    // 商品データJSON
    var products = $.parseJSON('<?= ($json_products != "[]") ? $json_products : "{}" ?>');
    var prices = {};
    var row = '<tr class="product_{id}">'
            + '<td><span class="name"></span><?= $this->Form->hidden("products.{id}._joinData.product_id") ?></td>'
            + '<td><span class="quantity"></span><?= $this->Form->hidden("products.{id}._joinData.quantity") ?></td>'
            + '<td><span class="subtotal"></span><?= $this->Form->hidden("products.{id}._joinData.price") ?></td>'
            + '<td><button type="button" class="del_product" data-id="{id}"><?= __("削除") ?></button></td>'
            + '</tr>';
    // 描画後
    $(function () {
        // 商品個数調整
        $('[name$="[quantity]"]').on('keyup', function (e) {
            if ($(this).val() == '') {
                $(this).val(1);
            } else if (parseInt($(this).val()) < 1) {
                $(this).val(1);
            } else if (999 < parseInt($(this).val())) {
                $(this).val(999);
            }
        });
        // 商品行追加
        $('.add_product').on('click', function (e) {
            var product_id = $('[name="product_id"]').val();
            var quantity = $('[name="quantity"]').val();
            var price = $('[name="price"]').val();
            // 商品が選択されている場合
            if (product_id != '') {
                // 商品データに商品追加
                products[product_id + ''] = quantity;
                // 価格データ
                prices[product_id + ''] = price;
                // 商品行全削除
                $('#products').empty();
                // 商品行作成
                var i = 0;
                $.each(products, function (key, value) {
                    // 商品行作成
                    var $row = $(row.replace(/{id}/g, i));
                    // value設定
                    $row.find('[name$="[product_id]"]').val(key);
                    $row.find('[name$="[quantity]"]').val(value);
                    $row.find('.quantity').text(value);
                    // 商品情報取得
                    var product_name = '';
                    var tax_in_price = 0;
                    $.ajax({
                        url: '<?= $this->Url->build(["prefix" => "api", "controller" => "Products", "action" => "get_product", "_ext" => "json"], true) ?>',
                        type: 'get',
                        dataType: 'json',
                        data: { 'product_id': key, 'price': (typeof prices[key] !== 'undefined' ? prices[key] - 0 : 0) }
                    }).done(function (data) {
                        if (data.result.status) {
                            // 値取得
                            product_name = data.result.data.label;
                            tax_in_price = data.result.data.tax_in_price;
                            // text設定
                            $row.find('.name').text(product_name);
                            $row.find('.subtotal').text(currency(tax_in_price * value));
                            $row.find('[name$="[price]"]').val(data.result.data.price);
                            // "その他"商品の場合
                            if (data.result.data.product_type_id == <?= Configure::read('ProductType.other') ?>) {
                                // "その他"商品の詳細入力欄表示
                                $('#other-detail').show();
                            }
                        }
                    });
                    // 行追加
                    $('#products').append($row);
                    i++;
                });
            }
        });
        // 商品行削除
        $('#products').on('click', '.del_product', function (e) {
            // データから商品を削除
            var product_id = $(this).data('id');
            delete products[product_id + ''];
            delete prices[product_id + ''];
            // 商品行削除
            $(this).parents('tr').remove();
            // データを精査して表示欄の調整をする
            var is_other_product = false;
            $.each(products, function (key, value) {
                $.ajax({
                    url: '<?= $this->Url->build(["prefix" => "api", "controller" => "Products", "action" => "get_product", "_ext" => "json"], true) ?>',
                    type: 'get',
                    dataType: 'json',
                    data: { 'product_id': key }
                }).done(function (data) {
                    if (data.result.status && data.result.data.product_type_id == <?= Configure::read('ProductType.other') ?>) {
                        // "その他"商品がある場合、詳細入力欄表示
                        is_other_product = true;
                    }
                });
            });
            if (!is_other_product) {
                $('#other-detail').hide();
            }
        });
        // お届け先の住所検索
        $('[name="delivery_postal_code"]').on('blur', function (e) {
            // 郵便番号調整
            var postal_code = $(this).val().replace(/[^0-9]/g, '').replace(/(\d{3})(\d{4})/g, '$1-$2');
            $(this).val(postal_code);
            // 住所検索
            AjaxZip3.zip2addr('delivery_postal_code', '', 'delivery_prefecture_id', 'delivery_address');
            $('[name="delivery_address_etc"]').val('');
        });
        // 請求先の住所検索
        $('[name="postal_code"]').on('blur', function (e) {
            // 郵便番号調整
            var postal_code = $(this).val().replace(/[^0-9]/g, '').replace(/(\d{3})(\d{4})/g, '$1-$2');
            $(this).val(postal_code);
            // 住所検索
            AjaxZip3.zip2addr('postal_code', '', 'prefecture_id', 'address');
            $('[name="address_etc"]').val('');
        });
        // 電話番号のフォーマット
        $('[name="tel"], [name="fax"]').on('blur', function (e) {
            $.ajax({
                url: '<?= $this->Url->build(["prefix" => "api", "controller" => "Format", "action" => "get_phone", "_ext" => "json"], true) ?>',
                type: 'get',
                data: { 'number': $(this).val() },
                dataType: 'json'
            }).done(function (data) {
                if (data.result.status) {
                    $.each(data.result.data, function (field, val) {
                        if (field == 'number') {
                            $(e.target).val(val);
                        }
                    });
                }
            });
        });
        // お届け先タイプ切り替え
        changeDeliveryType($('[name="delivery_type_id"]:checked'));
        $('[name="delivery_type_id"]').on('change', function (e) {
            changeDeliveryType(this);
        });
        // メッセージタイプ切り替え
        changeMessageType($('[name="message_type_id"]:checked'));
        $('[name="message_type_id"]').on('change', function (e) {
            changeMessageType(this);
        });
        // 会員検索
        getCustomer($('[name="customer_id"]'));
        $('[name="customer_id"]').on('change', function (e) {
            getCustomer(this);
        });
        // 商品選択処理
        selectProduct($('#product-id'));
        $('#product-id').on('change', function (e) {
            selectProduct(this);
        });
    });
})(jQuery);
</script>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('受注情報一覧'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="orders form large-10 medium-9 columns content">
    <h3><?= __('新しい受注情報') ?></h3>
    <?= $this->Form->create($order, ['novalidate' => true]) ?>
    <fieldset>
        <legend><?= __('受注情報') ?></legend>
        <?= $this->Form->input('order_state_id', [
            'label' => __('状況'),
            'options' => $order_states,
            'default' => Configure::read('OrderState.waiting')
        ]) ?>
        <?= $this->Form->input('payment_type_id', [
            'label' => __('支払い方法'),
            'type' => 'radio',
            'options' => $payment_types,
            'default' => Configure::read('PaymentType.bank'),
            'required' => false
        ]) ?>
    </fieldset>
    <fieldset>
        <legend><?= __('メッセージ情報') ?></legend>
        <?= $this->Form->input('delivery', [
            'label' => __('お届け先')
        ]) ?>
        <?= $this->Form->input('delivery_type_id', [
            'label' => __('お届け先タイプ'),
            'type' => 'radio',
            'options' => $delivery_types,
            'default' => Configure::read('DeliveryType.murata'),
            'required' => false
        ]) ?>
        <div id="section-delivery-type-murata">
            <?= $this->Form->input('facility_id', [
                'label' => __('葬祭式場'),
                'options' => $facilities,
                'empty' => '葬祭式場を選択する',
                'default' => ''
            ]) ?>
        </div>
        <div id="section-delivery-type-other">
            <?= $this->Form->input('delivery_postal_code', [
                'label' => __('郵便番号')
            ]) ?>
            <?= $this->Form->input('delivery_prefecture_id', [
                'label' => __('都道府県'),
                'options' => $prefectures,
                'empty' => '都道府県を選択する'
            ]) ?>
            <?= $this->Form->input('delivery_address', [
                'label' => __('住所(市区町村名)')
            ]) ?>
            <?= $this->Form->input('delivery_address_etc', [
                'label' => __('住所(番地・ビル名)')
            ]) ?>
        </div>
        <hr>
        <?= $this->Form->input('company', [
            'label' => __('名札:会社名')
        ]) ?>
        <?= $this->Form->input('sender', [
            'label' => __('名札:肩書・差出人名(お名前)')
        ]) ?>
        <?= $this->Form->input('is_sender_external', [
            'label' => __('お名前に外字がある場合チェックしてください'),
            'type' => 'checkbox'
        ]) ?>
        <?= $this->Form->input('memo', [
            'label' => __('備考'),
            'type' => 'textarea'
        ]) ?>
        <?= $this->Form->input('message_type_id', [
            'label' => __('メッセージ'),
            'type' => 'radio',
            'options' => $message_types,
            'empty' => 'なし',
            'default' => '',
            'required' => false
        ]) ?>
        <div id="section-message-type-template">
            <?= $this->Form->input('message_template_id', [
                'label' => __('メッセージテンプレート'),
                'options' => $message_templates,
                'empty' => 'メッセージテンプレートを選択',
                'default' => ''
            ]) ?>
        </div>
        <div id="section-message-type-original">
            <?= $this->Form->input('original_message', [
                'label' => __('オリジナルメッセージ')
            ]) ?>
            <p id="message_limit">残り<strong id="message_count">300</strong>文字</p>
        </div>
    </fieldset>
    <fieldset>
        <legend><?= __('請求先') ?></legend>
        <?= $this->Form->input('customer_id', [
            'label' => __('会員情報'),
            'options' => $customers,
            'empty' => '会員未選択',
        ]) ?>
        <?= $this->Form->input('billing', [
            'label' => __('会社名(個人名)'),
        ]) ?>
        <?= $this->Form->input('is_billing_external', [
            'label' => __('お名前に外字がある場合チェックしてください'),
            'type' => 'checkbox'
        ]) ?>
        <?= $this->Form->input('position', [
            'label' => __('部署名'),
        ]) ?>
        <?= $this->Form->input('representative', [
            'label' => __('担当者名'),
        ]) ?>
        <?= $this->Form->input('postal_code', [
            'label' => __('郵便番号'),
        ]) ?>
        <?= $this->Form->input('prefecture_id', [
            'label' => __('都道府県'),
            'options' => $prefectures,
            'empty' => '都道府県を選択する'
        ]) ?>
        <?= $this->Form->input('address', [
            'label' => __('住所(市区町村名)'),
        ]) ?>
        <?= $this->Form->input('address_etc', [
            'label' => __('住所(番地・ビル名)'),
        ]) ?>
        <?= $this->Form->input('tel', [
            'label' => __('電話番号'),
        ]) ?>
        <?= $this->Form->input('fax', [
            'label' => __('ファックス番号'),
        ]) ?>
        <?= $this->Form->input('email', [
            'label' => __('メールアドレス'),
            'type' => 'email'
        ]) ?>
    </fieldset>
    <fieldset>
        <legend><?= __('購入商品') ?></legend>
        <table>
            <tbody id="add-product">
                <th><?= __('商品') ?></th>
                <td>
                    <?= $this->Form->input("product_id", [
                        'label' => false,
                        'type' => 'select',
                        'options' => $products,
                        'empty' => '商品を選択する'
                    ]) ?>
                </td>
                <th class="add_product_quantity"><?= __('数量') ?></th>
                <td class="add_product_quantity">
                    <?= $this->Form->input('quantity', [
                        'label' => false,
                        'type' => 'number',
                        'default' => 1
                    ]) ?>
                </td>
                <th class="add_product_price"><?= __('価格') ?></th>
                <td class="add_product_price">
                    <?= $this->Form->input('price', [
                        'label' => false,
                        'type' => 'number',
                        'default' => 0
                    ]) ?>
                </td>
                <td><button type="button" class="add_product"><?= __('追加') ?></button></td>
            </tbody>
        </table>
        <table>
            <thead>
                <tr>
                    <th><?= __('商品') ?></th>
                    <th><?= __('数量') ?></th>
                    <th><?= __('小計') ?></th>
                    <th><?= __('削除') ?></th>
                </tr>
            </thead>
            <tbody id="products">
                <?php if ($order->has('products')): ?>
                <?php foreach ($order->get('products') as $key => $product): ?>
                <tr class="product_<?= $product->get('id') ?>">
                    <td class="product_<?= $product->get('id') ?>_name">
                        <?= h($product->get('name')) ?>
                        <?= $this->Form->hidden("products.{$key}._joinData.product_id") ?>
                    </td>
                    <td class="product_<?= $product->get('id') ?>_quantity">
                        <?= h($product->get('_joinData')->get('quantity')) ?>
                        <?= $this->Form->hidden("products.{$key}._joinData.quantity") ?>
                    </td>
                    <td class="product_<?= $product->get('id') ?>_tax_in_subtotal">
                        <span class="subtotal"><?= $this->Number->currency($product->get('_joinData')->get('tax_in_subtotal'), 'JPY') ?></span>
                        <?= $this->Form->hidden("products.{$key}._joinData.price") ?>
                    </td>
                    <td><button type="button" class="del_product" data><?= __('削除') ?></button></td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
            <tbody id="other-detail"<?= !$order->get('is_other_product') ? ' class="hide"' : null ?>>
                <tr>
                    <td><?= __('その他商品詳細欄') ?></td>
                    <td colspan="3">
                        <?= $this->Form->input('other_detail', [
                            'label' => false,
                            'type' => 'textarea'
                        ]) ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?= $this->Form->button(__('登録')) ?>
    <?= $this->Form->end() ?>
</div>
