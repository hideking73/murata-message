<?php
/**
 * OrdersController::view
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '受注情報の詳細｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('受注情報一覧'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('新しい受注情報'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('この受注情報を編集'), ['action' => 'edit', $order->get('id')]) ?></li>
        <li><?= $this->Form->postLink(__('この受注情報を削除'), ['action' => 'delete', $order->get('id')], ['confirm' => __('本当に ID:{0} を削除しますか？', $order->get('id'))]) ?></li>
    </ul>
</nav>
<div class="orders view large-10 medium-9 columns content">
    <h3><?= __('受注情報の詳細') ?></h3>
    <?= $this->Form->create($order, ['novalidate' => true]) ?>
    <h4><?= __('受注情報') ?></h4>
    <table class="vertical-table">
        <tr>
            <th><?= __('ID') ?></th>
            <td><?= $this->Number->format($order->get('id')) ?></td>
        </tr>
        <tr>
            <th><?= __('受注日') ?></th>
            <td><?= h($order->get('created')) ?></td>
        </tr>
        <tr>
            <th><?= __('状況') ?></th>
            <td>
                <?= $this->Form->input('order_state_id', [
                    'label' => false,
                    'options' => $order_states
                ]) ?>
                <?= $this->Form->button(__('更新')) ?>
            </td>
        </tr>
        <tr>
            <th><?= __('支払い方法') ?></th>
            <td><?= $order->has('payment_type') ? h($order->get('payment_type')->get('payment_type')) : null ?></td>
        </tr>
    </table>
    <?= $this->Form->end() ?>
    <h4><?= __('メッセージ情報') ?></h4>
    <table class="vertical-table">
        <tr>
            <th><?= __('お届け先') ?></th>
            <td><?= h($order->get('delivery')) ?><br>
                <?= h($order->get('delivery_to')) ?></td>
        </tr>
        <tr>
            <th><?= __('名札') ?></th>
            <td><?= nl2br(h($order->get('name_tag_nl'))) ?></td>
        </tr>
        <tr>
            <th><?= __('備考') ?></th>
            <td><?= nl2br(h($order->get('memo'))) ?></td>
        </tr>
        <?php if (!empty($order->get('message'))): ?>
        <tr>
            <th><?= __('メッセージ') ?></th>
            <td><?= nl2br(h($order->get('message'))) ?></td>
        </tr>
        <?php endif; ?>
    </table>
    <h4><?= __('請求先情報') ?></h4>
    <table class="vertical-table">
        <tr>
            <th><?= __('請求先') ?></th>
            <td><?= nl2br(h($order->get('billing_full_nl'))) ?></td>
        </tr>
        <tr>
            <th><?= __('請求先住所') ?></th>
            <td>〒<?= h($order->get('postal_code')) ?><br>
                <?= h($order->get('address_full')) ?></td>
        </tr>
        <tr>
            <th><?= __('電話番号') ?></th>
            <td><?= h($order->get('tel')) ?></td>
        </tr>
        <tr>
            <th><?= __('ファックス番号') ?></th>
            <td><?= h($order->get('fax')) ?></td>
        </tr>
        <tr>
            <th><?= __('メールアドレス') ?></th>
            <td><?= h($order->get('email')) ?></td>
        </tr>
        <tr>
            <th><?= __('会員状態') ?></th>
            <td><?= $order->has('customer') ? $this->Html->link($order->get('customer')->get('state'), ['controller' => 'Customers', 'action' => 'view', $order->get('customer_id')]) : __('非会員') ?></td>
        </tr>
    </table>
    <h4><?= __('購入商品情報') ?></h4>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('商品タイプ') ?></th>
                <th><?= __('商品名') ?></th>
                <th><?= __('単価(税込)') ?></th>
                <th><?= __('数量') ?></th>
                <th><?= __('小計(税込)') ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td><?= __('商品小計(税込)') ?></td>
                <td><?= $this->Number->currency($order->get('tax_in_subtotal'), 'JPY') ?></td>
            </tr>
            <?php if (0 < $order->get('discount_rate')): ?>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td><?= __('割引(' . h($order->get('discount_rate')) . '%)') ?></td>
                <td>-<?= $this->Number->currency($order->get('tax_in_discount'), 'JPY') ?></td>
            </tr>
            <?php endif; ?>
            <?php if (0 < $order->get('tax_in_carriage')): ?>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td><?= __('送料(税込)') ?></td>
                <td><?= $this->Number->currency($order->get('tax_in_carriage'), 'JPY') ?></td>
            </tr>
            <?php endif; ?>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td><?= __('合計(税込)') ?></td>
                <td><?= $this->Number->currency($order->get('tax_in_total'), 'JPY') ?></td>
            </tr>
            <?php if ($order->get('is_other_product')): ?>
            <tr>
                <td colspan="2"><?= __('その他商品詳細') ?></td>
                <td colspan="4"><?= nl2br(h($order->get('other_detail'))) ?></td>
            </tr>
            <?php endif; ?>
        </tfoot>
        <tbody>
            <?php foreach ($order->products as $product): ?>
            <tr>
                <td><?= h($product->get('id')) ?></td>
                <td><?= $product->has('product_type') ? h($product->get('product_type')->get('product_type')) : null ?></td>
                <td><?= h($product->get('name')) ?></td>
                <td><?= $this->Number->currency($product->_joinData->tax_in_price, 'JPY') ?></td>
                <td><?= $this->Number->format($product->_joinData->quantity) ?></td>
                <td><?= $this->Number->currency($product->_joinData->tax_in_subtotal, 'JPY') ?></td>
            </tr>
            <?php endforeach; ?>
            <?php if ($order->has('message_type')): ?>
            <tr>
                <td>&nbsp;</td>
                <td><?= __('メッセージ') ?></td>
                <td><?= h($order->get('message_type')->get('message_type')) ?></td>
                <td colspan="2">&nbsp;</td>
                <td>+<?= $this->Number->currency($order->tax_in_message_price, 'JPY') ?></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>
