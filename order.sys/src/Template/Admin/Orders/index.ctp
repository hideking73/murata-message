<?php
/**
 * OrdersController::view
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '受注情報一覧｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('新しい受注情報'), ['action' => 'add']) ?></li>
        <li class="heading"><?= __('出力操作') ?></li>
        <li><?= $this->Html->link(__('CSVをダウンロード'), ['action' => 'export', '_ext' => 'csv', '?' => $this->request->query]) ?></li>
    </ul>
</nav>
<div class="orders index large-10 medium-9 columns content">
    <h3><?= __('受注情報一覧') ?></h3>
    <?= $this->Form->create($order_search, ['type' => 'get', 'novalidate' => true]) ?>
    <h4><?= __('検索') ?></h4>
    <table>
        <tbody>
            <tr>
                <td><?= $this->Form->label('delivery', __('お届け先')) ?></td>
                <td colspan="3">
                    <?= $this->Form->input('delivery', [
                        'label' => false
                    ]) ?>
                </td>
            </tr>
            <tr>
                <td><?= $this->Form->label('billing', __('請求先')) ?></td>
                <td colspan="3">
                    <?= $this->Form->input('billing', [
                        'label' => false
                    ]) ?>
                </td>
            </tr>
            <tr>
                <td><?= $this->Form->label('facility', __('式場')) ?></td>
                <td colspan="3">
                    <?= $this->Form->input('facility', [
                        'label' => false,
                        'type' => 'select',
                        'options' => $order_search->facilities,
                        'empty' => true
                    ]) ?>
                </td>
            </tr>
            <tr>
                <td><?= $this->Form->label('created_from', __('受注期間')) ?></td>
                <td>
                    <?= $this->Form->input('created_from', [
                        'label' => false,
                        'type' => 'date',
                        'empty' => true,
                        'monthNames' => false
                    ]) ?>
                </td>
                <td>
                    <span>〜</span>
                </td>
                <td>
                    <?= $this->Form->input('created_to', [
                        'label' => false,
                        'type' => 'date',
                        'empty' => true,
                        'monthNames' => false
                    ]) ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?= $this->Form->button(__('検索')) ?>
    <?= $this->Form->end() ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Orders.id', __('ID')) ?></th>
                <th><?= $this->Paginator->sort('Orders.created', __('注文日')) ?></th>
                <th><?= __('合計(税込)') ?></th>
                <th><?= $this->Paginator->sort('Orders.order_state_id', __('状況')) ?></th>
                <th><?= $this->Paginator->sort('Orders.payment_type_id', __('支払い')) ?></th>
                <th><?= $this->Paginator->sort('Orders.delivery', __('お届け先')) ?></th>
                <th><?= $this->Paginator->sort('Orders.prefecture_id', __('請求先')) ?></th>
                <th><?= $this->Paginator->sort('Orders.customer_id', __('会員/非会員')) ?></th>
                <th class="actions"><?= __('操作') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orders as $order): ?>
            <tr>
                <td><?= $this->Number->format($order->get('id')) ?></td>
                <td><?= h($order->get('created')) ?></td>
                <td><?= $this->Number->currency($order->get('tax_in_total'), 'JPY') ?></td>
                <td><?= $order->has('order_state') ? h($order->get('order_state')->get('order_state')) : null ?></td>
                <td><?= $order->has('payment_type') ? h($order->get('payment_type')->get('payment_type')) : null ?></td>
                <td><?= h($order->get('delivery')) ?><br>
                    <?= h($order->get('delivery_to')) ?></td>
                <td><?= nl2br(h($order->get('billing_full_nl'))) ?><br>
                    〒<?= h($order->get('postal_code')) ?><br>
                    <?= h($order->get('address_full')) ?></td>
                <td><?= $order->has('customer') ? $this->Html->link(__($order->get('customer')->get('state')), ['controller' => 'Customers', 'action' => 'view', $order->get('customer_id')]) : __('非会員') ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('詳細'), ['action' => 'view', $order->get('id')]) ?><br>
                    <?= $this->Html->link(__('編集'), ['action' => 'edit', $order->get('id')]) ?><br>
                    <?= !empty($order->message) ? $this->Html->link(__('印刷'), ['action' => 'message_print', $order->get('id')]) : null ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('前へ')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('次へ') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
