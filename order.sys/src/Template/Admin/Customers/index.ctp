<?php
/**
 * CustomersController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '会員情報一覧｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('新しい会員情報'), ['action' => 'add']) ?></li>
        <li class="heading"><?= __('出力操作') ?></li>
        <li><?= $this->Html->link(__('CSVをダウンロード'), ['action' => 'export', '_ext' => 'csv']) ?></li>
    </ul>
</nav>
<div class="customers index large-10 medium-9 columns content">
    <h3><?= __('会員情報一覧') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Customers.id', __('ID')) ?></th>
                <th><?= $this->Paginator->sort('Customers.is_formal', __('状態')) ?></th>
                <th><?= $this->Paginator->sort('Customers.billing', __('請求先')) ?></th>
                <th><?= $this->Paginator->sort('Customers.prefecture_id', __('請求先住所')) ?></th>
                <th><?= $this->Paginator->sort('Customers.email', __('メールアドレス')) ?></th>
                <th><?= $this->Paginator->sort('Customers.created', __('登録日')) ?></th>
                <th><?= $this->Paginator->sort('Customers.modified', __('更新日')) ?></th>
                <th class="actions"><?= __('操作') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customers as $customer): ?>
            <tr>
                <td><?= $this->Number->format($customer->get('id')) ?></td>
                <td><?= h($customer->get('state')) ?></td>
                <td><?= nl2br(h($customer->get('billing_full_nl'))) ?></td>
                <td>〒<?= h($customer->get('postal_code')) ?><br>
                    <?= h($customer->get('address_full')) ?></td>
                <td><?= h($customer->get('email')) ?></td>
                <td><?= h($customer->get('created')) ?></td>
                <td><?= h($customer->get('modified')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('詳細'), ['action' => 'view', $customer->get('id')]) ?><br>
                    <?= $this->Html->link(__('編集'), ['action' => 'edit', $customer->get('id')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('前へ')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('次へ') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
