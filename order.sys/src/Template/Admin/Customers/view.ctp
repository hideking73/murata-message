<?php
/**
 * CustomersController::view
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '会員情報の詳細｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('会員情報一覧'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('新しい会員情報'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('この会員情報を編集'), ['action' => 'edit', $customer->get('id')]) ?></li>
        <li><?= $this->Form->postLink(__('この会員情報を削除'), ['action' => 'delete', $customer->get('id')], ['confirm' => __('本当に ID:{0} を削除しますか？', $customer->get('id'))]) ?></li>
        <li class="heading"><?= __('受注情報操作') ?></li>
        <li><?= $this->Html->link(__('この会員で新しい受注情報'), ['controller' => 'Orders', 'action' => 'add', $customer->get('id')]) ?></li>
    </ul>
</nav>
<div class="customers view large-10 medium-9 columns content">
    <h3><?= h('会員情報の詳細') ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('ID') ?></th>
            <td><?= $this->Number->format($customer->get('id')) ?></td>
        </tr>
        <tr>
            <th><?= __('メールアドレス') ?></th>
            <td><?= h($customer->get('email')) ?></td>
        </tr>
        <tr>
            <th><?= __('状態') ?></th>
            <td><?= h($customer->get('state')) ?></td>
        </tr>
        <tr>
            <th><?= __('請求先') ?></th>
            <td><?= nl2br(h($customer->get('billing_full_nl'))) ?></td>
        </tr>
        <tr>
            <th><?= __('請求先住所') ?></th>
            <td>〒<?= h($customer->get('postal_code')) ?><br>
                <?= h($customer->get('address_full')) ?></td>
        </tr>
        <tr>
            <th><?= __('電話番号') ?></th>
            <td><?= h($customer->get('tel')) ?></td>
        </tr>
        <tr>
            <th><?= __('ファックス番号') ?></th>
            <td><?= h($customer->get('fax')) ?></td>
        </tr>
        <tr>
            <th><?= __('登録日') ?></th>
            <td><?= h($customer->get('created')) ?></td>
        </tr>
        <tr>
            <th><?= __('更新日') ?></th>
            <td><?= h($customer->get('modified')) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('この会員の受注情報') ?></h4>
        <?php if (!empty($customer->orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('注文日') ?></th>
                <th><?= __('合計(税込)') ?></th>
                <th><?= __('状況') ?></th>
                <th><?= __('支払い') ?></th>
                <th><?= __('お届け先') ?></th>
                <th><?= __('請求先') ?></th>
                <th class="actions"><?= __('操作') ?></th>
            </tr>
            <?php foreach ($customer->orders as $order): ?>
            <tr>
                <td><?= h($order->get('id')) ?></td>
                <td><?= h($order->get('created')) ?></td>
                <td><?= $this->Number->currency($order->get('tax_in_total'), 'JPY') ?></td>
                <td><?= $order->has('order_state') ? h($order->get('order_state')->get('order_state')) : null ?></td>
                <td><?= $order->has('payment_type') ? h($order->get('payment_type')->get('payment_type')) : null ?></td>
                <td>
                    <?= h($order->get('delivery')) ?><br>
                    <?= h($order->get('delivery_to')) ?>
                </td>
                <td><?= nl2br(h($order->get('billing_full_nl'))) ?><br>
                    〒<?= h($order->get('postal_code')) ?><br>
                    <?= h($order->get('address_full')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('詳細'), ['controller' => 'Orders', 'action' => 'view', $order->get('id')]) ?>
                    <?= $this->Html->link(__('編集'), ['controller' => 'Orders', 'action' => 'edit', $order->get('id')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
