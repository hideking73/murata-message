<?php
/**
 * CustomersController::add
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '新しい会員情報｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?= $this->Html->script('//ajaxzip3.github.io/ajaxzip3.js', ['charset' => 'UTF-8']) ?>
<script>
(function ($) {
    $(function () {
        // 住所検索
        $('[name="postal_code"]').on('blur', function (e) {
            // 郵便番号調整
            var postal_code = $(this).val().replace(/[^0-9]/g, '').replace(/(\d{3})(\d{4})/g, '$1-$2');
            $(this).val(postal_code);
            // 住所検索
            AjaxZip3.zip2addr('postal_code', '', 'prefecture_id', 'address');
            $('[name="address_etc"]').val('').focus();
        });
        // 電話番号のフォーマット
        $('[name="tel"], [name="fax"]').on('blur', function (e) {
            $.ajax({
                url: '<?= $this->Url->build(["prefix" => "api", "controller" => "Format", "action" => "get_phone", "_ext" => "json"], true) ?>',
                type: 'get',
                data: { 'number': $(this).val() },
                dataType: 'json'
            }).done(function (data) {
                if (data.result.status) {
                    $.each(data.result.data, function (field, val) {
                        if (field == 'number') {
                            $(e.target).val(val);
                        }
                    });
                }
            });
        });
    });
})(jQuery);
</script>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('会員情報一覧'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="customers form large-10 medium-9 columns content">
    <h3><?= __('新しい会員情報') ?></h3>
    <?= $this->Form->create($customer, ['novalidate' => true]) ?>
    <fieldset>
        <legend><?= __('アカウント') ?></legend>
        <?= $this->Form->input('email', [
            'label' => __('メールアドレス'),
            'type' => 'email',
            'templateVars' => [
                'description' => '会員のログインIDになります'
            ]
        ]) ?>
        <?= $this->Form->input('password', [
            'label' => __('パスワード'),
            'templateVars' => [
                'description' => '半角英数記号8〜32文字'
            ]
        ]) ?>
        <?= $this->Form->input('password_again', [
            'label' => __('パスワード再入力'),
            'type' => 'password',
            'required' => true
        ]) ?>
        <?= $this->Form->input('is_formal', [
            'label' => __('状態'),
            'type' => 'select',
            'options' => $customer_states,
            'default' => false,
            'required' => true
        ]) ?>
        <?= $this->Form->input('discount_rate', [
            'label' => __('割引率(%)'),
            'templateVars' => [
                'description' => '割引適用企業の場合は入力してください'
            ]
        ]) ?>
    </fieldset>
    <fieldset>
        <legend><?= __('請求先') ?></legend>
        <?= $this->Form->input('billing', [
            'label' => __('会社名(個人名)')
        ]) ?>
        <?= $this->Form->input('is_billing_external', [
            'label' => __('お名前に外字がある場合チェックしてください'),
            'type' => 'checkbox'
        ]) ?>
        <?= $this->Form->input('position', [
            'label' => __('部署名')
        ]) ?>
        <?= $this->Form->input('representative', [
            'label' => __('担当者名')
        ]) ?>
        <?= $this->Form->input('postal_code', [
            'label' => __('郵便番号')
        ]) ?>
        <?= $this->Form->input('prefecture_id', [
            'label' => __('都道府県'),
            'empty' => '都道府県を選択',
            'options' => $prefectures,
        ]) ?>
        <?= $this->Form->input('address', [
            'label' => __('住所(市町村名)'),
            'templateVars' => [
                'description' => '(例: 松山市湊町)'
            ]
        ]) ?>
        <?= $this->Form->input('address_etc', [
            'label' => __('住所(番地・ビル名)'),
            'templateVars' => [
                'description' => '(例: 6丁目4-5)'
            ]
        ]) ?>
        <?= $this->Form->input('tel', [
            'label' => __('電話番号')
        ]) ?>
        <?= $this->Form->input('fax', [
            'label' => __('ファックス番号')
        ]) ?>
        <?= $this->Form->input('closing_day', [
            'label' => __('締日')
        ]) ?>
    </fieldset>
    <?= $this->Form->button(__('登録')) ?>
    <?= $this->Form->end() ?>
</div>
