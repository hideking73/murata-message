<?php
/**
 * AdministratorsController::view
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '管理者の詳細｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('管理者一覧'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('新しい管理者'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('この管理者を編集'), ['action' => 'edit', $administrator->get('id')]) ?></li>
        <li><?= $this->Form->postLink(__('この管理者を削除'), ['action' => 'delete', $administrator->get('id')], ['confirm' => __('本当に ID:{0} を削除しますか？', $administrator->get('id'))]) ?></li>
    </ul>
</nav>
<div class="administrators view large-10 medium-9 columns content">
    <h3><?= __('管理者の詳細') ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('ID') ?></th>
            <td><?= $this->Number->format($administrator->get('id')) ?></td>
        </tr>
        <tr>
            <th><?= __('ログインID') ?></th>
            <td><?= h($administrator->get('username')) ?></td>
        </tr>
        <tr>
            <th><?= __('管理者名') ?></th>
            <td><?= h($administrator->get('name')) ?></td>
        </tr>
        <tr>
            <th><?= __('作成日時') ?></th>
            <td><?= h($administrator->get('created')) ?></td>
        </tr>
        <tr>
            <th><?= __('更新日時') ?></th>
            <td><?= h($administrator->get('modified')) ?></td>
        </tr>
    </table>
</div>
