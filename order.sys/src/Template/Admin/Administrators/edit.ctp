<?php
/**
 * AdministratorsController::edit
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '管理者を編集｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('管理者一覧'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('新しい管理者'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('この管理者の詳細'), ['action' => 'view', $administrator->get('id')]) ?></li>
        <li><?= $this->Form->postLink(__('この管理者を削除'), ['action' => 'delete', $administrator->get('id')], ['confirm' => __('本当に ID:{0} を削除しますか？', $administrator->get('id'))]) ?></li>
    </ul>
</nav>
<div class="administrators form large-10 medium-9 columns content">
    <h3><?= __('管理者を編集') ?></h3>
    <?= $this->Form->create($administrator, ['novalidate' => true]) ?>
    <fieldset>
        <legend><?= __('管理者') ?></legend>
        <?= $this->Form->input('username', [
            'label' => __('ログインID'),
            'templateVars' => [
                'description' => '半角英数およびアンダーバー(_)'
            ]
        ]) ?>
        <?= $this->Form->input('name', [
            'label' => __('管理者名')
        ]) ?>
        <?= $this->Form->input('password', [
            'label' => __('パスワード'),
            'templateVars' => [
                'description' => '半角英数記号8〜32文字 (変更する場合のみ入力)'
            ]
        ]) ?>
        <?= $this->Form->input('password_again', [
            'label' => __('パスワード再入力'),
            'type' => 'password'
        ]) ?>
    </fieldset>
    <?= $this->Form->button(__('更新')) ?>
    <?= $this->Form->end() ?>
</div>
