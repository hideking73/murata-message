<?php
/**
 * AdministratorsController::index
 */
use Cake\Core\Configure;

// タイトル設定
$this->start('title');
echo '管理者一覧｜村田葬儀社コントロールパネル';
$this->end();

?>

<?php $this->start('meta') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
<?php $this->end() ?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('新しい管理者'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="administrators index large-10 medium-9 columns content">
    <h3><?= __('管理者一覧') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Administrators.id', __('ID')) ?></th>
                <th><?= $this->Paginator->sort('Administrators.username', __('ログインID')) ?></th>
                <th><?= $this->Paginator->sort('Administrators.name', __('管理者名')) ?></th>
                <th><?= $this->Paginator->sort('Administrators.created', __('登録日')) ?></th>
                <th><?= $this->Paginator->sort('Administrators.modified', __('更新日')) ?></th>
                <th class="actions"><?= __('操作') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($administrators as $administrator): ?>
            <tr>
                <td><?= $this->Number->format($administrator->get('id')) ?></td>
                <td><?= h($administrator->get('username')) ?></td>
                <td><?= h($administrator->get('name')) ?></td>
                <td><?= h($administrator->get('created')) ?></td>
                <td><?= h($administrator->get('modified')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('詳細'), ['action' => 'view', $administrator->get('id')]) ?>
                    <?= $this->Html->link(__('編集'), ['action' => 'edit', $administrator->get('id')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('前へ')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('次へ') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
