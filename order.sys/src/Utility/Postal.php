<?php
namespace App\Utility;

/**
 * Postal utility.
 */
class Postal
{
    /**
     * Retrieve only numbers.
     *
     * @param string $string
     * @return string
     */
    public function getNumber($string)
    {
        // 数字のみ出力
        return preg_replace('/[^\d]++/', '', $string);
    }

    /**
     * Retrieve format of Japan's postal code number.
     *
     * @param string $string
     * @return string
     */
    public function getJapanFormat($string)
    {
        // 数字のみの郵便番号を取得
        $number = $this->getNumber($string);

        // 郵便番号を整形して出力
        return preg_replace('/^(\d{3})(\d{4})$/', '$1-$2', $number);
    }
}
