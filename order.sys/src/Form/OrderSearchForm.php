<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Order Search Form
 */
class OrderSearchForm extends Form
{

    /**
     * Select options for facility
     *
     * @var array
     */
    public $facilities = [];

    /**
     * Builds the schema for the modelless form
     *
     * @param Schema $schema From schema
     * @return $this
     */
    protected function _buildSchema(Schema $schema)
    {
        // 検索用データの作成
        $this->facilities = TableRegistry::get('Facilities')
            ->find('list')
            ->toArray();
        $this->facilities = Hash::merge($this->facilities, [
            'none' => __('他社の式場')
        ]);

        // フィールドを指定

        $schema
            ->addField('delivery', ['type' => 'string']);

        $schema
            ->addField('billing', ['type' => 'string']);

        $schema
            ->addField('facility', ['type' => 'string']);

        $schema
            ->addField('created_from', ['type' => 'date']);

        $schema
            ->addField('created_to', ['type' => 'date']);

        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param Validator $validator to use against the form
     * @return Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        // フィールドに対するバリデーションを指定

        $validator
            ->date('created_from', ['ymd'], __('正しい日付を指定してください。'))
            ->allowEmpty('created_from');

        $validator
            ->date('created_to', ['ymd'], __('正しい日付を指定してください。'))
            ->allowEmpty('created_to');

        return $validator;
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        // バリデーションが通った時に実行する処理を指定
        return true;
    }
}
