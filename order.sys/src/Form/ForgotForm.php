<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;


/**
 * Forgot Form
 */
class ForgotForm extends Form
{

    /**
     * Builds the schema for the modelless form
     *
     * @param Schema $schema From schema
     * @return $this
     */
    protected function _buildSchema(Schema $schema)
    {
        // フィールドを指定
        return $schema
            ->addField('email', ['type' => 'string']);
    }

    /**
     * Form validation builder
     *
     * @param Validator $validator to use against the form
     * @return Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        // フィールドに対するバリデーションを指定
        return $validator
            ->notEmpty('email', __('メールアドレスを入力してください。'));
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        // バリデーションが通った時に実行する処理を指定
        return true;
    }
}
