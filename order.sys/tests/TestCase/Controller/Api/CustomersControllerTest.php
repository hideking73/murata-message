<?php
namespace App\Test\TestCase\Controller\Api;

use App\Controller\Api\CustomersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Api\CustomersController Test Case
 */
class CustomersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers',
        'app.prefectures'
    ];

    /**
     * Test get_customer method
     *
     * @return void
     */
    public function testGetCustomer()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
