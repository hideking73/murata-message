<?php
namespace App\Test\TestCase\Controller\Api;

use App\Controller\Api\ProductsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Api\ProductsController Test Case
 */
class ProductsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.products',
        'app.product_types'
    ];

    /**
     * Test get_product method
     *
     * @return void
     */
    public function testGetProduct()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
