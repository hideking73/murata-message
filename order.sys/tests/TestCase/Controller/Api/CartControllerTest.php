<?php
namespace App\Test\TestCase\Controller\Api;

use App\Controller\Api\CartController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Api\CartController Test Case
 */
class CartControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.orders',
        'app.order_states',
        'app.payment_types',
        'app.delivery_types',
        'app.facilities',
        'app.prefectures',
        'app.message_types',
        'app.orders_products',
        'app.products',
        'app.product_types',
        'app.customers'
    ];

    /**
     * Test get_customer method
     *
     * @return void
     */
    public function testUpdateOrderSession()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
