<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\CustomersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\CustomersController Test Case
 */
class CustomersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers',
        'app.customer_edit_tokens',
        'app.customer_signup_tokens',
        'app.reset_password_tokens',
        'app.orders',
        'app.orders_products',
        'app.prefectures',
        'app.order_states',
        'app.payment_types',
        'app.message_types',
        'app.delivery_areas',
        'app.delivery_types',
        'app.facilities',
        'app.product_types',
        'app.message_templates',
        'app.products',
        'app.tax_rates'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
