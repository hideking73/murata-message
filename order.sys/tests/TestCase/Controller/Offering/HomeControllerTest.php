<?php
namespace App\Test\TestCase\Controller\Offering;

use App\Controller\Offering\HomeController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Offering\HomeController Test Case
 */
class HomeControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
