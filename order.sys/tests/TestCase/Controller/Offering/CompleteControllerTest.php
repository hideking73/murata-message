<?php
namespace App\Test\TestCase\Controller\Offering;

use App\Controller\Offering\CompleteController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Offering\CompleteController Test Case
 */
class CompleteControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
