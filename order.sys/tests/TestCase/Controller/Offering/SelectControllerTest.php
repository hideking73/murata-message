<?php
namespace App\Test\TestCase\Controller\Offering;

use App\Controller\Offering\SelectController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Offering\SelectController Test Case
 */
class SelectControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test detail method
     *
     * @return void
     */
    public function testDetail()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
