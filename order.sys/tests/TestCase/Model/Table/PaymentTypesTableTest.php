<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentTypesTable Test Case
 */
class PaymentTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PaymentTypesTable
     */
    public $PaymentTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.payment_types',
        'app.orders',
        'app.order_states',
        'app.message_types',
        'app.message_templates',
        'app.product_types',
        'app.products',
        'app.orders_products',
        'app.prefectures',
        'app.customers',
        'app.customer_signup_tokens',
        'app.customer_edit_tokens',
        'app.reset_password_tokens',
        'app.delivery_types',
        'app.facilities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PaymentTypes') ? [] : ['className' => 'App\Model\Table\PaymentTypesTable'];
        $this->PaymentTypes = TableRegistry::get('PaymentTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
