<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MessageTemplatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MessageTemplatesTable Test Case
 */
class MessageTemplatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MessageTemplatesTable
     */
    public $MessageTemplates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.message_templates',
        'app.product_types',
        'app.products',
        'app.orders',
        'app.order_states',
        'app.payment_types',
        'app.message_types',
        'app.prefectures',
        'app.customers',
        'app.customer_signup_tokens',
        'app.customer_edit_tokens',
        'app.reset_password_tokens',
        'app.delivery_types',
        'app.facilities',
        'app.orders_products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MessageTemplates') ? [] : ['className' => 'App\Model\Table\MessageTemplatesTable'];
        $this->MessageTemplates = TableRegistry::get('MessageTemplates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MessageTemplates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findOffering method
     *
     * @return void
     */
    public function testFindOffering()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findCelebrate method
     *
     * @return void
     */
    public function testFindCelebrate()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
