<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomerSignupTokensTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomerSignupTokensTable Test Case
 */
class CustomerSignupTokensTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CustomerSignupTokensTable
     */
    public $CustomerSignupTokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CustomerSignupTokens') ? [] : ['className' => 'App\Model\Table\CustomerSignupTokensTable'];
        $this->CustomerSignupTokens = TableRegistry::get('CustomerSignupTokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CustomerSignupTokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
