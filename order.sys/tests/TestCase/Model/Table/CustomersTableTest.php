<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomersTable Test Case
 */
class CustomersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CustomersTable
     */
    public $Customers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers',
        'app.prefectures',
        'app.customer_edit_tokens',
        'app.customer_signup_tokens',
        'app.reset_password_tokens',
        'app.orders',
        'app.order_states',
        'app.payment_types',
        'app.message_types',
        'app.message_templates',
        'app.delivery_types',
        'app.facilities',
        'app.orders_products',
        'app.products',
        'app.product_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Customers') ? [] : ['className' => 'App\Model\Table\CustomersTable'];
        $this->Customers = TableRegistry::get('Customers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Customers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findDetail method
     *
     * @return void
     */
    public function testFindDetail()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findFormal method
     *
     * @return void
     */
    public function testFindFormal()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findTemporary method
     *
     * @return void
     */
    public function testFindTemporary()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test formal method
     *
     * @return void
     */
    public function testFormal()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test temporary method
     *
     * @return void
     */
    public function testTemporary()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test formalAll method
     *
     * @return void
     */
    public function testFormalAll()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test temporaryAll method
     *
     * @return void
     */
    public function testTemporaryAll()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
