<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DeliveryAreasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DeliveryAreasTable Test Case
 */
class DeliveryAreasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DeliveryAreasTable
     */
    public $DeliveryAreas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.delivery_areas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DeliveryAreas') ? [] : ['className' => 'App\Model\Table\DeliveryAreasTable'];
        $this->DeliveryAreas = TableRegistry::get('DeliveryAreas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DeliveryAreas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
