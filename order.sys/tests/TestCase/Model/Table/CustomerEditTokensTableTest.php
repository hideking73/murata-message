<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomerEditTokensTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomerEditTokensTable Test Case
 */
class CustomerEditTokensTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CustomerEditTokensTable
     */
    public $CustomerEditTokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CustomerEditTokens') ? [] : ['className' => 'App\Model\Table\CustomerEditTokensTable'];
        $this->CustomerEditTokens = TableRegistry::get('CustomerEditTokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CustomerEditTokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
