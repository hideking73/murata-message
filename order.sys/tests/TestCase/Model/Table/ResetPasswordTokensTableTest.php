<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ResetPasswordTokensTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ResetPasswordTokensTable Test Case
 */
class ResetPasswordTokensTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ResetPasswordTokensTable
     */
    public $ResetPasswordTokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ResetPasswordTokens') ? [] : ['className' => 'App\Model\Table\ResetPasswordTokensTable'];
        $this->ResetPasswordTokens = TableRegistry::get('ResetPasswordTokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ResetPasswordTokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
