<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OrdersFixture
 *
 */
class OrdersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'order_state_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '受注対応状況ID', 'precision' => null, 'autoIncrement' => null],
        'payment_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '決済方法ID', 'precision' => null, 'autoIncrement' => null],
        'delivery' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'お届け先', 'precision' => null, 'fixed' => null],
        'delivery_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '1', 'comment' => 'お届け先タイプID', 'precision' => null, 'autoIncrement' => null],
        'facility_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '施設ID', 'precision' => null, 'autoIncrement' => null],
        'delivery_postal_code' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'お届け先の郵便番号', 'precision' => null, 'fixed' => null],
        'delivery_prefecture_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'お届け先の都道府県ID', 'precision' => null, 'autoIncrement' => null],
        'delivery_address' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'お届け先の住所(市区町村名)', 'precision' => null, 'fixed' => null],
        'delivery_address_etc' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'お届け先のその他の住所(番地・ビル名)', 'precision' => null, 'fixed' => null],
        'company' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '名札:会社名', 'precision' => null, 'fixed' => null],
        'sender' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '名札:肩書・差出人名(お名前)', 'precision' => null, 'fixed' => null],
        'is_sender_external' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '差出人名外字フラグ [0:外字なし,1:外字あり]', 'precision' => null],
        'memo' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '備考', 'precision' => null],
        'message_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'メッセージタイプID', 'precision' => null, 'autoIncrement' => null],
        'message_template_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'メッセージテンプレートID', 'precision' => null, 'autoIncrement' => null],
        'original_message' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'オリジナルメッセージ', 'precision' => null],
        'message_price' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '購入時のメッセージ料(税別)', 'precision' => null, 'autoIncrement' => null],
        'tax_calc' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => 'floor', 'collate' => 'utf8_general_ci', 'comment' => '購入時の消費税の端数計算(処理名) [floor|ceil|round]', 'precision' => null, 'fixed' => null],
        'tax_rate' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '購入時の消費税率(%)', 'precision' => null, 'autoIncrement' => null],
        'discount_calc' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => 'floor', 'collate' => 'utf8_general_ci', 'comment' => '購入時の割引の端数計算(処理名) [floor|ceil|round]', 'precision' => null, 'fixed' => null],
        'discount_rate' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '購入時の割引率(%)', 'precision' => null, 'autoIncrement' => null],
        'carriage' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '購入時の送料(税別)', 'precision' => null, 'autoIncrement' => null],
        'billing' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '請求先(会社名または個人名)', 'precision' => null, 'fixed' => null],
        'is_billing_external' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '請求先名外字フラグ [0:外字なし,1:外字あり]', 'precision' => null],
        'position' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '部署名', 'precision' => null, 'fixed' => null],
        'representative' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '担当者名', 'precision' => null, 'fixed' => null],
        'postal_code' => ['type' => 'string', 'length' => 10, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '郵便番号', 'precision' => null, 'fixed' => null],
        'prefecture_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '都道府県ID', 'precision' => null, 'autoIncrement' => null],
        'address' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '住所(市区町村名)', 'precision' => null, 'fixed' => null],
        'address_etc' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'その他の住所(番地・ビル名)', 'precision' => null, 'fixed' => null],
        'tel' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '電話番号', 'precision' => null, 'fixed' => null],
        'fax' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'ファックス番号', 'precision' => null, 'fixed' => null],
        'email' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'メールアドレス', 'precision' => null, 'fixed' => null],
        'customer_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '会員情報ID', 'precision' => null, 'autoIncrement' => null],
        'is_active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => 'レコード状態フラグ [0:無効(削除),1:有効]', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '作成日時', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '更新日時', 'precision' => null],
        '_indexes' => [
            'order_state_id' => ['type' => 'index', 'columns' => ['order_state_id'], 'length' => []],
            'payment_type_id' => ['type' => 'index', 'columns' => ['payment_type_id'], 'length' => []],
            'delivery_type_id' => ['type' => 'index', 'columns' => ['delivery_type_id'], 'length' => []],
            'facility_id' => ['type' => 'index', 'columns' => ['facility_id'], 'length' => []],
            'delivery_prefecture_id' => ['type' => 'index', 'columns' => ['delivery_prefecture_id'], 'length' => []],
            'message_type_id' => ['type' => 'index', 'columns' => ['message_type_id'], 'length' => []],
            'message_template_id' => ['type' => 'index', 'columns' => ['message_template_id'], 'length' => []],
            'prefecture_id' => ['type' => 'index', 'columns' => ['prefecture_id'], 'length' => []],
            'customer_id' => ['type' => 'index', 'columns' => ['customer_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'orders_ibfk_1' => ['type' => 'foreign', 'columns' => ['order_state_id'], 'references' => ['order_states', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'orders_ibfk_2' => ['type' => 'foreign', 'columns' => ['payment_type_id'], 'references' => ['payment_types', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'orders_ibfk_3' => ['type' => 'foreign', 'columns' => ['delivery_type_id'], 'references' => ['delivery_types', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'orders_ibfk_4' => ['type' => 'foreign', 'columns' => ['facility_id'], 'references' => ['facilities', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'orders_ibfk_5' => ['type' => 'foreign', 'columns' => ['delivery_prefecture_id'], 'references' => ['prefectures', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'orders_ibfk_6' => ['type' => 'foreign', 'columns' => ['message_type_id'], 'references' => ['message_types', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'orders_ibfk_7' => ['type' => 'foreign', 'columns' => ['message_template_id'], 'references' => ['message_templates', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'orders_ibfk_8' => ['type' => 'foreign', 'columns' => ['prefecture_id'], 'references' => ['prefectures', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'orders_ibfk_9' => ['type' => 'foreign', 'columns' => ['customer_id'], 'references' => ['customers', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'order_state_id' => 1,
            'payment_type_id' => 1,
            'delivery' => 'Lorem ipsum dolor sit amet',
            'delivery_type_id' => 1,
            'facility_id' => 1,
            'delivery_postal_code' => 'Lorem ip',
            'delivery_prefecture_id' => 1,
            'delivery_address' => 'Lorem ipsum dolor sit amet',
            'delivery_address_etc' => 'Lorem ipsum dolor sit amet',
            'company' => 'Lorem ipsum dolor sit amet',
            'sender' => 'Lorem ipsum dolor sit amet',
            'is_sender_external' => 1,
            'memo' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'message_type_id' => 1,
            'message_template_id' => 1,
            'original_message' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'message_price' => 1,
            'tax_calc' => 'Lorem ipsum dolor sit amet',
            'tax_rate' => 1,
            'discount_calc' => 'Lorem ipsum dolor sit amet',
            'discount_rate' => 1,
            'carriage' => 1,
            'billing' => 'Lorem ipsum dolor sit amet',
            'is_billing_external' => 1,
            'position' => 'Lorem ipsum dolor sit amet',
            'representative' => 'Lorem ipsum dolor sit amet',
            'postal_code' => 'Lorem ip',
            'prefecture_id' => 1,
            'address' => 'Lorem ipsum dolor sit amet',
            'address_etc' => 'Lorem ipsum dolor sit amet',
            'tel' => 'Lorem ipsum dolor sit amet',
            'fax' => 'Lorem ipsum dolor ',
            'email' => 'Lorem ipsum dolor ',
            'customer_id' => 1,
            'is_active' => 1,
            'created' => '2016-08-20 02:12:04',
            'modified' => '2016-08-20 02:12:04'
        ],
    ];
}
