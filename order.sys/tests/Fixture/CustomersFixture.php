<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CustomersFixture
 *
 */
class CustomersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'email' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'メールアドレス', 'precision' => null, 'fixed' => null],
        'password' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'パスワード', 'precision' => null],
        'billing' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '請求先(会社名または個人名)', 'precision' => null, 'fixed' => null],
        'is_billing_external' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '請求先名外字フラグ [0:外字なし,1:外字あり]', 'precision' => null],
        'position' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '部署名', 'precision' => null, 'fixed' => null],
        'representative' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '担当者名', 'precision' => null, 'fixed' => null],
        'postal_code' => ['type' => 'string', 'length' => 10, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '郵便番号', 'precision' => null, 'fixed' => null],
        'prefecture_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '都道府県ID', 'precision' => null, 'autoIncrement' => null],
        'address' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '住所(市区町村名)', 'precision' => null, 'fixed' => null],
        'address_etc' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'その他の住所(番地・ビル名)', 'precision' => null, 'fixed' => null],
        'tel' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '電話番号', 'precision' => null, 'fixed' => null],
        'fax' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'ファックス番号', 'precision' => null, 'fixed' => null],
        'discount_rate' => ['type' => 'integer', 'length' => 2, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '割引率(%)', 'precision' => null, 'autoIncrement' => null],
        'closing_day' => ['type' => 'integer', 'length' => 2, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '締日', 'precision' => null, 'autoIncrement' => null],
        'is_formal' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '正会員フラグ [0:仮会員,1:正会員]', 'precision' => null],
        'is_active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => 'レコード状態フラグ [0:無効(削除),1:有効]', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '作成日時', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '更新日時', 'precision' => null],
        '_indexes' => [
            'prefecture_id' => ['type' => 'index', 'columns' => ['prefecture_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'email' => ['type' => 'unique', 'columns' => ['email'], 'length' => []],
            'customers_ibfk_1' => ['type' => 'foreign', 'columns' => ['prefecture_id'], 'references' => ['prefectures', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'billing' => 'Lorem ipsum dolor sit amet',
            'is_billing_external' => 1,
            'position' => 'Lorem ipsum dolor sit amet',
            'representative' => 'Lorem ipsum dolor sit amet',
            'postal_code' => 'Lorem ip',
            'prefecture_id' => 1,
            'address' => 'Lorem ipsum dolor sit amet',
            'address_etc' => 'Lorem ipsum dolor sit amet',
            'tel' => 'Lorem ipsum dolor ',
            'fax' => 'Lorem ipsum dolor ',
            'discount_rate' => 1,
            'closing_day' => 1,
            'is_formal' => 1,
            'is_active' => 1,
            'created' => '2016-08-01 09:00:00',
            'modified' => '2016-08-01 09:00:00'
        ],
    ];
}
